<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempUnblockApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_unblock_approvals', function (Blueprint $table) {
            $table->id();
            $table->string('cust_code');
            $table->string('tmp_unblock_period');
            $table->string('approval_status');
            $table->string('request_by');
            $table->datetime('request_date');
            $table->string('response_by');
            $table->datetime('response_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_unblock_approvals');
    }
}
