<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->string('CardCode');
            $table->string('CardName');
            $table->string('PayToCode');
            $table->text('CustAddress');
            $table->text('EnvAddress');
            $table->string('Attention');
            $table->string('DocNum');
            $table->integer('LineNum');
            $table->dateTime('DocDate');
            $table->dateTime('DocDueDate');
            $table->string('CustomerOrderNo');
            $table->string('SalesID');
            $table->text('Description');
            $table->string('SOCurrency');
            $table->float('UnitPrice');
            $table->float('TotalPrice');
            $table->float('Amount');
            $table->float('Rebate');
            $table->float('PPH23');
            $table->float('PPN');
            $table->float('TotalAmount');
            $table->string('VA');
            $table->string('CompanyCodeVA');
            $table->string('CustCodeVA');
            $table->string('PaymentStatus');
            $table->string('CID');
            $table->dateTime('CreatedDateINV');
            $table->string('checker');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
