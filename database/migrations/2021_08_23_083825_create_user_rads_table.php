<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rads', function (Blueprint $table) {
            $table->string('username');
            $table->string('value');
            $table->string('id');
            $table->string('groupname');
            $table->string('attribute');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('disabled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rads');
    }
}
