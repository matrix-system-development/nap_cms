<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOutStatusTableBillingTakeouts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_takeouts', function (Blueprint $table) {
            $table->float('Outstanding')->after('CreatedDateINV');
            $table->string('NewCustomer')->after('Outstanding');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_takeouts', function (Blueprint $table) {
            //
        });
    }
}
