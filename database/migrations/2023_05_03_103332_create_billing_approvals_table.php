<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_approvals', function (Blueprint $table) {
            $table->id();
            $table->string('docnum');
            $table->string('status');
            $table->dateTime('approval_time');
            $table->text('reason_reject');
            $table->text('remark_request');
            $table->string('request_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_approvals');
    }
}
