<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    
	
  </head>
  <body>
    <span>
      Dear All,
      <br>
      <br>
      Mohon bantuannya untuk dilakukan approval request unblock selama 7 hari untuk :
      <br>
      <br>
      <pre><b>Kode Pelanggan</b>  : {{ $details['cust_code'] }}</pre>
      <br>
      <pre><b>Nama Pelanggan</b>  : {{ $details['cust_name'] }}</pre>
      <br>
      <pre><b>Catatan</b>    : {{ $details['notes'] }}</pre>
      <br>
      <br>
      Terima Kasih
      <br>
      <br>
      <hr>
      <br>
      Dear All,
      <br>
      <br>
      Need your approval for unblock request 7 days period :
      <br>
      <br>
      <pre><b>Customer Code</b>  : {{ $details['cust_code'] }}</pre>
      <br>
      <pre><b>Customer Name</b>  : {{ $details['cust_name'] }}</pre>
      <br>
      <pre><b>Note</b>    : {{ $details['notes'] }}</pre>
      <br>
      <br>
      Terima Kasih
      <br>
    </span>
  </body>
</html>
