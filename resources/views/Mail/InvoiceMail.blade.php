<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    
	
  </head>
  <body>
    <span>
        Pelanggan Yth,
        <br>
        <br>
        Salam hangat dari Matrix NAP Info.
        <br>
        <br>
        Terima kasih atas kepercayaan Anda dalam menggunakan layanan kami. Bersama ini kami informasikan tagihan Anda bulan {{$details['tgl_tagihan_bulan']}}
        <br>
        <br>
        <table border="1">
          <tr>
              <th>Tanggal Invoice</th>
              <th>Tanggal Jatuh Tempo</th>
              <th>No Invoice</th>
              <th>Nominal (RP)</th>
          </tr>
          <tr>
              <td>{{$details['tgl_tagihan']}}</td>
              <td>{{$details['tgl_jatuh_tempo']}}</td>
              <td>{{$details['inv_no']}}</td>
              <td>{{number_format($details['outstanding'],2,",",".")}}</td>
        </tr>
      </table>
        <br>
        <br>
        Demi menjaga kenyamanan Anda dalam menggunakan layanan kami, mohon dapat melakukan pembayaran tagihan sebelum tanggal jatuh tempo.
        <br>
        <br>
        Demikian yang dapat kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih.
        <br>
        <br>
        Hormat Kami
        <br>
        <br>
        Matrix NAP Info
        <br>
        <br>
        @if ($details['is_corp'] == 1 )
          @if (env('APP_ENV')=='local')
            <u><a href="{{ url('http://127.0.0.1:8000/invoice/pdfCorp/'.$details['inv_no'].'/'.(date('Y-m', strtotime($details['tgl_tagihan_bulan'])))) }}" title="Export Invoice">Download PDF</a></u>
          @endif

          @if (env('APP_ENV')=='staging')
            <u><a href="{{ url('https://stagingapi.napinfo.co.id/nap_cms/invoice/pdfCorp/'.$details['inv_no'].'/'.(date('Y-m', strtotime($details['tgl_tagihan_bulan'])))) }}" title="Export Invoice">Download PDF</a></u>
          @endif

          @if (env('APP_ENV')=='production')
            <u><a href="{{ url('https://lentera.napinfo.co.id/cms/invoice/pdfCorp/'.$details['inv_no'].'/'.(date('Y-m', strtotime($details['tgl_tagihan_bulan'])))) }}" title="Export Invoice">Download PDF</a></u>
          @endif
        @else
          @if (env('APP_ENV')=='local')
            <u><a href="{{ url('http://127.0.0.1:8000/invoice/pdf/'.$details['inv_no']) }}" title="Export Invoice">Download PDF</a></u>
          @endif

          @if (env('APP_ENV')=='staging')
            <u><a href="{{ url('https://stagingapi.napinfo.co.id/nap_cms/invoice/pdf/'.$details['inv_no']) }}" title="Export Invoice">Download PDF</a></u>
          @endif

          @if (env('APP_ENV')=='production')
            <u><a href="{{ url('https://lentera.napinfo.co.id/cms/invoice/pdf/'.$details['inv_no']) }}" title="Export Invoice">Download PDF</a></u>
          @endif
        @endif
    </span>
  </body>
</html>
