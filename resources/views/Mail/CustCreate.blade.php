<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    
	
  </head>
  <body>
    <span>
        Hi {{ $details['cust_name'] }},
        <br>
        <br> 
        Thank you for trusting us for your internet service, you can access the dashboard via the following link :
        <br>
        <br>
        Link : <a href="{{ $details['url'] }}">NAP Customer Portal</a> <br>
        Username : <b><i>{{ $details['username'] }}</i></b><br>
        Password : <b><i>{{ $details['password'] }}</i></b>
        <br>
        <br>
        For security reasons, please change your password immediately at the following link: <a href="{{ url('/user-cust/change-pass/'.base64_encode($details['username'])) }}" class="btn btn-success btn-md">Change Password</a>
        <br>
        If there are problems, you can contact our customer care at customer.care@napinfo.co.id
        <br>
        <br>
        Regards,
        <br>
        <br>
        Matrix NAP Info Lintas Nusa
    </span>
  </body>
</html>
