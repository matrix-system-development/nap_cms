<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    
	
  </head>
  <body>
    <span>
        Dear Sir /Madam {{ $details['cust_name'] }}
        <br>
        <br>
        First of all, We Thank you for using Matrix Broadband Services.
        <br>
        <br>
        We hereby inform you of the details for your outstanding due:
        <br>
        <br>
        <table border="1">
            <tr>
                <th>Number</th>
                <th>Customer Code</th>
                <th>Invoice Date</th>
                <th>Due Date</th>
                <th>Invoice Number</th>
                <th>Amount In {{ $details['currency'] }}</th>
            </tr>
            <tr>
                <td>1</td>
                <td>{{ $details['cust_code'] }}</td>
                <td>{{ date('Y-m-d', strtotime($details['inv_date'])) }}</td>
                <td>{{ date('Y-m-25', strtotime($details['inv_date'])) }}</td>
                <td>{{ $details['inv_no'] }}</td>
                <td>{{ number_format($details['amount'],2,",",".") }}</td>
            </tr>
        </table>
        <br>
        <br>
        For your convenience. Kindly do payment on schedule.
        <br>
        <br>
        <i>Please ignore this notification, if you have paid the bill, and please send proof of payment to customer.care@napinfo.co.id</i>
        <br>
        <br>
        Thank you for your attention and cooperation.
        <br>
        <br>
        Best Regards
        <br>
        Matrix NAP Info
        <br>
        <br>
        <hr>
        <br>
        Yang terhormat Bapak/Ibu {{ $details['cust_name'] }}
        <br>
        <br>
        Pertama-tama kami ucapkan terima kasih telah menggunakan Jasa Layanan Matrix Broadband.
        <br>
        <br>
        Bersama ini kami informasikan tagihan anda yang akan memasuki tanggal jatuh tempo. Berikut rinciannya:
        <br>
        <br>
        <table border="1">
            <tr>
                <th>No</th>
                <th>Kode Pelanggan</th>
                <th>Tgl Tagihan</th>
                <th>Tgl Jatuh Tempo</th>
                <th>No Tagihan</th>
                <th>Nominal {{ $details['currency'] }}</th>
            </tr>
            <tr>
                <td>1</td>
                <td>{{ $details['cust_code'] }}</td>
                <td>{{ date('Y-m-d', strtotime($details['inv_date'])) }}</td>
                <td>{{ date('Y-m-25', strtotime($details['inv_date'])) }}</td>
                <td>{{ $details['inv_no'] }}</td>
                <td>{{ number_format($details['amount'],2,",",".") }}</td>
            </tr>
        </table>
        <br>
        <br>
        Demi menjaga kenyamanan anda dalam menggunakan layanan kami, kami himbau agar pembayaran tagihan dapat dilakukan tepat waktu.
        <br>
        <br>
        <i> Mohon abaikan email Pemberitahuan ini apabila Bapak/Ibu telah melakukan pembayaran tagihan diatas, serta mohon dapat dikirimkan bukti transfer pembayaran ke customer.care@napinfo.co.id</i>
        <br>
        <br>
        Terima kasih atas perhatian dan kerjasamanya.
        <br>
        <br>
        Hormat Kami
        <br>
        Matrix NAP Info
    </span>
  </body>
</html>
