@extends('Layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
            <h1 class="m-0">Summary Customers</h1>
        </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $totalUser }}</h3>
                    <br>
                    <br>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{url('/mon-cust')}}" class="small-box-footer">Total Radius Users</a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{ $activeUser }}</h3>
                    <br>
                    <br>
                </div>
                <div class="icon">
                    <i class="ion ion-android-checkmark-circle"></i>
                </div>
                <a href="{{url('/mon-cust')}}" class="small-box-footer">Active Users</a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $tempUnblock }}</h3>
                    <br>
                    <br>
                </div>
                <div class="icon">
                    <i class="ion ion-android-time"></i>
                </div>
                <a href="{{url('/mon-cust')}}" class="small-box-footer">Temporary Unblocked Users</a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{ $blockUser }}</h3>
                    <br>
                    <br>
                </div>
                <div class="icon">
                    <i class="ion ion-android-close"></i>
                </div>
                <a href="{{url('/mon-cust')}}" class="small-box-footer">Blocked Users</a>
                </div>
            </div>
        <!-- ./col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection