@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Announcement Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Announcement</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                      <button title="Add Announcement" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add">
                        <i class="far fa-comment-alt"></i>
                      </button>
                    </div>
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <table id="tableAnnounce" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Created_by</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($announcements as $data)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $data->name_announcement }}</td>
                        <td>{{ $data->announce_type }}</td>
                        <td>
                          @if ($data->status == '1')
                            <small class="badge badge-success">ACTIVE</small> 
                          @else
                            <small class="badge badge-danger">INACTIVE</small>
                          @endif
                        </td>
                        <td>{{ $data->created_by }}
                        </td>
                        <td>
                          <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit{{ $data->id }}">
                            Edit
                          </button>
                          <button title="Detail" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-detail-id{{ $data->id }}">
                            ID
                          </button>
                          <button title="Detail" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-detail-en{{ $data->id }}">
                            EN
                          </button>
                          <button title="Detail" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-detail-cn{{ $data->id }}">
                            CN
                          </button>
                          @if ($data->status == '1')
                            <button title="Revoke" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-revoke{{ $data->id }}">
                              Revoke
                            </button>
                          @else
                            <button title="Activate" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-access{{ $data->id }}">
                              Activate
                            </button>
                          @endif
                          
                        </td>
                    </tr>

                    {{-- Modal Update --}}
                    <div class="modal fade" id="modal-edit{{ $data->id }}">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Edit Announcement</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('announcement/'.$data->id) }}" enctype="multipart/form-data" method="POST">
                          @csrf
                          @method('patch')
                          <div class="modal-body">
                            <div class="form-group">
                              <input type="text" class="form-control" id="ann_name" name="ann_name" placeholder="Announcement Name" value="{{ $data->name_announcement }}">
                            </div>
                            <div class="form-group">
                              <textarea name="ann_description" class="form-control" rows="3" value="{{ $data->description }}" placeholder="Announcement Description In Bahasa">{{ $data->description }}</textarea>
                            </div>
                            <div class="form-group">
                              <textarea name="ann_description_en" class="form-control" rows="3" value="{{ $data->description_en }}" placeholder="Announcement Description In English">{{ $data->description_en }}</textarea>
                            </div>
                            <div class="form-group">
                              <textarea name="ann_description_cn" class="form-control" rows="3" value="{{ $data->description_cn }}" placeholder="Announcement Description In Mandarin">{{ $data->description_cn }}</textarea>
                            </div>
                            <div class="form-group">
                              <select name="ann_type" id="ann_type" class="form-control">
                                <option value="">- Please Select Type -</option>
                                @foreach ($dropdowns['ann_types'] as $type)
                                  <option value="{{ $type->name_value }}" {{ $type->name_value == $data->announce_type ? 'selected' : '' }}>{{ $type->name_value }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Update">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Update --}}

                    {{-- Modal Detail ID--}}
                    <div class="modal fade" id="modal-detail-id{{ $data->id }}">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">{{ $data->name_announcement }}</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            @php
                                echo htmlspecialchars_decode(stripslashes($data->description)); 
                            @endphp
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Detail --}}

                    {{-- Modal Detail EN--}}
                    <div class="modal fade" id="modal-detail-en{{ $data->id }}">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">{{ $data->name_announcement }}</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            @php
                                echo htmlspecialchars_decode(stripslashes($data->description_en)); 
                            @endphp
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Detail --}}

                    {{-- Modal Detail CN--}}
                    <div class="modal fade" id="modal-detail-cn{{ $data->id }}">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">{{ $data->name_announcement }}</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            @php
                                echo htmlspecialchars_decode(stripslashes($data->description_cn)); 
                            @endphp
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Detail --}}

                    {{-- Modal Revoke --}}
                    <div class="modal fade" id="modal-revoke{{ $data->id }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Revoke Announcement</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/announcement/revoke/'.$data->id) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to revoke <label>{{ $data->name_announcement }}</label> ?
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Revoke --}}

                    {{-- Modal Access --}}
                    <div class="modal fade" id="modal-access{{ $data->id }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Activated Announcement</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/announcement/active/'.$data->id) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are You Sure Activate <label for="email">{{ $data->name_announcement }}</label> ?
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Revoke --}}

                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Add Announcement</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ url('announcement') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <input type="text" class="form-control" id="ann_name" name="ann_name" placeholder="Announcement Name">
          </div>
          <div class="form-group">
            <textarea name="ann_description" class="form-control" rows="3" placeholder="Announcement Description In Bahasa"></textarea>
          </div>
          <div class="form-group">
            <textarea name="ann_description_en" class="form-control" rows="3" placeholder="Announcement Description In English"></textarea>
          </div>
          <div class="form-group">
            <textarea name="ann_description_cn" class="form-control" rows="3" placeholder="Announcement Description In Mandarin"></textarea>
          </div>
          <div class="form-group">
            <select name="ann_type" id="ann_type" class="form-control">
              <option value="">- Please Select Type -</option>
              @foreach ($dropdowns['ann_types'] as $type)
                <option value="{{ $type->name_value }}">{{ $type->name_value }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Submit">
        </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>

<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableAnnounce").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });

    new FroalaEditor('textarea#froala-editor-id',{
      placeholderText: 'Start typing something in bahasa...'
    });

    new FroalaEditor('textarea#froala-editor-en',{
      placeholderText: 'Start typing something in english...'
    });

    new FroalaEditor('textarea#froala-editor-cn',{
      placeholderText: 'Start typing something in mandarin...'
    })
  });
</script>
@endsection