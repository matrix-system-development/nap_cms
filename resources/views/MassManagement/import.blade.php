@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Import List Menu</h1>
          </div>
          <div class="col-sm-6">
            <!--alert success -->
            @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{ session('status') }}</strong>
            </div> 
            @endif
            <!--alert success -->
            <!--validasi form-->
            @if (count($errors)>0)
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul>
                    <li><strong>Upload file Failed !</strong></li>
                    @foreach ($errors->all() as $error)
                        <li><strong>{{ $error }}</strong></li>
                    @endforeach
                </ul>
            </div>
          @endif
        <!--end validasi form-->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Upload CSV List</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ url('/import-process') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <input type="file" name="file_list" class="form-control-file" id="exampleFormControlFile1">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" id="upload">Upload</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Upload file</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                <div class="card-body">
                  <table id="tableListUpload" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      {{-- <th>No</th> --}}
                      <th>Filename</th>
                      <th>Row Count</th>
                      <th>Upload Date</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                        $no=1;
                      @endphp
                      @foreach ($upload_lists as $item)
                      <tr>
                          {{-- <td>{{ $no++ }}</td> --}}
                          <td>{{ $item->filename }}</td>
                          <td>{{ $item->row_count }}</td>
                          <td>{{ $item->created_at }}</td>
                          <td>
                            <button title="Block Network" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-block{{ $item->token }}">
                                <i class="fas fa-lock"></i>
                            </button>
                            <button title="UnBlock Network" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-unblock{{ $item->token }}">
                                <i class="fas fa-unlock-alt"></i>
                            </button>
                            <button title="Delete File" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete{{ $item->token }}">
                              <i class="fas fa-trash-alt"></i>
                            </button>
                          </td>
                      </tr>

                        {{-- Modal Block --}}
                        <div class="modal fade" id="modal-block{{ $item->token }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Block Customer Network</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <form action="{{ url('/mass-block/'.$item->token) }}" method="GET" enctype="multipart/form-data">
                              @csrf
                              <div class="modal-body">
                                <div class="form-group">
                                  Are you sure want to Block {{ $item->row_count }} Customers?
                                </div>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Submit" id="submitblock">
                              </div>
                              </form>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                        <!-- /.modal-dialog -->
                        </div>
                        {{-- Modal Block --}}


                        {{-- Modal unBlock --}}
                        <div class="modal fade" id="modal-unblock{{ $item->token }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Unblock Customer Network</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <form action="{{ url('/mass-unblock/'.$item->token) }}" method="GET" enctype="multipart/form-data">
                              @csrf
                              <div class="modal-body">
                                <div class="form-group">
                                  Are you sure want to Unblock {{ $item->row_count }} Customers?
                                </div>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Submit" id="submitblock">
                              </div>
                              </form>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                        <!-- /.modal-dialog -->
                        </div>
                        {{-- Modal unBlock --}}

                        {{-- Modal Delete --}}
                        <div class="modal fade" id="modal-delete{{ $item->token }}">
                          <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Delete File</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <form action="{{ url('/mass-list-delete/'.$item->token) }}" method="GET" enctype="multipart/form-data">
                              @csrf
                              <div class="modal-body">
                                <div class="form-group">
                                  Are you sure want to Delete {{ $item->filename }}?
                                </div>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Submit" id="submitblock">
                              </div>
                              </form>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                        <!-- /.modal-dialog -->
                        </div>
                        {{-- Modal Block --}}
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableListUpload").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "order": [],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>

@endsection