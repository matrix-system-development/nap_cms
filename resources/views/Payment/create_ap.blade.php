@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Payment Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <form action="{{ url('/payment/auto/store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="request_id" value="{{ $reqID }}">
            <input type="hidden" name="transaction_date" value="{{ $paymentData->payment_date }}">
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-between">
                            <div class="col-8">
                                <h3 class="card-title"><b>Create Auto Journal Payment (Sent Data To SAP)</b></h3>
                            </div>
                            <div class="col-4 text-right">
                                @if ($isSentSAP == '0')
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#submitModal"><i class="fas fa-upload"></i> Sent Data</button>
                                @else
                                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>This Payment Data Already Sent to SAP</strong>
                                    </div> 
                                @endif
                            </div>

                            <!-- submitModal -->
                            <div class="modal fade" id="submitModal" tabindex="-1" aria-labelledby="submitModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="submitModalLabel">Submit Data To SAP</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="created_at">Transaction Date</label>
                                            <input type="text" id="transaction_date" name="transaction_date" value="{{ \Carbon\Carbon::parse($paymentData->payment_date)->format('Y-m-d') }}" class="form-control" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="created_at">Payment Date</label>
                                            <input type="date" id="payment_date" name="payment_date" class="form-control" value="{{ old('payment_date') }}">
                                        </div>
                                        @if ($isSentSAP == '0')
                                            <div class="col">
                                                <label class="text-danger">*After data submitted the list of data will be sent to SAP</label>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!--alert success -->
                                @if (session('status'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>{{ session('status') }}</strong>
                                    </div> 
                                @endif
                                <!--alert success -->

                                <!--validasi form-->
                                @if (count($errors)>0)
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <ul>
                                            <li><strong>Check Data Failed !</strong></li>
                                            @foreach ($errors->all() as $error)
                                                <li><strong>{{ $error }}</strong></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <!--end validasi form-->
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="tableAP" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Request ID</th>
                                            <th>Invoice no</th>
                                            <th>Channel</th>
                                            <th>Trans Date</th>
                                            <th>Billed Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no=1;
                                        @endphp
                                        @foreach ($datas as $item)
                                        <tr>
                                            <td>{{ $item->RequestID }}</td>
                                            <td>
                                                <a href="{{ url('/billing/'.$item->inv_number) }}" class="btn btn-info btn-xs" title="View Detail" target="_BLANK">{{ $item->inv_number }}</a>
                                                @if ($item->is_sent_SAP == '1')
                                                    <small class="badge badge-pill badge-success"><i class="fas fa-check"></i> SAP Sent</small>
                                                @else
                                                    <small class="badge badge-pill badge-danger"><i class="fas fa-times"></i> SAP Sent</small>
                                                @endif
                                            </td>
                                            <td>{{ $item->payment_channel }}</td>
                                            <td>{{ $item->payment_date }}</td>
                                            <td><i>{{ $item->SOCurrency." ". number_format($item->nominal_payment,2,",",".") }}</i></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-5">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th style="width:40%">Outstanding Amount:</th>
                                                <td><i>{{ $q_oustanding->SOCurrency . " " . number_format($oustanding,2,",",".") }}</i></td>
                                            </tr>
                                            <tr>
                                                <th>Bank Charge:</th>
                                                <td><i>{{ "(" . $q_oustanding->SOCurrency . " " . number_format($bankCharge,2,",",".") . ")" }}</i></td>
                                            </tr>
                                            <tr>
                                                <th>Paid Amount:</th>
                                                <td><i>{{ $q_oustanding->SOCurrency . " " . number_format($paidAmount,2,",",".") }}</i></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                        
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </form>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
    $(document).ready(function() {
      var table = $("#tableAP").DataTable({
        "responsive": true, 
        //"lengthChange": true, 
        "autoWidth": false,
        "order": [[ 0, "asc" ]],
        "dom": '<"top"i>rt<"clear">',
        "columnDefs": [
            { "width": "25%", "targets": 4 }
        ],
        // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      });
    });
</script>
@endsection