@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Payment Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
             {{--  <div class="card-header">
                <h3 class="card-title">List of Payment From <b><i>{{ date('d F Y', strtotime($date_start)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_finish)) }}</i></b></h3>
              </div> --}}
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{url('/payment/')}}" class="btn btn-success btn-sm" title="Check Payment" target="_BLANK">
                            Check Payment
                        </a>
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-filter">
                          <i class="fas fa-filter"></i> Filter
                        </button>
                        <form action="{{ url('/export-payment') }}" method="post" class="d-inline">
                          @csrf
                          <input type="hidden" name="date_start" id="date_start" value="{{ $date_start }}">
                          <input type="hidden" name="date_finish" id="date_finish" value="{{ $date_finish }}">
                          <input type="hidden" name="pay_channel" id="trans_status" value="{{ $payChannel }}">
                          <button type="submit" class="btn btn-success btn-sm"><i class="far fa-file-excel"></i> Export To Excel</button>
                        </form>
                    </div>
                    {{-- Modal Filter --}}
                    <div class="modal fade" id="modal-filter">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Data Filter</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/payment/list') }}" enctype="multipart/form-data" method="POST">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              <label for="created_at">Created At From</label>
                              <input type="date" id="date_start" name="date_start" class="form-control" value="{{ old('date_start') }}">
                            </div>
                            <div class="form-group">
                              <label for="created_at">Created At Until</label>
                              <input type="date" id="date_finish" name="date_finish" class="form-control" value="{{ old('date_finish') }}">
                            </div>
                            <div class="form-group">
                              <label>Payment Channel</label>
                                <select name="pay_channel" id="pay_channel" class="form-control">
                                    <option value="">All Channel</option>
                                    @foreach ($dropdowns['channel'] as $dropitem)
                                        <option value="{{ $dropitem->payment_channel }}">{{ $dropitem->payment_channel }}</option>
                                    @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Show Data">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Filter --}}
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <table id="tablePay" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>ID</th>
                    <th>Inv Number</th>
                    <th>Cust. Code</th>
                    <th>Nominal Payment</th>
                    <th>Channel</th>
                    <th>RequestID</th>
                    <th>Date Info</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($q_listpays as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>
                              <b>{{ $item->inv_number }}</b>
                              <br> <a href="{{ url('/billing/'.$item->inv_number) }}" class="btn btn-info btn-xs" title="View Detail" target="_BLANK">Detail Bill</a>
                          
                          </td>
                          <td><b>{{ $item->cust_code }}</b><br> {{ $item->CardName }}</td>
                          <td><i>{{ $item->SOCurrency . " " . number_format($item->nominal_payment,2,",",".") }}</i></td>
                          <td>{{ $item->payment_channel }}</td>
                          <td>
                            @if ($item->payment_channel == 'Payments Channel Indomaret' || $item->payment_channel == 'DANA')
                                {{-- Condition untuk is_sent_SAP --}}
                                @if ($item->is_sent_SAP == '1')
                                    {{ $item->RequestID }}
                                    <br>
                                    <small class="badge badge-pill badge-success"><i class="fas fa-check"></i> SAP Sent</small>
                                @else
                                    <a href="{{ url('/payment/auto/create/'.encrypt($item->RequestID)) }}" target="_blank">{{ $item->RequestID }}</a>
                                    <br>
                                    <small class="badge badge-pill badge-danger"><i class="fas fa-times"></i> SAP Sent</small>
                                @endif
                                
                                {{-- Condition untuk is_sync_SAP --}}
                                @if ($item->is_sync_SAP == '1')
                                    <small class="badge badge-pill badge-success"><i class="fas fa-check"></i> SAP Sync</small>
                                @else
                                    <small class="badge badge-pill badge-danger"><i class="fas fa-times"></i> SAP Sync</small>
                                @endif
                            @elseif($item->payment_channel == 'BCA - 6010' || $item->payment_channel == 'BCA - 6011' || $item->payment_channel == 'BCA - 6014' || $item->payment_channel == 'BCA - 6016' || $item->payment_channel == 'BCA - 6017' || $item->payment_channel == 'BCA - 6018')
                                {{-- Condition untuk is_sent_SAP --}}
                                @if ($item->is_sent_SAP == '1')
                                    {{ $item->RequestID }}
                                    <br>
                                    <small class="badge badge-pill badge-success"><i class="fas fa-check"></i> SAP Sent</small>
                                @else
                                  {{-- Check if payment_date is end of month and time is 21:00 or later --}}
                                  @if (Carbon\Carbon::parse($item->payment_date)->isSameDay(Carbon\Carbon::parse($item->payment_date)->copy()->endOfMonth()) && Carbon\Carbon::parse($item->payment_date)->hour >= 21)
                                    <a href="{{ url('/payment/auto/create/'.encrypt($item->RequestID)) }}" target="_blank">{{ $item->RequestID }}</a>
                                    <br>
                                  @else
                                    {{ $item->RequestID }}
                                    <br>
                                  @endif
                                    <small class="badge badge-pill badge-danger"><i class="fas fa-times"></i> SAP Sent</small>
                                @endif
                                
                                {{-- Condition untuk is_sync_SAP --}}
                                @if ($item->is_sync_SAP == '1')
                                    <small class="badge badge-pill badge-success"><i class="fas fa-check"></i> SAP Sync</small>
                                    <br>
                                    <b>SAP Inc Payment Number: </b>{{ $item->SAP_payment_no }}
                                @else
                                    <small class="badge badge-pill badge-danger"><i class="fas fa-times"></i> SAP Sync</small>
                                @endif
                            @else
                                {{ $item->RequestID }}
                            @endif
                          </td>
                          <td>
                              <b>Doc Date: </b>{{ date('Y-m-d', strtotime($item->DocDate)) }}
                              <br>
                              <b>Paid At: </b>{{ date('Y-m-d', strtotime($item->payment_date)) }}
                              <br>
                              <b>Created At: </b>{{ $item->created_at }}
                          </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tablePay").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "order": [0,"desc"],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection