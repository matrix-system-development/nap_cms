@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Payment Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Result Check Payment Status</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Check Data Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div> 
                </div>
                @if ($valid=='1')
                    @foreach ($datas as $data)
                    <div class="row">
                        <div class="col-sm-4">
                            <strong>Invoice Number</strong>
                            <p class="text-muted">
                                {{ $inv_number }}
                            </p>

                            <strong>Cust. ID</strong>
                            <p class="text-muted">
                                {{ $data['Code'] }}
                            </p>

                            <strong>Cust. Name</strong>
                            <p class="text-muted">
                                {{ $data['Name'] }}
                            </p>

                            <strong>Bill To </strong>
                            <p class="text-muted">
                                {{ $data['Bill To'] }}
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <strong>Remarks</strong>
                            <p class="text-muted">
                                {{ $data['Remarks'] }}
                            </p>

                            <strong>Journal Remarks</strong>
                            <p class="text-muted">
                                {{ $data['Journal Remarks'] }}
                            </p>

                            <strong>Transfer Account</strong>
                            <p class="text-muted">
                                {{ $data['Transfer Acct'] }}
                            </p>

                            <strong>Credit Card</strong>
                            <p class="text-muted">
                                {{ $data['Credit Card'] }}
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <strong>Payment Date</strong>
                            <p class="text-muted">
                                {{ date('Y-m-d', strtotime($data['PaymentDate'])) }}
                            </p>

                            <strong>Document Date</strong>
                            <p class="text-muted">
                                {{ date('Y-m-d', strtotime($data['Doc Date'])) }}
                            </p>

                            <strong>Created Date</strong>
                            <p class="text-muted">
                                {{ date('Y-m-d', strtotime($data['CreateDate'])) }}
                            </p>

                            <strong>Nominal Payment</strong>
                            <p class="text-muted">
                                <i>{{ $data['DocCurr']." ".number_format($data['Nominal Payment'],2,",",".") }}</i>
                            </p>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                @else
                    <center><b><i>{{ "Payment for ".$message }}</i></b></center>                   
                @endif
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer" id="search">
                <a href="{{ URL::previous() }}" class="btn btn-primary">Back</a>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script type="text/javascript">
  
</script>
@endsection