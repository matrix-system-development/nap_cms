<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    
    <script type="text/javascript">
      function disableBack() { window.history.forward(); }
      setTimeout("disableBack()", 0);
      window.onunload = function () { null };
    </script>
    
    <style>
      body {
        background-color: #151A48;
      }
    </style>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-6">
            <div class="card mt-5">
                <div class="card-header">
                  <h4>Change Password Confirmation</h4>
                </div>
                <form action="{{ url('/user-cust/submit-change-pass') }}" method="POST">
                @csrf
                <div class="card-body">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Customer Code: <b>{{ $custCode }}</b></label>
                    <input type="hidden" class="form-control" id="cust_code" name="cust_code" value="{{ $custCode }}">
                  </div>
                  <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">New Password</label>
                      <input type="password" class="form-control" id="password_new" name="password_new" onkeyup='check(); checkPwd()'>
                  </div>
                  <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Confirmation Password</label>
                      <input type="password" class="form-control" id="password_confirm" name="password_confirm" onkeyup='check();'>
                  </div>
                  <div class="mb-3">
                      <input type="checkbox" class="form-check-input" onclick="showPassword()">
                      <label class="form-check-label">Show Password</label>
                  </div>
                  <div class="mb-3 text-center">
                      <span id='valid_pass'></span>
                      <br>
                      <span id='message'></span>
                  </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary" id="btnSubmit" disabled>Submit</button>
                </div>
                </form>
              </div>
          </div>
        </div>
    </div>

    <script>
        function showPassword() {
            var x = document.getElementById("password_new");
            var y = document.getElementById("password_confirm");
            //var z = document.getElementById("password_now");
            if (x.type === "password") {
                x.type = "text";
                y.type = "text";
            } else {
                x.type = "password";
                y.type = "password";
            }
        }

        function check() {
          if(document.getElementById('password_new').value == '' || document.getElementById('password_confirm').value =='')
            {
              document.getElementById('message').innerHTML = "";
              document.getElementById("btnSubmit").disabled = true;
            }
          else{
            if (document.getElementById('password_new').value == document.getElementById('password_confirm').value) 
            {
              document.getElementById('message').style.color = 'green';
              document.getElementById('message').innerHTML = "New Password match with Confirmation Password";
              document.getElementById("btnSubmit").disabled = false;

              var str = document.getElementById('password_new').value;
              if (str.length < 8) 
              {
                //alert("too_short");
                document.getElementById('valid_pass').style.color = 'red';
                document.getElementById('valid_pass').innerHTML = "Password Must Contain Minimal 8 Character";
                document.getElementById("btnSubmit").disabled = true;
                return ("Password Must Contain Minimal 8 Character");
              } 
              else if (str.length > 50) 
              {
                document.getElementById('valid_pass').style.color = 'red';
                document.getElementById('valid_pass').innerHTML = "Password Must Contain Maximal 50 Character";
                document.getElementById("btnSubmit").disabled = true;
                return ("Password Must Contain Maximal 50 Character");
              } 
              else if (str.search(/\d/) == -1) 
              {
                //no num
                document.getElementById('valid_pass').style.color = 'red';
                document.getElementById('valid_pass').innerHTML = "Password Must Contain Combination of Letter & Number";
                document.getElementById("btnSubmit").disabled = true;
                return ("Password Must Contain Combination of Letter & Number");
              } 
              else if (str.search(/[a-zA-Z]/) == -1) 
              {
                //no letter
                document.getElementById('valid_pass').style.color = 'red';
                document.getElementById('valid_pass').innerHTML = "Password Must Contain Combination of Letter & Number";
                document.getElementById("btnSubmit").disabled = true;
                return ("Password Must Contain Combination of Letter & Number");
              } 
              else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) != -1) 
              {
                document.getElementById('valid_pass').style.color = 'red';
                document.getElementById('valid_pass').innerHTML = "Password Have Bad Character";
                document.getElementById("btnSubmit").disabled = true;
                return ("Password Have Bad Character");
              }
            }
            else 
            {
              document.getElementById('message').style.color = 'red';
              document.getElementById('message').innerHTML = "New Password doesn't match with Confirmation Password";
              document.getElementById("btnSubmit").disabled = true;
            }
          }
        }

        checkPwd = function () {
          var str = document.getElementById('password_new').value;
          if (str.length < 8) 
          {
            //alert("too_short");
            document.getElementById('valid_pass').style.color = 'red';
            document.getElementById('valid_pass').innerHTML = "Password Must Contain Minimal 8 Character";
            document.getElementById("btnSubmit").disabled = true;
            return ("Password Must Contain Minimal 8 Character");
          } 
          else if (str.length > 50) 
          {
            document.getElementById('valid_pass').style.color = 'red';
            document.getElementById('valid_pass').innerHTML = "Password Must Contain Maximal 50 Character";
            document.getElementById("btnSubmit").disabled = true;
            return ("Password Must Contain Maximal 50 Character");
          } 
          else if (str.search(/\d/) == -1) 
          {
            //no num
            document.getElementById('valid_pass').style.color = 'red';
            document.getElementById('valid_pass').innerHTML = "Password Must Contain Combination of Letter & Number";
            document.getElementById("btnSubmit").disabled = true;
            return ("Password Must Contain Combination of Letter & Number");
          } 
          else if (str.search(/[a-zA-Z]/) == -1) 
          {
            //no letter
            document.getElementById('valid_pass').style.color = 'red';
            document.getElementById('valid_pass').innerHTML = "Password Must Contain Combination of Letter & Number";
            document.getElementById("btnSubmit").disabled = true;
            return ("Password Must Contain Combination of Letter & Number");
          } 
          else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) != -1) 
          {
            document.getElementById('valid_pass').style.color = 'red';
            document.getElementById('valid_pass').innerHTML = "Password Have Bad Character";
            document.getElementById("btnSubmit").disabled = true;
            return ("Password Have Bad Character");
          }

          document.getElementById('valid_pass').style.color = 'green';
          document.getElementById('valid_pass').innerHTML = "Password Strong";
          return ("Password Strong");
        }
    </script>
  </body>
</html>
