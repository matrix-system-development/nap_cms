@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of User Data</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <div class="row mb-3">
                  <div class="col-8">
                    <form class="form-inline" method="POST" action="{{ url('/user') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="name" @if (old('search_by') == "name") {{ 'selected' }} @endif>Employee Name</option>
                          <option value="department" @if (old('search_by') == "department") {{ 'selected' }} @endif>Department</option>
                          <option value="email" @if (old('search_by') == "email") {{ 'selected' }} @endif>Email</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                      <button title="Add User" type="button" class="btn btn-primary btn-sm mb-2 mx-sm-1" data-toggle="modal" data-target="#modal-add">
                        <i class="fas fa-user-plus"></i>
                      </button>
                    </form>
                  </div>
                </div>
                <table id="tableUser" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>PIC Status</th>
                    <th>Last Login</th>
                    <th>Login Counter</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>
                          {{ $user->name }}<br>
                          <b><i>{{ $user->department }}</i></b><br>
                          @if ($user->active_status=='0')
                            <small class="badge badge-danger">INACTIVE</small>
                          @endif
                          @if ($user->is_ca=='1')
                            <small class="badge badge-success">CA</small>
                          @endif
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role }}</td>
                        <td>
                            @if ($user->pic=='1')
                                <small class="badge badge-success">TRUE</small>
                            @else
                                <small class="badge badge-danger">FALSE</small>
                            @endif
                        </td>
                        <td>
                          {{ date('Y-m-d H:i:s', strtotime($user->last_login)) }}
                        </td>
                        <td>
                          {{ $user->login_counter }}
                        </td>
                        <td>
                          <button title="Edit User" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-update{{ $user->id }}">
                            <i class="fas fa-user-edit"></i>
                          </button>

                          @if ($user->active_status=='1')
                            <button title="Revoke Access" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-revoke{{ $user->id }}">
                              <i class="fas fa-user-lock"></i>
                            </button>
                          @else
                            <button title="Give Access" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-access{{ $user->id }}">
                              <i class="fas fa-user-check"></i>
                            </button>
                          @endif

                          @if ($user->is_ca=='1')
                            <button title="Revoke User as CA" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-revoke-ca{{ $user->id }}">
                              <i class="fas fa-times"></i> Revoke as CA
                            </button>
                          @else
                            <button title="Set User as CA" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-add-ca{{ $user->id }}">
                              <i class="fas fa-check"></i> Set as CA
                            </button>
                          @endif
                          
                        </td>
                    </tr>

                    {{-- Modal as CA --}}
                    <div class="modal fade" id="modal-add-ca{{ $user->id }}">
                      <div class="modal-dialog modal-md">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Add User as CA</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/user/access-ca/'.$user->id) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to give this user <label for="email">{{ $user->email }}</label> as CA ?
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal as CA --}}

                    {{-- Modal Revoke CA --}}
                    <div class="modal fade" id="modal-revoke-ca{{ $user->id }}">
                      <div class="modal-dialog modal-md">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Revoke User as CA</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/user/revoke-ca/'.$user->id) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to Revoke this user <label for="email">{{ $user->email }}</label> as CA ?
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-danger" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Revoke CA --}}

                    {{-- Modal Update --}}
                    <div class="modal fade" id="modal-update{{ $user->id }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Edit User</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/user/'.$user->id) }}" enctype="multipart/form-data" method="POST">
                          @csrf
                          @method('patch')
                          <div class="modal-body">
                            <div class="form-group">
                              <label for="email">{{ $user->email }}</label>
                            </div>
                            <div class="form-group">
                              <select name="role" id="role" class="form-control">
                                <option value="">- Please Select Role -</option>
                                @foreach ($dropdowns['role'] as $role)
                                    <option value="{{ $role->name_value }}" {{ $user->role == $role->name_value ? 'selected' : '' }}>{{ $role->name_value }}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" name="pic" value="1" @if( $user->pic == '1') checked="checked" @endif>
                              <label class="form-check-label">Set as PIC</label>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Update">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Update --}}

                    {{-- Modal Revoke --}}
                    <div class="modal fade" id="modal-revoke{{ $user->id }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Revoke User Access</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/user/revoke/'.$user->id) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to revoke <label for="email">{{ $user->email }}</label> ?
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Revoke --}}

                    {{-- Modal Access --}}
                    <div class="modal fade" id="modal-access{{ $user->id }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Give User Access</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/user/access/'.$user->id) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Give Access to <label for="email">{{ $user->email }}</label> ?
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Revoke --}}

                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Add User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ url('/user/store') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <input type="email" class="form-control" id="email_add" name="email" placeholder="Enter email">
          </div>
          <div class="form-group">
            <select name="role" id="role" class="form-control">
              <option value="">- Please Select Role -</option>
              @foreach ($dropdowns['role'] as $role)
                  <option value="{{ $role->name_value }}" {{ old('role') == $role->name_value ? 'selected' : '' }}>{{ $role->name_value }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="pic" value="1">
            <label class="form-check-label">Set as PIC</label>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Submit">
        </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>

<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableUser").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection