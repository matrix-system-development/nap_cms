@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customers Network Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h3 class="card-title">List of Customer</h3>
              </div> --}}
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <!--alert success -->
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{{ session('status') }}</strong>
                    </div> 
                    @endif
                    <!--alert success -->
                    <!--alert warning -->
                    @if (!empty($status))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{{ $status }}</strong>
                    </div> 
                    @endif
                    <!--alert warning -->
                    <!--validasi form-->
                      @if (count($errors)>0)
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li><strong>Data Process Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                      @endif
                    <!--end validasi form-->
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-12">
                    <form class="form-inline" method="POST" action="{{ url('/mst-broadband') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="cust_code" @if (old('search_by') == "cust_code") {{ 'selected' }} @endif>Customer Code</option>
                          <option value="first_name" @if (old('search_by') == "first_name") {{ 'selected' }} @endif>Customer Name</option>
                          <option value="address" @if (old('search_by') == "address") {{ 'selected' }} @endif>Customer Address</option>
                          <option value="internet_service" @if (old('search_by') == "internet_service") {{ 'selected' }} @endif>Service</option>
                          <option value="am" @if (old('search_by') == "am") {{ 'selected' }} @endif>AM Name</option>
                          <option value="customer_status" @if (old('search_by') == "customer_status") {{ 'selected' }} @endif>Customer Status</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2" id="search_method_div">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2" id="search_value_div">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <div class="form-group mx-md-1 mb-2" id="search_value_am_div">
                        <select name="search_value_am" id="search_value_am" class="form-control form-control-sm">
                          <option value="" selected>Select AM Name</option>
                          @foreach ($AMLists as $am)
                            <option value="{{ $am['name'] }}" @if (old('search_value_am') == $am['name']) {{ 'selected' }} @endif>{{ $am['name'] }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2" id="search_value_service_div">
                        <select name="search_value_service" id="search_value_service" class="form-control form-control-sm">
                          <option value="" selected>Select Service Name</option>
                          @foreach ($serviceLists as $service)
                            <option value="{{ $service['internet_service_name'] }}" @if (old('search_value_service') == $service['internet_service_name']) {{ 'selected' }} @endif>{{ $service['internet_service_name'] }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2" id="search_value_status_div">
                        <select name="search_value_status" id="search_value_status" class="form-control form-control-sm">
                          <option value="" selected>Select Status</option>
                          @foreach ($statusLists as $status)
                            <option value="{{ $status['name'] }}" @if (old('search_value_status') == $status['name']) {{ 'selected' }} @endif>{{ $status['name'] }}</option>
                          @endforeach
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    </form>
                  </div>
                </div>
                <table id="tableCust" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Cust. Code</th>
                    <th>Cust. Name</th>
                    <th>Net Choice</th>
                    <th>Active Date</th>
                    <th>Service</th>
                    <th>Address</th>
                    <th>AM Name</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($customers as $item)
                    <tr>
                        <td>{{ $item['cust_code'] }}</td>
                        <td>{{ $item['first_name'] }}</td>
                        <td>{{ $item['net_choice'] }}</td>
                        <td>{{ date('d-m-Y', strtotime($item['activated_date'])) }}</td>
                        <td>{{ $item['internet_service'] }}</td>
                        <td>{{ $item['address'] }}</td>
                        <td>{{ $item['am'] }}</td>
                        <td>
                          <small class="badge badge-info">{{ $item['customer_status'] }}</small>
                          <br>
                          @foreach ($radUsers as $radUser)
                            @if ($item['cust_code'] == $radUser->cid)
                              @if ($radUser->disabled == '0')
                                <small class="badge badge-success">Link Active</small>
                              @else
                                <small class="badge badge-danger">Link Not Active</small>
                              @endif
                            @endif
                          @endforeach
                          <br>
                          @foreach ($quarantines as $quarantine)
                            @if ($item['cust_code'] == $quarantine->cid)
                              <small class="badge badge-dark">Quarantine: YES</small>
                            @endif
                          @endforeach
                        </td>
                        <td>
                          <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Action
                              </button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ url('/mst-broadband/net-status/'.$item['cust_code']) }}">Network Status</a>
                                <a class="dropdown-item" href="{{ url('/mst-broadband/fin-status/'.$item['cust_code']) }}">Financial Info</a>
                              </div>
                            </div>
                          </div>
                        </td>
                    </tr>

                    <!-- Modal Detail-->
                    {{-- <div class="modal fade" id="modalDetail{{ $item->CardCode }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Detail Info Customer</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Customer Code</label>
                                            <br>
                                            {{ $item->CardCode }}
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <br>
                                            {{ $item->CardName }}
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <br>
                                            {{ $item->Address }}
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <br>
                                            {{ $item->PhoneNo }}
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <br>
                                            {{ $item->Email }}
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Service</label>
                                            <br>
                                            {{ $item->last_service }}
                                        </div>
                                        <div class="form-group">
                                            <label>Asuransi</label>
                                            <br>
                                            {{ $item->asuransi }}
                                        </div>
                                        <div class="form-group">
                                            <label>Billing Pay Type</label>
                                            <br>
                                            {{ $item->BPT }}
                                        </div>
                                        <div class="form-group">
                                            <label>AM Name</label>
                                            <br>
                                            {{ $item->am }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
                            </div>
                        </div>
                        </div>
                    </div>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableCust").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": false,
      "order": [0, "asc"],
      // "oLanguage": {
      //   "sSearch": "Search by CID or Username"
      //   }
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });

    $("#search_value_am_div").hide();
    $("#search_value_service_div").hide();
    $("#search_value_status_div").hide();

    $('#search_by').on('change', function() {
      if ( this.value == 'am')
      {
        $("#search_value_am_div").show();
        $("#search_value_div").hide();
        $("#search_value_service_div").hide();
        $("#search_value_status_div").hide();
        $("#search_method_div").hide();
      }
      else if ( this.value == 'internet_service')
      {
        $("#search_value_service_div").show();
        $("#search_value_am_div").hide();
        $("#search_value_status_div").hide();
        $("#search_value_div").hide();
        $("#search_method_div").hide();
      }
      else if ( this.value == 'customer_status')
      {
        $("#search_value_status_div").show();
        $("#search_value_service_div").hide();
        $("#search_value_am_div").hide();
        $("#search_value_div").hide();
        $("#search_method_div").hide();
      }
      else{
        $("#search_value_service_div").hide();
        $("#search_value_am_div").hide();
        $("#search_value_status_div").hide();
        $("#search_value_div").show();
        $("#search_method_div").show();
      }
    });
  });
</script>
@endsection