@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Quarantines Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h3 class="card-title">List of Quarantine</h3>
              </div> --}}
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    {{-- <div class="col-sm-6">
                        <a href="{{url('/log')}}" class="btn btn-success btn-sm" title="Refresh Menu">
                            <i class="fas fa-sync-alt"></i>
                        </a>
                    </div> --}}
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <div class="row mb-2">
                  <div class="col-12">
                    <form class="form-inline" method="POST" action="{{ url('/quarantines') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="cid" @if (old('search_by') == "cid") {{ 'selected' }} @endif>Customer Code</option>
                          <option value="firstname" @if (old('search_by') == "firstname") {{ 'selected' }} @endif>Customer Name</option>
                          <option value="Sales_Employee" @if (old('search_by') == "Sales_Employee") {{ 'selected' }} @endif>Sales Name</option>
                          <option value="Regional" @if (old('search_by') == "Regional") {{ 'selected' }} @endif>Regional</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    </form>
                  </div>
                </div>
                <table id="tableQua" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>ID</th>
                    <th>Cust Info</th>
                    <th>Sales Name</th>
                    <th>Regional</th>
                    <th>Temporary Unblock</th>
                    <th>Description</th>
                    <th>Created By</th>
                    <th>Created At</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($quarantines as $data)
                        <tr>
                            <td>{{ $data->id }}</td>
                            <td>
                              <b>{{ $data->firstname }}</b><br>  
                              {{ $data->cid }} <br>
                              @if ($data->is_approval=='1')
                                <b><i><p class="text-danger">Need Approval</p></i></b>
                              @endif
                            </td>
                            <td>{{ $data->Sales_Employee }}</td>
                            <td>{{ $data->Regional }}</td>
                            <td>
                                @if ($data->temporary_unblock_flag == '0')
                                    <b><i>NO</i></b>
                                @else
                                    <b><i>YES</i></b><br>
                                    <i>until: {{ $data->temporary_unblock_exp }}</i><br>
                                @endif
                            </td>
                            <td>{{ $data->message }}</td>
                            <td>{{ $data->created_by }}</td>
                            <td>{{ $data->created_at }}</td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableQua").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "order": [0,"desc"],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection