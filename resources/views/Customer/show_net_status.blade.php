@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            {{-- <h1>Customer Network Status</h1> --}}
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                <div class="card">
                    <div class="card-header p-2">
                        Network Status Info
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        @foreach ($userRads as $userRad)
                        <div class="row">
                            <div class="col">
                                <strong>Customer Code</strong>
                                <p class="text-muted">
                                {{ $userRad->cid }}
                                @if ($userRad->disabled == '0')
                                    <small class="badge badge-success">Link Active</small>
                                @elseif(($userRad->disabled == $userRad->username))
                                    <small class="badge badge-danger">Link Not Active</small>
                                @endif
                                </p>
                            </div>
                            <div class="col">
                                <strong>Username</strong>
                                <p class="text-muted">
                                {{ $userRad->username }}
                                </p>
                            </div>
                            <div class="col">
                                <strong>Password</strong>
                                <p class="text-muted">
                                {{ $userRad->value }}
                                </p>
                            </div>
                            <div class="col">
                                <strong>Groupname</strong>
                                <p class="text-muted">
                                {{ $userRad->groupname }}
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                <div class="card">
                    <div class="card-header p-2">
                        Quarantine Info
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        @foreach ($quarantines as $quarantine)
                        <div class="row">
                            <div class="col">
                                <strong>Temporary Unblock</strong>
                                <p class="text-muted">
                                @if ($quarantine->temporary_unblock_flag == '1')
                                    <small class="badge badge-info">YES</small>
                                @else
                                    <small class="badge badge-info">NO</small>
                                @endif
                                </p>
                            </div>
                            <div class="col">
                                <strong>Temporary Unblock Expired at</strong>
                                <p class="text-muted">
                                {{ $quarantine->temporary_unblock_exp }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <strong>Created By</strong>
                                <p class="text-muted">
                                {{ $quarantine->created_by }}
                                </p>
                            </div>
                            <div class="col">
                                <strong>Created At</strong>
                                <p class="text-muted">
                                {{ $quarantine->updated_at }}
                                </p>
                            </div>
                            <div class="col">
                                <strong>Message</strong>
                                <p class="text-muted">
                                {{ $quarantine->message }}
                                </p>
                            </div>
                        </div>   
                        @endforeach
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableCurrentBilling").DataTable({
      "responsive": true, 
      //"lengthChange": true, 
      "autoWidth": false,
      "order": [[ 0, "asc" ]],
      "dom": '<"top"i>rt<"clear">',
      "columnDefs": [
            { "width": "60%", "targets": 1 }
        ],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection