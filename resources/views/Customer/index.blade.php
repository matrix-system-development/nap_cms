@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customers Network Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h3 class="card-title">List of Customer</h3>
              </div> --}}
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <!--alert success -->
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{{ session('status') }}</strong>
                    </div> 
                    @endif
                    <!--alert success -->
                    <!--validasi form-->
                      @if (count($errors)>0)
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li><strong>Data Process Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                      @endif
                    <!--end validasi form-->
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-12">
                    <form class="form-inline" method="POST" action="{{ url('/mon-cust') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="user_rads.cid" @if (old('search_by') == "user_rads.cid") {{ 'selected' }} @endif>Customer Code</option>
                          <option value="user_rads.firstname" @if (old('search_by') == "user_rads.firstname") {{ 'selected' }} @endif>Customer Name</option>
                          <option value="user_rads.groupname" @if (old('search_by') == "user_rads.groupname") {{ 'selected' }} @endif>Service Type</option>
                          <option value="user_rads.disabled" @if (old('search_by') == "user_rads.disabled") {{ 'selected' }} @endif>Network Status</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    </form>
                  </div>
                </div>
                <table id="tableCust" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>Cust. Code</th>
                    <th>Customer Info</th>
                    {{-- <th>Password</th> --}}
                    <th>Service Type</th>
                    <th>Network Status</th>
                    <th>Temp Unblock</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($UserRads as $item)
                    <tr>
                        {{-- <td>{{ $no++ }}</td> --}}
                        <td>{{ $item->cid }}</td>
                        <td><b>{{ $item->firstname }}</b> <br> {{ $item->username }}</td>
                        {{-- <td>{{ $item->value }}</td> --}}
                        @php
                            if($item->max_temp_unblock==null){
                              $max="0";
                              $disabled="";
                              $notes="";
                            }
                            elseif($item->max_temp_unblock==$max_temp){
                              $max=$item->max_temp_unblock;
                              $disabled="disabled";
                              $notes=" User has reached the maximum temporary unblock limit this month";
                            }
                            else{
                              $max=$item->max_temp_unblock;
                              $disabled="";
                              $notes="";
                            }
                        @endphp
                        <td>{{ $item->groupname}}</td>
                        <td>
                          @if ($item->disabled =='0')
                            @if ($item->temporary_unblock_exp=='')
                              <small class="badge badge-success">ACTIVE</small>
                            @else
                              <small class="badge badge-success">ACTIVE</small> <br>
                              <i>until: {{ $item->temporary_unblock_exp }}</i>
                            @endif
                          @else
                              <small class="badge badge-danger">INACTIVE</small>
                          @endif
                        </td>
                        <td>{{ $max }} <i>Times</i></td>
                        <td>
                          @if (($item->cid == 'NULL')||($item->cid == ''))
                            <font color='red'><i>Invalid Customer ID</i></font>
                          @else
                            @if (auth()->user()->role=='Customer Care' || auth()->user()->role=='Super Admin')
                              @if ($item->disabled=='0')
                                <button title="Block Network" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-block{{ $item->cid }}">
                                    <i class="fas fa-lock"></i> Block
                                </button>
                              @else
                                <div class="dropdown">
                                  <button class="btn btn-sm btn-success dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-unlock-alt"></i> Unblock
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item btn-sm btn-success" type="button" data-toggle="modal" data-target="#modal-unblock{{ $item->cid }}">3 Days</button>
                                    <button class="dropdown-item btn-sm btn-success" type="button" data-toggle="modal" data-target="#modal-unblock2{{ $item->cid }}">7 Days</button>
                                  </div>
                                </div>
                              @endif
                            @elseif(auth()->user()->role=='User Finance' || auth()->user()->role=='User Network')
                              @if ($item->disabled=='0')
                                <button title="Block Network" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-block{{ $item->cid }}">
                                  <i class="fas fa-lock"></i> Block
                                </button>
                              @else
                                <div class="dropdown">
                                  <button class="btn btn-sm btn-success dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-unlock-alt"></i> Unblock
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item btn-sm btn-success" type="button" data-toggle="modal" data-target="#modal-unblock{{ $item->cid }}">3 Days</button>
                                  </div>
                                </div>
                              @endif
                            @else
                              <i>No Action Needed</i>
                            @endif
                          @endif
                        </td>
                    </tr>

                    {{-- Modal Block --}}
                    <div class="modal fade" id="modal-block{{ $item->cid }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Block Customer Network</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/mon-cust/block/'.$item->cid) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to Block <label for="username">{{ $item->cid.' -> '.$item->username }}</label> ?
                            </div>
                            <div class="form-group">
                              <label>Remarks</label>
                              <textarea class="form-control" rows="3" placeholder="Enter Remarks" name="remark_block"></textarea>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Block --}}

                    {{-- Modal UnBlock 1--}}
                    <div class="modal fade" id="modal-unblock{{ $item->cid }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Unblock Customer Network (3 Days)</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/mon-cust/unblock/'.$item->cid) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to Unblock <label for="username">{{ $item->cid.' -> '.$item->username }}</label> ?
                            </div>
                            {{-- <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="temp_unblock" name="temp_unblock" {{ $disabled }}>
                              <label class="form-check-label" for="exampleCheck1">Temporary Unblock</label>
                            </div> --}}
                            <div class="form-group">
                              <textarea class="form-control" rows="3" placeholder="Enter Remarks" name="remark_unblock"></textarea>
                            </div>
                            <div class="form-group">
                              <font color="red"><i>{{ $notes }}</i></font>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal UnBlock 1--}}

                    {{-- Modal UnBlock 2--}}
                    <div class="modal fade" id="modal-unblock2{{ $item->cid }}">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Unblock Customer Network (7 Days)</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/mon-cust/unblock2/'.$item->cid) }}" enctype="multipart/form-data" method="GET">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              Are you sure want to Unblock <label for="username">{{ $item->cid.' -> '.$item->username }}</label> ?
                            </div>
                            <div class="form-group">
                              <textarea class="form-control" rows="3" placeholder="Enter Remarks" name="remark_unblock"></textarea>
                            </div>
                            <div class="form-group">
                              <font color="red"><i>{{ $notes }}</i></font>
                            </div>
                            <div class="form-group">
                              <font color="red"><b><i>This action need Customer Care approval, if not approved this customer would be temporary unblock in 3 Days</i></b></font>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Submit">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal UnBlock 2--}}

                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableCust").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": false,
      "order": [[ 3, "desc" ],[0,"desc"]],
      // "oLanguage": {
      //   "sSearch": "Search by CID or Username"
      //   }
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection