@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Log Approval Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Detail Log</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <b>Cust Code:</b> {{ $custInfo->cust_code }}
                        </div>
                        <div class="form-group">
                            <b>Cust Name:</b> {{ $custInfo->firstname }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <b>Request At:</b> {{ date('d-M-Y H:i:s', strtotime($custInfo->request_date)) }}
                        </div>
                        <div class="form-group">
                            <b>Request By:</b> {{ $custInfo->request_by }}
                        </div>
                    </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="tableLogApp" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Action By</th>
                                <th>Created At</th>
                            </tr>
                            </thead>
                            <tbody>
                              @php
                                $no=1;
                              @endphp
                              @foreach ($detailLogs as $data)
                                  <tr>
                                      <td>{{ $data->action }}</td>
                                      <td>{{ $data->action_by }}</td>
                                      <td>{{ date('d-M-Y H:i:s', strtotime($data->created_at)) }}</td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableLogApp").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      //"order": [0,"desc"],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection