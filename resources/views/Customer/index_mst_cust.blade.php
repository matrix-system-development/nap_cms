@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customers Network Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h3 class="card-title">List of Customer</h3>
              </div> --}}
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <!--alert success -->
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{{ session('status') }}</strong>
                    </div> 
                    @endif
                    <!--alert success -->
                    <!--validasi form-->
                      @if (count($errors)>0)
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li><strong>Data Process Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                      @endif
                    <!--end validasi form-->
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-12">
                    <form class="form-inline" method="POST" action="{{ url('/mst-cust') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="CardCode" @if (old('search_by') == "CardCode") {{ 'selected' }} @endif>Customer Code</option>
                          <option value="CardName" @if (old('search_by') == "CardName") {{ 'selected' }} @endif>Customer Name</option>
                          <option value="CustAddress" @if (old('search_by') == "CustAddress") {{ 'selected' }} @endif>Customer Address</option>
                          <option value="Service" @if (old('search_by') == "Service") {{ 'selected' }} @endif>Service</option>
                          <option value="Sales_Employee" @if (old('search_by') == "Sales_Employee") {{ 'selected' }} @endif>AM Name</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    </form>
                  </div>
                </div>
                <table id="tableCust" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Cust. Code</th>
                    <th>Cust. Name</th>
                    <th>Service</th>
                    <th>Cust. Address</th>
                    <th>AM Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($customers as $item)
                    <tr>
                        <td>{{ $item->CardCode }}</td>
                        <td>{{ $item->CardName }}</td>
                        <td>{{ $item->last_service }}</td>
                        <td>{{ $item->CustAddress }}</td>
                        <td>{{ $item->am }}</td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modalDetail{{ $item->CardCode }}">
                                Detail
                            </button>
                        </td>
                    </tr>

                    <!-- Modal Detail-->
                    <div class="modal fade" id="modalDetail{{ $item->CardCode }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Detail Info Customer</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Customer Code</label>
                                            <br>
                                            {{ $item->CardCode }}
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <br>
                                            {{ $item->CardName }}
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <br>
                                            {{ $item->Address }}
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <br>
                                            {{ $item->PhoneNo }}
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <br>
                                            {{ $item->Email }}
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Service</label>
                                            <br>
                                            {{ $item->last_service }}
                                        </div>
                                        <div class="form-group">
                                            <label>Asuransi</label>
                                            <br>
                                            {{ $item->asuransi }}
                                        </div>
                                        <div class="form-group">
                                            <label>Billing Pay Type</label>
                                            <br>
                                            {{ $item->BPT }}
                                        </div>
                                        <div class="form-group">
                                            <label>AM Name</label>
                                            <br>
                                            {{ $item->am }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
                            </div>
                        </div>
                        </div>
                    </div>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableCust").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": false,
      "order": [[ 3, "desc" ],[0,"desc"]],
      // "oLanguage": {
      //   "sSearch": "Search by CID or Username"
      //   }
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection