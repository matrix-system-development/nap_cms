@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List of Approval Temp Unblock Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Approval</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    {{-- <div class="col-sm-6">
                        <a href="{{url('/log')}}" class="btn btn-success btn-sm" title="Refresh Menu">
                            <i class="fas fa-sync-alt"></i>
                        </a>
                    </div> --}}
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <table id="tableApv" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>ID</th>
                    <th>Cust Info</th>
                    <th>Created Info</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($ApprovalLists as $data)
                        <tr>
                            <td>{{ $data->id }}</td>
                            <td>
                                <b>{{ $data->firstname }}</b>
                                <br>
                                {{ $data->cust_code }}
                                <br>
                                <a href="{{url('/apv-temp-unblock/log/'.$data->id)}}" class="btn btn-info btn-xs" target="_BLANK">
                                  <i class="fa fa-info-circle" aria-hidden="true"></i> Detail Log
                                </a>
                            </td>
                            <td>
                                <b>by: {{ $data->request_by }}</b>
                                <br>
                                <i>at: {{ date('d-M-Y H:i:s', strtotime($data->request_date)) }}</i>
                                <br>
                                <i>unblock period: {{ $data->tmp_unblock_period }}</i>
                            </td>
                            <td>
                                @if ($data->approval_status == '1')
                                  <small class="badge badge-info">REQUESTED</small>
                                @elseif ($data->approval_status == '2')
                                  <small class="badge badge-success">APPROVED</small>  
                                @elseif ($data->approval_status == '3')
                                  <small class="badge badge-danger">REJECTED</small> 
                                @else
                                  <small class="badge badge-warning">NO ACTION</small> 
                                @endif
                                
                            </td>
                            <td>
                              @if ($data->approval_status == '1')
                                <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-approval{{ $data->id }}">
                                  <i class="fa fa-check" aria-hidden="true"></i> Approved
                                </button>
                                <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-reject{{ $data->id }}">
                                  <i class="fa fa-times" aria-hidden="true"></i> Rejected
                                </button>
                              @endif 
                                
                                {{-- Modal Reject--}}
                                <div class="modal fade" id="modal-reject{{ $data->id }}">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                      <div class="modal-header">
                                          <h4 class="modal-title">Reject Request Temp Unblock</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      <form action="{{ url('/apv-temp-unblock/reject/'.$data->id.'/'.$data->cust_code) }}" enctype="multipart/form-data" method="GET">
                                      @csrf
                                      <div class="modal-body">
                                        <div class="form-group">
                                          Are you sure want to Reject this Request ?
                                        </div>
                                      </div>
                                      <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                        <input type="submit" class="btn btn-primary" value="Yes">
                                      </div>
                                      </form>
                                      </div>
                                      <!-- /.modal-content -->
                                  </div>
                                <!-- /.modal-dialog -->
                                </div>
                                {{-- Modal Reject--}}

                                {{-- Modal Approval--}}
                                <div class="modal fade" id="modal-approval{{ $data->id }}">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                      <div class="modal-header">
                                          <h4 class="modal-title">Approval Request Temp Unblock</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      <form action="{{ url('/apv-temp-unblock/approve/'.$data->id.'/'.$data->cust_code) }}" enctype="multipart/form-data" method="GET">
                                      @csrf
                                      <div class="modal-body">
                                        <div class="form-group">
                                          Are you sure want to Approve this Request ?
                                        </div>
                                      </div>
                                      <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                        <input type="submit" class="btn btn-primary" value="Yes">
                                      </div>
                                      </form>
                                      </div>
                                      <!-- /.modal-content -->
                                  </div>
                                <!-- /.modal-dialog -->
                                </div>
                                {{-- Modal Approval--}}
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableApv").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "order": [0,"desc"],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection