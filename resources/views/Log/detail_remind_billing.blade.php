@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Billing Reminder Detail Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Billing Reminder Detail</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Check Data Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div> 
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card card-primary">
                      <div class="card-header bg-light">
                        <h3 class="card-title"><b>Mail Info</b></h3>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-sm-6">
                            <strong>ID</strong>
                            <p class="text-muted">
                              {{ $response['_id'] }}
                            </p>
                            <strong>From</strong>
                            <p class="text-muted">
                              {{ $response['headers']['From'] }}
                            </p>
                            <strong>Subject</strong>
                            <p class="text-muted">
                              {{ $response['subject'] }}
                            </p>
                          </div>
                          <div class="col-sm-6">
                            <strong>Date Sent</strong>
                            <p class="text-muted">
                              {{ date('Y-m-d H:i:s', $response['ts']) }}
                            </p>
                            <strong>To</strong>
                            <p class="text-muted">
                              {{ $response['headers']['To'] }}
                            </p>
                          </div>
                        </div>
                      </div>
                      <!-- /.card-body -->
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card card-primary">
                      <div class="card-header bg-light">
                        <h3 class="card-title"><b>Mail Content</b></h3>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-sm-12">
                            {!! $response['html'] !!}
                          </div>
                        </div>
                      </div>
                      <!-- /.card-body -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script type="text/javascript">
  
</script>
@endsection