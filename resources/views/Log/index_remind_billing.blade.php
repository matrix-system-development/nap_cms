@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Billing Reminder Logs Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Billing Reminder Logs From <b><i>{{ date('d F Y', strtotime($label_date_start)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_finish)) }}</i></b></h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row pb-3">
                    <div class="col-sm-6">
                        <a href="{{url('/remind-billing-log')}}" class="btn btn-success btn-sm" title="Refresh Menu">
                            <i class="fas fa-sync-alt"></i> Refresh Page
                        </a>
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-filter">
                          <i class="fas fa-filter"></i> Filter
                        </button>

                        {{-- Modal Filter --}}
                        <div class="modal fade" id="modal-filter">
                          <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Filter by Email Sent Date</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <form action="{{ url('/remind-billing-log') }}" enctype="multipart/form-data" method="POST">
                              @csrf
                              <div class="modal-body">
                                <div class="form-group">
                                  <label for="created_at">From</label>
                                  <input type="date" id="date_start" name="date_start" class="form-control" value="{{ old('date_start') }}">
                                </div>
                                <div class="form-group">
                                  <label for="created_at">Until</label>
                                  <input type="date" id="date_finish" name="date_finish" class="form-control" value="{{ old('date_finish') }}">
                                </div>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Show Data">
                              </div>
                              </form>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                        <!-- /.modal-dialog -->
                        </div>
                        {{-- Modal Filter --}}
                    </div>
                    <div class="col-sm-6">
                      @if ($status == 'error')
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>Invalid API Key, Please Contact your Administrator</strong>
                        </div>
                      @else
                      @endif
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <table id="tableLog" class="table table-bordered table-striped" style="width:100%">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>Recipent</th>
                    <th>Sent Time</th>
                    <th>Subject</th>
                    <th>Result</th>
                    <th>Sender</th>
                    <th>Detail</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($result as $data)
                    <tr>
                        {{-- <td>{{ $no++ }}</td> --}}
                        <td>{{ $data->email }}</td>
                        <td>{{ date('Y-m-d H:i:s', $data->ts) }}</td>
                        <td>{{ $data->subject }}</td>
                        <td>
                          @php
                              if($data->state == 'sent'){
                                $state= $data->state;
                              }
                              elseif($data->state == 'rejected'){
                                $reject=$data->reject;
                                if (is_array($reject) || is_object($reject)){
                                  foreach($reject as $key => $value){
                                    if($key == 'reason'){
                                      $state= $data->state."-".$value;
                                    }
                                  }
                                }
                              }
                              else{
                                $state= $data->state."-".$data->bounce_description;
                              }
                          @endphp
                          {{ $state }}
                        </td>
                        <td>{{ $data->sender }}</td>
                        <td>
                          @if ($data->state == 'sent')
                            <a href="{{ url('/remind-billing-detail/'.$data->_id) }}" class="btn btn-info btn-sm" target="_blank">Detail</a>
                          @endif
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  var minDate, maxDate;
 
  // Custom filtering function which will search data in column four between two values
  $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          var min = minDate.val();
          var max = maxDate.val();
          var date = new Date( data[4] );
    
          if (
              ( min === null && max === null ) ||
              ( min === null && date <= max ) ||
              ( min <= date   && max === null ) ||
              ( min <= date   && date <= max )
          ) {
              return true;
          }
          return false;
      }
  );

  $(document).ready(function() {

    // Create date inputs
    minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });
  
    // DataTables initialisation
    var table = $("#tableLog").DataTable({
      dom: 'Bfrtip',
      "responsive": true,
      "scrollX": true,
      "order": [],
      buttons: [{
        extend: 'excel',
        className: "btn btn-success btn-sm",
        text: 'Export to Excel',
        exportOptions: {
          columns: [ 0, 1, 2, 3, 4 ]
        }
      }]
    });

    // Refilter the table
    $('#min, #max').on('change', function () {
        table.draw();
    });
  });
</script>
@endsection