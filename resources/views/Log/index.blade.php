@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Logs Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Logs History From <b><i>{{ date('d F Y', strtotime($date_start)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_finish)) }}</i></b></h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row mb-3">
                  <div class="col-8 ">
                    <form class="form-inline" method="POST" action="{{ url('/log-search') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="cid_log" @if (old('search_by') == "cid_log") {{ 'selected' }} @endif>Customer Code</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    {{--   <button title="Add User" type="button" class="btn btn-primary btn-sm mb-2 mx-sm-1" data-toggle="modal" data-target="#modal-add">
                        <i class="fas fa-user-plus"></i>
                      </button> --}}
                    </form>
                  </div>
                    <div class="col-sm-4 text-right">
                      
                        <a href="{{url('/log')}}" class="btn btn-success btn-sm">
                            <i class="fas fa-sync-alt"></i> Refresh Page
                        </a>
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-filter">
                          <i class="fas fa-filter"></i> Filter
                        </button>
                        <form action="{{ url('/export-log') }}" method="post" class="d-inline">
                          @csrf
                          <input type="hidden" name="date_start" id="date_start" value="{{ $date_start }}">
                          <input type="hidden" name="date_finish" id="date_finish" value="{{ $date_finish }}">
                          <input type="hidden" name="action_by" id="action_by" value="{{ $actionBy }}">
                          <button type="submit" class="btn btn-success btn-sm"><i class="far fa-file-excel"></i> Export To Excel</button>
                        </form>
                    </div>

                    {{-- Modal Filter --}}
                    <div class="modal fade" id="modal-filter">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Data Filter</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/log') }}" enctype="multipart/form-data" method="POST">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              <label for="created_at">Created At From</label>
                              <input type="date" id="date_start" name="date_start" class="form-control" value="{{ old('date_start') }}">
                            </div>
                            <div class="form-group">
                              <label for="created_at">Created At Until</label>
                              <input type="date" id="date_finish" name="date_finish" class="form-control" value="{{ old('date_finish') }}">
                            </div>
                            <div class="form-group">
                              <label>Action By</label>
                                <select name="action_by" id="action_by" class="form-control">
                                  <option value="">- Select Action By -</option>
                                  @foreach ($dropdowns['action by'] as $action_by)
                                      <option value="{{ $action_by->action_by_log }}">{{ $action_by->action_by_log }}</option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Show Data">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Filter --}}
                    
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <div class="row">
                  
                </div>
                <table id="tableLog" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>ID</th>
                    <th>Cust. ID</th>
                    <th>Username</th>
                    <th>Message</th>
                    <th>Created At</th>
                    <th>Created By</th>
                    <th>Action By</th>
                    <th>Remarks</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($logs as $item)
                    <tr>
                        {{-- <td>{{ $no++ }}</td> --}}
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->cid_log }}</td>
                        <td>{{ $item->username_log }}</td>
                        <td>{{ $item->message_log }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td>{{ $item->created_by_log }}</td>
                        <td>{{ $item->action_by_log }}</td>
                        <td><b><i>{{ $item->remarks_log }}</i></b></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  var minDate, maxDate;
 
  // Custom filtering function which will search data in column four between two values
  $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          var min = minDate.val();
          var max = maxDate.val();
          var date = new Date( data[4] );
    
          if (
              ( min === null && max === null ) ||
              ( min === null && date <= max ) ||
              ( min <= date   && max === null ) ||
              ( min <= date   && date <= max )
          ) {
              return true;
          }
          return false;
      }
  );

  $(document).ready(function() {

    // Create date inputs
    minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });
  
    // DataTables initialisation
    var table = $("#tableLog").DataTable({
      //dom: 'Bfrtip',
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "order": [],
      //"buttons": ["excel"]
    });

    // Refilter the table
    $('#min, #max').on('change', function () {
        table.draw();
    });
  });
</script>
@endsection