@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     {{--  <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Monitoring PG - Midtrans</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Payment Midtrans From <b><i>{{ date('d F Y', strtotime($date_start)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_finish)) }}</i></b></h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                      
                        <a href="{{url('/mt-monitor')}}" class="btn btn-success btn-sm">
                            <i class="fas fa-sync-alt"></i> Refresh Page
                        </a>
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-filter">
                          <i class="fas fa-filter"></i> Filter
                        </button>
                        <form action="{{ url('/export-mt-trans') }}" method="post" class="d-inline">
                          @csrf
                          <input type="hidden" name="date_start" id="date_start" value="{{ $date_start }}">
                          <input type="hidden" name="date_finish" id="date_finish" value="{{ $date_finish }}">
                          <input type="hidden" name="trans_status" id="trans_status" value="{{ $transStatus }}">
                          <button type="submit" class="btn btn-success btn-sm"><i class="far fa-file-excel"></i> Export To Excel</button>
                        </form>
                    </div>

                    {{-- Modal Filter --}}
                    <div class="modal fade" id="modal-filter">
                      <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Data Filter</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <form action="{{ url('/mt-monitor') }}" enctype="multipart/form-data" method="POST">
                          @csrf
                          <div class="modal-body">
                            <div class="form-group">
                              <label for="created_at">Created At From</label>
                              <input type="date" id="date_start" name="date_start" class="form-control" value="{{ old('date_start') }}">
                            </div>
                            <div class="form-group">
                              <label for="created_at">Created At Until</label>
                              <input type="date" id="date_finish" name="date_finish" class="form-control" value="{{ old('date_finish') }}">
                            </div>
                            <div class="form-group">
                              <label>Transaction Status</label>
                                <select name="trans_status" id="trans_status" class="form-control">
                                    <option value="">All Status</option>
                                    @foreach ($dropdowns['trans status'] as $dropitem)
                                        <option value="{{ $dropitem->name_value }}">{{ $dropitem->name_value }}</option>
                                    @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Show Data">
                          </div>
                          </form>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                    </div>
                    {{-- Modal Filter --}}
                    
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div>
                </div>
                <table id="tableMT" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    {{-- <th>No</th> --}}
                    <th>No</th>
                    <th>Cust Info</th>
                    <th>Trans Info</th>
                    <th>Nominal Pay</th>
                    <th>Update At</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                      $no=1;
                    @endphp
                    @foreach ($mtTrans as $item)
                    <tr>
                        {{-- <td>{{ $no++ }}</td> --}}
                        <td>{{ $no++ }}</td>
                        <td>
                            <b>{{ $item->firstname }}</b><br>
                            {{ $item->cust_code }}
                        </td>
                        <td>
                            <b>Inv No:</b> {{ $item->DocNum }} <br>
                            <b>ID:</b> {{ $item->order_id }} <br>
                            <b>Pay Method:</b> {{ $item->via }} <br>
                            <small class="badge badge-info">{{ strtoupper($item->transaction_status) }}</small>
                        </td>
                        <td>{{ "IDR ". number_format($item->nominal_payment,2,",",".") }}</td>
                        <td>{{ date('Y-m-d H:i:s', strtotime($item->updated_at)) }}</td>
                        <td><a href={{ url('/mt-monitor/detail/'.$item->order_id) }} class="btn btn-xs btn-success" target="_BLANK">Detail</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  var minDate, maxDate;
 
  // Custom filtering function which will search data in column four between two values
  $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          var min = minDate.val();
          var max = maxDate.val();
          var date = new Date( data[4] );
    
          if (
              ( min === null && max === null ) ||
              ( min === null && date <= max ) ||
              ( min <= date   && max === null ) ||
              ( min <= date   && date <= max )
          ) {
              return true;
          }
          return false;
      }
  );

  $(document).ready(function() {

    // Create date inputs
    minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });
  
    // DataTables initialisation
    var table = $("#tableMT").DataTable({
      //dom: 'Bfrtip',
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "order": [],
      //"buttons": ["excel"]
    });

    // Refilter the table
    $('#min, #max').on('change', function () {
        table.draw();
    });
  });
</script>
@endsection