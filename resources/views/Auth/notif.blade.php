<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    <link rel="shortcut icon" href="{{ asset('dist2/img/Matrix Black.png') }}" />
	
	<style>
	body {
		min-height: 100%;
		background-color: #151A48;
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
	}
	h4 {
	  color: white;
	}
	footer{
	  color: white;
	}
	</style>
  </head>
  <body>
    <div class="container">
        <div class="row pt-4 justify-content-center pb-4 text-center">
			{{-- <img src="{{ asset('dist2/img/FINAL-min.jpg') }}" class="img-fluid" alt="" > --}}
			<picture>
				<source media="(min-width:900px)" srcset="{{ asset('dist2/img/web-notif.png') }}" type="image/svg+xml">
				<source media="(min-width:300px)" srcset="{{ asset('dist2/img/mobile-notif.png') }}" type="image/svg+xml">
				<img src="{{ asset('dist2/img/web-notif.png') }}" alt="Info" class="img-fluid img-thumbnail">
			</picture>
        </div>
    </div>
	<center></center>
  </body>
</html>
