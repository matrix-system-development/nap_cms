@extends('Layouts.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Billing Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
             {{--  <div class="card-header">
                <h3 class="card-title">List of Billings: <b>{{ date('F Y', strtotime($current_month)) }}</b></h3>
              </div> --}}
              

             
              <!-- /.card-header -->
              <div class="card-body">
               
                <div class="row">
                    <div class="col-sm-6">
                  @if(session('success'))
                      <div class="alert alert-success">
                          {{ session('success') }}
                      </div>
                  @endif
                  @if(session('warning'))
                      <div class="alert alert-warning">
                          {{ session('warning') }}
                      </div>
                  @endif

                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div> 
                </div>
                @if ($type  == "Dept Head")
                <div class="row">
                  <div class="col-md-4">
                    
                    <a class="btn btn-info btn-sm" href="{{ url('/billing-adjustment') }} " >Updated request</a>
                 
                  </div>
                </div>
                @else
                <div class="row">
                  <div class="col-md-4">
                    <button title="Block Network" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-filter">
                      <i class="fas fa-search"></i> Find by Invoice No
                    </button>
                    <a class="btn btn-info btn-sm" href="{{ url('/billing-adjustment') }} " >Updated request</a>
                 
                  </div>
                </div>
                @endif
                <div class="row mb-3">
                    
                  </div>
                <div class="row mb-3">
                  
                </div>
                
                <table id="tableBilling" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Invoice No</th>
                      <th>Invoice Period</th>
                      <th>Cust. Info</th>
                      <th>Current Outstanding</th>
                      @if ($type  == "Dept Head")
                      <th>Request By</th>
                        @else
                        <th>Remarks</th>
                        @endif
                      <th>Status</th>
                      {{-- <th>Last Update</th> --}}
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($AdjusmentApproval as $data)
                      <tr>
                        <td>{{ $data->docnum }}</td>
                        <td>{{ date('Y-m-d', strtotime($data->DocDate)) }}</td>
                        <td>
                            <b>{{ $data->CardCode }}</b>
                            <br> {{ $data->CardName }}
                        </td>
                        <td><i>{{ $data->SOCurrency." ". number_format($data->Outstanding,2,",",".") }}</i></td>
                        @if ($type  == "Dept Head")
                            <td><b>{{$requestBy = str_replace('@napinfo.co.id', '', $data->request_by)}}
                            </b> 
                                <br>{{ $data->remark_request }}</td>
                        @else
                            
                            <td>@if ($data->reason_reject == null)
                                {{$data->remark_request}}
                                @else
                                {{ $data->reason_reject }}</td>
                                @endif</td>
                        @endif
                        <td>
                            @if ($data->status == '0')
                            <small class="badge badge-warning text-light">Pending</small>
                            @endif
                            @if ($data->status == '1')
                            <small class="badge badge-success">Aproved</small>
                            @endif
                            @if ($data->status == '2')
                            <small class="badge badge-danger">Reject</small>
                            @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {{-- Modal Filter --}}
  <div class="modal fade" id="modal-filter">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Find Invoice No</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ url('/find-billing') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <input type="number" class="form-control" name="inv_no"/>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Submit">
        </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
  </div>
  {{-- Modal Filter --}}


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableBilling").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": true,
      //"dom": '<"top"fli>rt<"bottom"p><"clear">',
      //"pagingType": "first_last_numbers",
      "order": [[ 0, "asc" ],[ 1, "asc" ]],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });

  $("#datepicker").datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months"
});
</script>
@endsection