@extends('Layouts.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Billing Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
             {{--  <div class="card-header">
                <h3 class="card-title">List of Billings: <b>{{ date('F Y', strtotime($current_month)) }}</b></h3>
              </div> --}}
             
              <!-- /.card-header -->
              <div class="card-body">
               
                <div class="row">
                    <div class="col-sm-6">
                  @if(session('success'))
                      <div class="alert alert-success">
                          {{ session('success') }}
                      </div>
                  @endif
                  @if(session('warning'))
                      <div class="alert alert-warning">
                          {{ session('warning') }}
                      </div>
                  @endif
                  @if(session('danger'))
                      <div class="alert alert-danger">
                          {{ session('danger') }}
                      </div>
                  @endif

                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div> 
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <form class="form-inline" method="POST" action="{{ url('/history-search') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="DocNum" @if (old('search_by') == "DocNum") {{ 'selected' }} @endif>Invoice Number</option>
                          {{-- <option value="CardCode" @if (old('search_by') == "CardCode") {{ 'selected' }} @endif>Customer Code</option>
                          <option value="CardName" @if (old('search_by') == "CardName") {{ 'selected' }} @endif>Customer Name</option> --}}
                          <option value="Created_at" @if (old('search_by') == "Created_at") {{ 'selected' }} @endif>Created At</option>
                          <option value="Status" @if (old('search_by') == "Status") {{ 'selected' }} @endif>Status</option>
                        </select>
                      </div>
                      <div id="search_method_container">
                        {{-- <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any" @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select> --}}
                      </div>
                      <div class="form-group mx-sm-1 mb-2" id="status_container" style="display: none;">
                        <select name="status" id="status" class="form-control form-control-sm">
                          <option value="" selected>Select status</option>
                          <option value="0" @if (old('status') == "Pending") {{ 'selected' }} @endif>Pending</option>
                          <option value="1" @if (old('status') == "Approved") {{ 'selected' }} @endif>Approved</option>
                          <option value="2" @if (old('status') == "Rejected") {{ 'selected' }} @endif>Rejected</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2" id="created_at_container" style="display: none;">
                        <input class="form-control form-control-sm" type="date" name="created_at" id="created_at">
                      </div>
                      <div class="form-group mx-md-1 mb-2" id="search_value_container">
                        <input type="number" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    </form>
                    
                    <script>
                      // toggle search method and search value containers based on selected search by value
                      const searchBySelect = document.getElementById('search_by');
                      const searchMethodContainer = document.getElementById('search_method_container');
                      const statusContainer = document.getElementById('status_container');
                      const createdAtContainer = document.getElementById('created_at_container');
                      const searchValueContainer = document.getElementById('search_value_container');
                  
                      searchBySelect.addEventListener('change', (event) => {
                          const selectedValue = event.target.value;
                          if (selectedValue === 'Status') {
                              searchMethodContainer.style.display = 'none';
                              statusContainer.style.display = 'block';
                              createdAtContainer.style.display = 'none';
                              searchValueContainer.style.display = 'none';
                          } else if (selectedValue === 'Created_at') {
                              searchMethodContainer.style.display = 'none';
                              statusContainer.style.display = 'none';
                              createdAtContainer.style.display = 'block';
                              searchValueContainer.style.display = 'none';
                          } else {
                              searchMethodContainer.style.display = 'block';
                              statusContainer.style.display = 'none';
                              createdAtContainer.style.display = 'none';
                              searchValueContainer.style.display = 'block';
                          }
                      });
                  </script>
                  
                    
                  </div>

                  
                  <div class="col-md-4 text-right">
                    <button title="Block Network" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-filter">
                      <i class="fas fa-search"></i> Request by Invoice No
                    </button>
                    {{-- <a class="btn btn-info btn-sm" href="{{ url('/history-req') }} ">Historical request</a> --}}
                  </div>

                </div>
                
                <div class="row mb-3">
                  
                </div>
                
                <table id="tableBilling" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Invoice No</th>
                      <th>Invoice Period</th>
                      <th>Cust. Info</th>
                      <th>Current Outstanding</th>
                      @if ($type  == "Dept Head")
                      <th>Request By</th>
                        @else
                        <th>Remarks</th>
                        @endif
                      <th>Status</th>
                      <th>Action</th>
                      {{-- <th>Last Update</th> --}}
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($AdjusmentApproval as $data)
                      <tr>
                        <td>{{ $data->docnum }}</td>
                        <td>{{ date('Y-m-d', strtotime($data->DocDate)) }}</td>
                        <td>
                            <b>{{ $data->CardCode }}</b>
                            <br> {{ $data->CardName }}
                        </td>
                        <td><i>{{ $data->SOCurrency." ". number_format($data->Outstanding,2,",",".") }}</i></td>
                        @if ($type  == "Dept Head")
                            <td><b>{{$requestBy = str_replace('@napinfo.co.id', '', $data->request_by)}}</b> 
                             <br>{{ $data->remark_request }}</td>
                        @else
                            <td>
                              @if ($data->reason_reject == null)
                              {{$data->remark_request}}
                              @else
                              {{ $data->reason_reject }}</td>
                              @endif
                        @endif
                        <td>
                            @if ($data->status == '0')
                            <small class="badge badge-warning text-light">Pending</small>
                            @endif
                            @if ($data->status == '1')
                            <small class="badge badge-success">Success</small>
                            @endif
                            @if ($data->status == '2')
                            <small class="badge badge-danger">Reject</small>
                            @endif
                        </td>
                        <td>
                            @if ($type  == "Dept Head")
                                @if ($data->status == '0')
                                <a class="btn btn-success btn-sm" href="{{ url('/acc-req/'.$data->docnum) }}">Accept</a>
                                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#rejectlModal" data-docnum="{{ $data->docnum }}">Reject</button>

                            <!-- Modal -->
                            <div class="modal fade" id="rejectlModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="cancelModalLabel">Cancel Invoice No {{ $data->docnum }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="{{ url('/rej-req') }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <p>Remarks Reject</p>
                                        <input type="text" class="form-control" name="remarks" required/>
                                        <input name="inv" type="number" value="{{$data->docnum}}" hidden>
                                      </div>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                                {{-- <a class="btn btn-danger btn-sm" href="{{ url('/rej-req/'.$data->docnum) }}">Reject</a> --}}
                                @endif
                            @else
                            @if ($data->status == '0')
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#cancelModal" data-docnum="{{ $data->docnum }}">Cancel</button>

                            <!-- Modal -->
                            <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="cancelModalLabel">Cancel Invoice No {{ $data->docnum }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <p>Are you sure you want to cancel invoice no {{ $data->docnum }}?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                    <a href="{{ route('cancelDocument', ['docnum' => $data->docnum]) }}" class="btn btn-danger">Yes</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                             @endif
                            @endif
                            
                        </td>
                        
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {{-- Modal Filter --}}
  <div class="modal fade" id="modal-filter">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Find Invoice No</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ url('/find-billing') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <input type="number" class="form-control" name="inv_no"/>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Submit">
        </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
  </div>
  {{-- Modal Filter --}}


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableBilling").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": true,
      //"dom": '<"top"fli>rt<"bottom"p><"clear">',
      //"pagingType": "first_last_numbers",
      "order": [[ 0, "asc" ],[ 1, "asc" ]],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });

  $("#datepicker").datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months"
});
</script>
@endsection