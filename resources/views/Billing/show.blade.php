@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customer Billing Detail</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-12">
              <div class="card">
                <div class="card-header p-2">
                  <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="#currentBill" data-toggle="tab">Billing Info </a></li>
                    <li class="nav-item"><a class="nav-link" href="#info" data-toggle="tab">Customer Info</a></li>
                    {{-- <li class="nav-item"><a class="nav-link" href="#historyBill" data-toggle="tab">History Billing</a></li> --}}
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="active tab-pane" id="currentBill">
                        <div class="row">
                            <div class="col-sm-4">
                                <strong>Invoice No</strong>
                                <p class="text-muted">
                                {{ $info->DocNum }}
                                <small class="badge badge-info">{{ $info->Billing_Pay_Type }}</small>
                                </p>

                                <strong>Invoice Date</strong>
                                <p class="text-muted">
                                {{ date('Y-m-d', strtotime($info->DocDate)) }}
                                </p>

                                <strong>Current Billing Amount</strong>
                                <p class="text-muted">
                                <i>{{ $info->SOCurrency." ". number_format($info->TotalAmount,2,",",".") }}</i>
                                @if ($info->PaymentStatus == "Paid")
                                    <small class="badge badge-success">Paid</small>
                                @else
                                    <small class="badge badge-danger">Unpaid</small>
                                @endif
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <strong>Customer ID</strong>
                                <p class="text-muted">
                                {{ $info->CardCode }}
                                </p>

                                <strong>Invoice Duedate</strong>
                                <p class="text-muted">
                                {{ date('Y-m-d', strtotime($info->DocDueDate)) }}
                                </p>

                                <strong>Outstanding Amount</strong>
                                <p class="text-muted">
                                <i>{{ $info->SOCurrency." ". number_format($info->Outstanding,2,",",".") }}</i>
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <strong>Customer Name</strong>
                                <p class="text-muted">
                                {{ $info->CardName }}
                                </p>

                                <strong>New Customer</strong>
                                <p class="text-muted">
                                  {{ $info->NewCustomer }}
                                </p>

                                <strong>Last Update</strong>
                                <p class="text-muted">
                                  {{ $info->last_update }}
                                </p>
                            </div>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="tableCurrentBilling" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Description</th>
                                            <th>Unit Price</th>
                                            <th>Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($current_billings as $data)
                                            <tr>
                                                <td>{{ $data->LineNum }}</td>
                                                <td>{{ $data->Description }}</td>
                                                <td><i>{{ $info->SOCurrency." ". number_format($data->UnitPrice,2,",",".") }}</i></td>
                                                <td><i>{{ $info->SOCurrency." ". number_format($data->TotalPrice,2,",",".") }}</i></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-5">
                                <div class="table-responsive">
                                    <table class="table">
                                      <tbody><tr>
                                        <th style="width:60%">Amount:</th>
                                        <td><i>{{ $info->SOCurrency." ". number_format($data->Amount,2,",",".") }}</i></td>
                                      </tr>
                                      <tr>
                                        <th>PPH23:</th>
                                        <td><i>{{ $info->SOCurrency." ". number_format($data->PPH23,2,",",".") }}</i></td>
                                      </tr>
                                      <tr>
                                        <th>VAT:</th>
                                        <td><i>{{ $info->SOCurrency." ". number_format($data->PPN,2,",",".") }}</i></td>
                                      </tr>
                                      <tr>
                                        <th>Rebate:</th>
                                        <td><i>{{ $info->SOCurrency." ". number_format($data->Rebate,2,",",".") }}</i></td>
                                      </tr>
                                      <tr>
                                        <th>Total Amount:</th>
                                        <td><i>{{ $info->SOCurrency." ". number_format($data->TotalAmount,2,",",".") }}</i></td>
                                      </tr>
                                    </tbody></table>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="info">
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Customer ID</strong>
                                <p class="text-muted">
                                {{ $info->CardCode }}
                                </p>

                                <strong>Customer Address</strong>
                                <p class="text-muted">
                                {{ $info->CustAddress }}
                                </p>

                                <strong>Customer VA</strong>
                                <p class="text-muted">
                                {{ $info->CustCodeVA }}
                                </p>

                            </div>
                            <div class="col-md-4">
                                <strong>Customer Name</strong>
                                <p class="text-muted">
                                {{ $info->CardName }}
                                </p>

                                <strong>Customer Envelope Address</strong>
                                <p class="text-muted">
                                {{ $info->EnvAddress }}
                                </p>

                                <strong>Customer VA Name</strong>
                                <p class="text-muted">
                                {{ $info->VA }}
                                </p>
  
                            </div>
                            <div class="col-md-4">
                              <strong>Email</strong>
                              <p class="text-muted">
                              {{ $info->Email }}
                              </p>

                              <strong>Sales Name</strong>
                              <p class="text-muted">
                              {{ $info->Sales_Employee }}
                              </p>

                              <strong>Phone No</strong>
                              <p class="text-muted">
                              {{ $info->PhoneNo }}
                              </p>
                          </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="historyBill">
                        tiga
                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableCurrentBilling").DataTable({
      "responsive": true, 
      //"lengthChange": true, 
      "autoWidth": false,
      "order": [[ 0, "asc" ]],
      "dom": '<"top"i>rt<"clear">',
      "columnDefs": [
            { "width": "60%", "targets": 1 }
        ],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection