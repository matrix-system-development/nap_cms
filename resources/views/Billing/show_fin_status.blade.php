@extends('Layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            {{-- <h1>Customer Network Status</h1> --}}
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                <div class="card">
                    <div class="card-header p-2">
                        Billing Info
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <table id="tableBilling" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Invoice No</th>
                                        <th>Invoice Period</th>
                                        <th>Cust. Info</th>
                                        <th>Billing Type</th>
                                        <th>Total Billing</th>
                                        <th>Outstanding</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @php
                                        $no=1;
                                      @endphp
                                      @foreach ($billings as $billing)
                                      <tr>
                                        <td>{{ $billing->DocNum }}</td>
                                        <td>{{ date('Y-m-d', strtotime($billing->DocDate)) }}</td>
                                        <td>
                                            <b>{{ $billing->CardCode }}</b>
                                            <br> {{ $billing->CardName }}
                                        </td>
                                        <td><small class="badge badge-info">{{ $billing->Billing_Pay_Type }}</small></td>
                                        <td><i>{{ $billing->SOCurrency." ". number_format($billing->TotalAmount,2,",",".") }}</i></td>
                                        <td><i>{{ $billing->SOCurrency." ". number_format($billing->Outstanding,2,",",".") }}</i></td>
                                        <td>
                                            @if ($billing->NewCustomer =='YES')
                                                <small class="badge badge-info">New Customer</small>
                                                <br>
                                            @endif
                    
                                            @if ($billing->PaymentStatus == "Paid")
                                                <small class="badge badge-success">Paid</small>
                                            @else
                                                <small class="badge badge-danger">Unpaid</small>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                <div class="btn-group" role="group">
                                                  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    Action
                                                  </button>
                                                  <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="{{ url('billing/'.$billing->DocNum) }}" target="_BLANK">Billing Detail</a>
                                                    @if ($billing->PaymentStatus == "Paid")
                                                    <form action="{{ url('/payment/result') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <input name="inv_number" type="hidden" class="form-control" id="inv_number" value="{{ $billing->DocNum }}">
                                                        <button type="submit" class="dropdown-item">Payment Info</button>
                                                    </form>
                                                    @endif
                                                  </div>
                                                </div>
                                            </div>
                                        </td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableBilling").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": true,
      //"dom": '<"top"fli>rt<"bottom"p><"clear">',
      //"pagingType": "first_last_numbers",
      "order": [[ 0, "desc" ]],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });
</script>
@endsection