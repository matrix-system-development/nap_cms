<style>
    .main-sidebar { background-color: #151A48 !important }
    </style>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ url("/home") }}" class="brand-link">
          <img src="{{ asset('dist2/img/Matrix Color.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light">Nap Lentera</span>
        </a>
    
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-header">MENU</li>
                    <li class="nav-item">
                        <a href={{ url("/home") }} class="nav-link">
                            <i class="nav-icon fas fa-house-user"></i>
                            <p>
                            Dashboard
                            </p>
                        </a>
                    </li>
                    @if (auth()->user()->role == 'BOD')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/mon-cust')}}" class="nav-link">
                                <i class="fas fa-network-wired nav-icon"></i>
                                <p>Network Status</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-coins"></i>
                            <p>
                                Finance
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/billing')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Broadband</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/billing-corps')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Corporate</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/invoice')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Invoice</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a href="{{url('/payment/list')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Payment</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/payment/')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Check Payment</p>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                    @endif
                    
                    @if (auth()->user()->role == 'Customer Care')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/mon-cust')}}" class="nav-link">
                                <i class="fas fa-network-wired nav-icon"></i>
                                <p>Network Status</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        @if (auth()->user()->pic == '1')
                        <li class="nav-item">
                            <a href="{{url('/apv-temp-unblock')}}" class="nav-link">
                            <i class="fas fa-tasks nav-icon"></i>
                            <p>Approval Temp Unblock</p>
                            </a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-coins"></i>
                            <p>
                                Finance
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/billing')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/payment/list')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Payment</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/payment/')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Check Payment</p>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Master
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/user-cust')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User Customer</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                    @endif

                    @if (auth()->user()->role == 'Guest')
                        
                    @endif
    
                    @if (auth()->user()->role == 'Super Admin')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/mon-cust')}}" class="nav-link">
                                <i class="fas fa-network-wired nav-icon"></i>
                                <p>Network Status</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Monitoring
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/mt-monitor')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>PG - Midtrans</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/dana-monitor')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>DANA</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                        {{-- <li class="nav-item">
                            <a href={{ url("/import") }} class="nav-link">
                                <i class="nav-icon fas fa-upload"></i>
                                <p>
                                Bulk Action
                                </p>
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-coins"></i>
                            <p>
                                Finance
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/billing')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Broadband</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/billing-corps')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Corporate</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/billing-adjustment')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Adjustment</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/invoice')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Invoice</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a href="{{url('/payment/list')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Payment</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/payment/')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Check Payment</p>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/api-log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>API Logs</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/audit-log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Audit Logs</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/remind-billing-log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Billing Reminder Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Master
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/rule')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>App Rule</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/user')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User Internal</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/user-cust')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User Customer</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/announcement')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Announcement</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cog"></i>
                            <p>
                                Services Config
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/service/job')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Run Services</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-server"></i>
                            <p>
                                FTP
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/ftp/indomaret')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Indomaret</p>
                                </a>
                            </li>
                            </ul>
                        </li> --}}
                    @endif

                    @if (auth()->user()->role == 'Sales Support')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Master
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/user-cust')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User Customer</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                    @endif

                    @if (auth()->user()->role == 'Sales Broadband')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-coins"></i>
                            <p>
                                Finance
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/billing')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/payment/list')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Payment</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/payment/')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Check Payment</p>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        @if (auth()->user()->pic == '1')
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                        @endif
                    @endif

                    @if (auth()->user()->role == 'Marcomm')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Master
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/announcement')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Announcement</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
    
                    @if (auth()->user()->role == 'User')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                    @endif
    
                    @if (auth()->user()->role == 'User Finance')
                        <li class="nav-item">
                            <a href="{{url('/mst-broadband')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/mon-cust')}}" class="nav-link">
                                <i class="fas fa-network-wired nav-icon"></i>
                                <p>Network Status</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-coins"></i>
                            <p>
                                Finance
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/billing')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Broadband</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/billing-corps')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Corporate</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/invoice')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Invoice</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a href="{{url('/billing-adjustment')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Adjustment</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/payment/list')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Payment</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/payment/')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Check Payment</p>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/remind-billing-log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Billing Reminder Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                    @endif
    
                    @if (auth()->user()->role == 'User Network')
                        <li class="nav-item">
                            <a href="{{url('/mon-cust')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Network Status</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/quarantines')}}" class="nav-link">
                                <i class="fas fa-user-slash nav-icon"></i>
                                <p>Quarantines</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-business-time"></i>
                            <p>
                                Logs
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('/log')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Monitoring Logs</p>
                                </a>
                            </li>
                            </ul>
                        </li>
                    @endif

                    @if (auth()->user()->role == 'Sales Corporate')
                        <li class="nav-item">
                            <a href="{{url('/mst-cust')}}" class="nav-link">
                                <i class="fas fa-user nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-coins"></i>
                            <p>
                                Finance
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{url('/billing-corps')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Billing Corporate</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('/payment/list')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Payment</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    <a href="{{url('/payment/')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Check Payment</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
    
                    <li class="nav-item">
                        <a href={{ url("/logout") }} class="nav-link">
                            <i class="nav-icon fas fa-sign-out-alt"></i>
                            <p>
                            Logout
                            </p>
                        </a>
                    </li>
                </ul>
                
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>