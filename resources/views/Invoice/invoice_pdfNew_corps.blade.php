<!DOCTYPE html>
<html>

<head>
	<title>{{$pdf_header->CardName.'_'.$pdf_header->DocNum}}</title>
    <link rel="shortcut icon" href="{{ asset('dist2/img/matrix-logo-new.png') }}" />
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<style>
		#footer {
			position: fixed;
			padding: 10px 10px 0px 10px;
			bottom: 0;
			width: 100%;
			/* Height of the footer*/
			height: 40px;
		}

        table{
            font-size: 12px;
        }
		tr,
		td {
			padding-left: 2px;
            vertical-align: middle;
		}
        tr.hide_left_with > td, td.hide_left_with{
        border-left-style:hidden;
        border-bottom-style:hidden;
      }
      tr.hide_all > td, td.hide_all{
        border-left-style:hidden;
        border-right-style:hidden;
        border-top-style:hidden;
        border-bottom-style:hidden;
      }
      tr.uniq > td, td.uniq{
        border-top-style:hidden;
        border-bottom-style:hidden;
      }
      tr.hide_left > td, td.hide_left{
        border-left-style:hidden;
      }
      tr.hide_bottom > td, td.hide_bottom{
        border-bottom-style:hidden;
      }
      tr.hide_all_test > td, td.hide_all_test{
        border-top-style:hidden;
      }
        th{
            text-align: center;
        }

        .page-break {
            page-break-after: always;
        }
        .first-half {
    float: left;
    width: 50%;
}
.second-half {
    float: right;
    width: 50%;
}
        <?php include(public_path().'/dist2/css/pdf.css');?>
	</style>

</head>
<body style="font-size: 12px">
	<img <img src="{{public_path('dist2/img/header_mail_withNPWP.JPG')}}" alt=""width="705" height="140" />
    <table border="1">
       
        <tr>
            <td width="347px" rowspan="6">
                <b>{{$pdf_header->CardName}}</b>
                <br>
                <p>@php
                echo $address;
                echo '<br>';
                echo  'Attention : '.$pdf_header->Attention;
                echo '<br>';
                echo  'Remarks : '.$pdf_header->Remarks;
               @endphp</p>
               
            </td>
            <td width="145px"><small>Total Tagihan / <i>Total Amount</i></small></td>  
            <td width="198px" style="border-left-style: hidden;"><small>: {{number_format($pdf_header->TotalAmount,0,",",".")}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Tanggal Jatuh Tempo / <i>Due Date</i></small></td>  
            <td width="198px" style="border-left-style: hidden;"><small>: {{$due_date}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Nomor Pelanggan / <i>Customer ID</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>:  {{$pdf_header->CardCode}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Nomor Tagihan / <i>Invoice ID</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>: {{$pdf_header->DocNum}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Tanggal Tagihan / <i>Invoice Date</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>: {{date('d-M-Y', strtotime($pdf_header->DocDate))}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>No. PO  / <i>Customer Ref. No.</i></small></td>
            <td width="198px" style="border-left-style: hidden;"><small style="font-size: 9px">: {{$pdf_header->CustomerRef}}</small></td>
        </tr>
    </table>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th width="30%"><small><strong>Tagihan Sebelumnya / <i>Prev. Balance</i></strong></small></th>
            <th width="20%"><small><strong>Pembayaran / <i>Payment</i></strong></small></th>
            <th width="25%"><small><strong>Tagihan Baru / <i>New Balance</i></strong></small></th>
            <th width="25%"><small><strong>Total Tagihan / <i>Total Balance</i></strong></small></th>
        </tr>
        <tr style="text-align: center;">
            <th width="30%">{{number_format($previous_bill_detail,0,",",".")}}</th>
            <th width="20%">{{number_format($payment,0,",",".")}}</th>
            <th width="25%">{{number_format($pdf_header->Amount,0,",",".")}}</th>
            <th width="25%">{{number_format($pdf_header->TotalAmount,0,",",".")}}</th>
        </tr>
    </table>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th width="20%">Tanggal / <i>Date</i></th>
            <th width="60%">Rincian Transaksi / <i>Detail Transaction</i></th>
            <th width="20%">Jumlah / <i>Amount</i> {{'('.$pdf_header->SOCurrency.')'}}</th>
        </tr>
        <tr>
            <td width="20%"><small>{{$date_desc_previous_bill}}</small></td>
            <td width="60%"><small>Tagihan Bulan Sebelumnya / <i>Prev. Balance</i> :</small></td>
            <td  style="text-align: right;"><small>{{number_format($previous_bill_detail,0,",",".")}}</small></td>
        </tr>
        <tr>
           <td width="20%"><small>{{$date_previous_bill}}</small></td>
            <td width="60%"><small>Pembayaran Diterima / <i>Payment Received (Bank Transfer)</i> :</small></td>
            <td  style="text-align: right;"><small>({{number_format($payment,0,",",".")}})</small></td>
        </tr>
        @if ($next == 1)
                    @if ($materai_value == 1)
                    <tr>
                        <td width="20%"><small>{{$date_desc_serve}}</small></td>
                        <td width="60%"><small>Biaya Layanan / <i>Service Charges</i></small></td>
                        <td  style="text-align: right;" ><small>{{number_format(($pdf_header->Amount-$materai->TotalPrice),0,",",".")}}</small></td>
                        </tr>
                <tr>
                <td width="20%"><small>{{$date_desc_serve}}</small></td>
                <td width="60%"><small>{{$materai->Description}}</small></td>
                <td  style="text-align: right;" ><small>{{number_format($materai->TotalPrice,0,",",".")}}</small></td>
                </tr>
                 
                    @else
                    <tr>
                        <td width="20%"><small>{{$date_desc_serve}}</small></td>
                        <td width="60%"><small>Biaya Layanan / <i>Service Charges</i></small></td>
                        <td  style="text-align: right;" ><small>{{number_format($pdf_header->Amount,0,",",".")}}</small></td>
                    </tr>
                    @php
        for ($x = 1; $x <= 3; $x++) {
           echo '   <tr> 
                    <td style="color: white" width="20%"><small>Matrix Broadband Invoice</small></td>
                    <td style="color: white" width="60%"><small>Matrix Broadband Invoice</small></td>
                    <td style="color: white"><small>Matrix Broadband Invoice</small></td>
                    </tr>';
          }
          @endphp
                        @endif
        
        
     
        @else
     
        @foreach ($descripton as $data)
        <tr>
            @php
            if (str_contains($data->Description, 'Period')) {
                $count_char =  strrpos($data->Description,"(");
            $select_date = substr($data->Description,$count_char,12);
            $deleteChar = substr($select_date,-11);
            $date_desc = str_replace(".","-",$deleteChar);
            } else {
                $date_desc = null;
            }
            
           
            @endphp
            <td width="20%"><small>{{$date_desc}}</small></td>
            <td width="60%"><small>{{$data->Description}}</small></td>
           
            <td  style="text-align: right;" ><small>{{number_format($data->TotalPrice,0,",",".")}}</small></td>
        </tr>
       
        @endforeach
        
        @endif
        @php
        for ($x = 1; $x <= $count; $x++) {
           echo '   <tr> 
                    <td style="color: white" width="20%"><small>Matrix Broadband Invoice</small></td>
                    <td style="color: white" width="60%"><small>Matrix Broadband Invoice</small></td>
                    <td style="color: white"><small>Matrix Broadband Invoice</small></td>
                    </tr>';
          }
          @endphp
        <tr>
            <td width="20%" rowspan="4"></td>
            <td class="text-right" width="60%"><small><strong>Potongan / <i>Discount</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->Rebate,0,",",".")}}</small></td>
        </tr>
        <tr>
           
            <td class="text-right" width="60%"><small><strong> Total / <i>Total</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->Amount,0,",",".")}}</small></td>
        </tr>
        
        <tr>
            <td class="text-right" width="60%"><small><strong>PPN / <i>VAT</i> : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->PPN,0,",",".")}}</small></td>
        </tr>
        <tr>
            <td class="text-right" width="60%"><small><strong>Total Tagihan / <i>Total Balance</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->TotalAmount,0,",",".")}}</small></td>
        </tr>
    </table>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th colspan = "4" width="100%"><strong>METODE PEMBAYARAN / <i>PAYMENT METHOD</i></strong></th>
        </tr>
        <tr style="">
            <td class="hide_all_test" style="color: white;"  width="14%"><small><strong></strong></small></td>
            <td  width="36%"><small><strong>1. Virtual Account BCA</strong></small></td>
            <td colspan = "2" width="43%"><small><strong>2. {{$NoPO['U_MEB_ACCTBANK']}}</strong></small></td>
        </tr>
        <tr style="">
            <td  width="14%"><small>Nama / Acct. Name :</small></td>
            <td  class="text-center" width="18%"><small>NAP {{$pdf_header->CardName}}<strong></strong></small></td>
            <td   width="19%"><small>Nama / Acct. Name :</small></td>
            <td  class="hide_left"  width="31%"><small>PT. NAP INFO LINTAS NUSA</small></td>
        </tr>
        <tr style="">
            <td class="hide_bottom"  width="14%"><small>Nomor / Acct. No. :</small></td>
            <td class="hide_bottom text-center"   width="18%"><small>{{str_replace("02567","67805",$pdf_header->CustCodeVA)}}<strong></strong></small></td>
            <td class="hide_bottom"   width="19%"><small>Nomor / Acct. No. : </small></td>
            <td  class="hide_left_with"  width="31%"><small>{{$NoPO['U_MEB_ACCTBANKNO']}}</small></td>
        </tr>
    </table>
    <table  border="1" width="100%">
        <tr>
            <td style="vertical-align:top;" width="50%"><small><strong>Informasi Penting</strong><br>
            <span style="font-size: 8.3px;display:inline-block">1. Saat ini pembayaran dapat dilakukan melalui Virtual Account BCA atau {{$NoPO['U_MEB_ACCTBANK']}}.
            <br>2. Bukti pembayaran dan bukti potong PPh 23 dapat dikirimkan ke alamat email revenue.assurance@napinfo.co.id
            <br>3. Kode objek pajak PPh 23 adalah 24-104-26 (jasa internet dan sambungannya).
            <br>4. PT. NAP Info Lintas Nusa berhak melakukan usaha-usaha penagihan dan penghentian layanan apabila terdapat tunggakan atas tagihan.
            <br>5. Abaikan tagihan ini apabila telah melakukan pembayaran.
            </span>
            </small>
            </td>
            <td style="vertical-align:top;" width="50%"><small><strong>Important Information</strong><br>
            <span style="font-size: 8.68px;display:inline-block">1.  Payment can be made via Virtual Account BCA or {{$NoPO['U_MEB_ACCTBANK']}}.
            <br>2. Payment evidence and PPh 23 tax withholding evidence can be sent to the email address revenue.assurance@napinfo.co.id.
            <br>3. The tax object code for PPh 23 is 24-104-26 (Jasa Internet dan Sambungannya).
            <br>4. PT. NAP Info Lintas Nusa has the right to undertake billing efforts and suspend services in case of any outstanding balances on invoices.
            <br>5. Please disregard this invoice if payment has already been made.
           </span>
            </small>
            </td>
        </tr>
        <tr style="text-align: center;">
            <td style="font-size: 11px;" colspan="2" ><small>Jika Anda memiliki pertanyaan lebih lanjut, mohon menghubungi 24-Hr Customer Care kami  via email di customer.care@napinfo.co.id atau telepon di nomor 1500787</small></td>
        </tr>
        <tr style="text-align: center;">
            <td class="font-italic" colspan="2" ><small>Should there be any other questions, please kindly contact 24-Hr Customer Care at customer.care@napinfo.co.id or 1500787</small></td>
        </tr>
    </table>
    <div class="text-right" >
    <p><strong>Disetujui Oleh / <i>Approved By</i> :</strong></p>
    <img <img src="{{public_path('dist2/img/ttd.JPG')}}" alt="" width="141" height="55"/>
    <p><strong>Omar Nasution / Direktur</strong></p>
    </div>
    <img <img src="{{public_path('dist2/img/header-footer.jpg')}}" alt=""width="705" height="100" />
    <div id="footer">
		<img <img src="{{public_path('dist2/img/footer-new.png')}}" alt="" width="700" height="70" />
	</div>
    @if ($next == 1)
    <div class="page-break"></div>
    <img <img src="{{public_path('dist2/img/header_mail_withNPWP.JPG')}}" alt=""width="705" height="140" />
    <table border="1">
       
        <tr>
            <td width="347px" rowspan="6">
                <b>{{$pdf_header->CardName}}</b>
                <br>
                <p>@php
                echo $address;
                echo '<br>';
                echo  'Attention : '.$pdf_header->Attention;
                echo '<br>';
                echo  'Remarks : Lorem, ipsum dolor sit amet consectetur';
               @endphp</p>
            </td>
            <td width="145px"><small>Total Tagihan / <i>Total Amount</i></small></td>  
            <td width="198px" style="border-left-style: hidden;"><small>: {{number_format((($previous_bill_detail+$TotalAmount_all)-$payment),0,",",".") }}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Tanggal Jatuh Tempo / <i>Due Date</i></small></td>  
            <td width="198px" style="border-left-style: hidden;"><small>: {{$due_date}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Nomor Pelanggan / <i>Customer ID</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>:  {{$pdf_header->CardCode}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Nomor Tagihan / <i>Invoice ID</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>: {{$pdf_header->DocNum}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Tanggal Tagihan / <i>Invoice Date</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>: {{date('d-M-Y', strtotime($pdf_header->DocDate))}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>No. PO  / <i>Customer Ref. No.</i></small></td>
            <td width="198px" style="border-left-style: hidden;"><small style="font-size: 9px">: </small></td>
        </tr>
    </table>
    <br>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th width="5%">No</th>
            <th width="75%">Rincian Transaksi / <i>Detail Transaction</i></th>
            <th width="20%">Jumlah / <i>Amount</i> {{'('.$pdf_header->SOCurrency.')'}}</th>
        </tr>
        <tr style="text-align: center;">
            <th width="5%"></th>
            <th width="75%"></th>
            <th width="20%"></th>
        </tr>
        @foreach ($descripton as $data)
        <tr>
            @php
            if (str_contains($data->Description, 'Period')) {
                $count_char =  strrpos($data->Description,"(");
            $select_date = substr($data->Description,$count_char,12);
            $deleteChar = substr($select_date,-11);
            $date_desc = str_replace(".","-",$deleteChar);
            } else {
                $date_desc = null;
            }
            
           
            @endphp
            <td class="uniq" style="text-align: center;" width="5%"><small>{{$data->LineNum}}</small></td>
            <td class="uniq" width="75%"><small>{{$data->Description}}</small></td>
           
            <td class="uniq"  style="text-align: right;" ><small>{{number_format($data->TotalPrice,0,",",".")}}</small></td>
        </tr>
       
        @endforeach
        @php
        for ($x = 1; $x <= $count_check; $x++) {
           echo '   <tr> 
                    <td class="uniq" style="color: white" width="20%"><small>Matrix Broadband Invoice</small></td>
                    <td class="uniq" style="color: white" width="60%"><small>Matrix Broadband Invoice</small></td>
                    <td class="uniq" style="color: white"><small>Matrix Broadband Invoice</small></td>
                    </tr>';
          }
          @endphp
            <tr> 
                <td  width="20%"><small></small></td>
                <td  width="60%"><small></small></td>
                <td ><small></small></td>
                </tr>
          <tr>
             <td style="color: white" width="20%"><small>Matrix Broadband Invoice</small></td>
             <td class="text-right" width="60%"><small><strong> Total / <i>Total</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
             <td  style="text-align: right;"><small>{{number_format($pdf_footer->Amount,0,",",".")}}</small></td>
        </tr>
    </table>
    @endif
    <div id="footer">
		<img <img src="{{public_path('dist2/img/footer-new.png')}}" alt="" width="700" height="70" />
	</div>
</body>
</html>