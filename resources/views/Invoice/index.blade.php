@extends('Layouts.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice Menu</h1>
          </div>
        </div>
      </div><!-- /.container-fluid --> --}}
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Invoice: {{-- <b>{{ date('F Y', strtotime($current_month)) }}</b> --}}</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                      <!--alert success -->
                      @if (session('status'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ session('status') }}</strong>
                      </div> 
                      @endif
                      <!--alert success -->
                      <!--validasi form-->
                        @if (count($errors)>0)
                          <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <ul>
                                  <li><strong>Data Process Failed !</strong></li>
                                  @foreach ($errors->all() as $error)
                                      <li><strong>{{ $error }}</strong></li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                      <!--end validasi form-->
                    </div> 
                </div>
                <div class="row mb-3">
                  <div class="col-md-auto">
                    {{-- <button title="Block Network" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-filter">
                      <i class="fas fa-filter"></i> Filter by Invoice Period
                    </button> --}}
                    <form action="{{ url('/export-billing') }}" method="post" class="d-inline">
                      @csrf
                      <input type="hidden" name="inv_period" id="inv_period" value="{{ $current_month }}">
                     {{--  <button type="submit" class="btn btn-success btn-sm"><i class="far fa-file-excel"></i> Export To Excel</button> --}}
                    </form>
                  </div>
                </div>

                <div class="row mb-3">
                  <div class="col-12">
                    <form class="form-inline" method="POST" action="{{ url('/invoice-search') }}">
                      @csrf
                      <div class="form-group mx-md-1 mb-2">
                        <select name="search_by" id="search_by" class="form-control form-control-sm">
                          <option value="" selected>Select Search By</option>
                          <option value="DocNum" @if (old('search_by') == "DocNum") {{ 'selected' }} @endif>Invoice Number</option>
                          <option value="CardCode" @if (old('search_by') == "CardCode") {{ 'selected' }} @endif>Customer Code</option>
                          <option value="CardName" @if (old('search_by') == "CardName") {{ 'selected' }} @endif>Customer Name</option>
                        </select>
                      </div>
                      <div class="form-group mx-sm-1 mb-2">
                        <select name="search_method" id="search_method" class="form-control form-control-sm">
                          <option value="" selected>Select Search Method</option>
                          <option value="any"  @if (old('search_method') == "any") {{ 'selected' }} @endif>Any</option>
                          <option value="exact" @if (old('search_method') == "exact") {{ 'selected' }} @endif>Exact</option>
                        </select>
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" id="" name="search_value" placeholder="Input Search Value..." value="{{ old('search_value') }}">
                      </div>
                      <div class="form-group mx-md-1 mb-2">
                        <input type="text" class="form-control form-control-sm" name="inv_period" id="datepicker" placeholder="Invoice Period"/>
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                    </form>
                  </div>
                </div>
                
                <table id="tableBilling" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Invoice No</th>
                      <th>Invoice Period</th>
                      <th>Cust. Info</th>
                      <th>Total Billing</th>
                      <th>Outstanding</th>
                      <th>Status</th>
                      <th></th>
                      {{-- <th>Last Update</th> --}}
                    </tr>
                  </thead>
                  <tbody>
                     @foreach ($query_billings as $billing)
                      <tr>
                        <td>{{ $billing->DocNum }}</td>
                        <td>{{ date('Y-m-d', strtotime($billing->DocDate)) }}</td>
                        <td>
                          <b>{{ $billing->CardCode }}</b>
                          <br> {{ $billing->CardName }}
                          <br> <small class="badge badge-info">{{ $billing->Billing_Pay_Type }}</small>
                        </td>
                        <td><i>{{ $billing->SOCurrency." ". number_format($billing->TotalAmount,2,",",".") }}</i></td>
                        <td><i>{{ $billing->SOCurrency." ". number_format($billing->Outstanding,2,",",".") }}</i></td>
                        <td>
                          @if ($billing->NewCustomer =='YES')
                            <small class="badge badge-info">New Customer</small>
                            <br>
                          @endif

                          @if ($billing->PaymentStatus == "Paid")
                            <small class="badge badge-success">Paid</small>
                          @else
                            <small class="badge badge-danger">Unpaid</small>
                          @endif
                          <br>
                          {{-- @if ($billing->disabled =='0')
                            @if ($billing->temporary_unblock_exp=='')
                              <small class="badge badge-success">ACTIVE</small>
                            @else
                              <small class="badge badge-success">ACTIVE</small> <br>
                              <i>until: {{ $billing->temporary_unblock_exp }}</i>
                            @endif
                          @elseif($billing->disabled =='')
                            <i><b>NULL</b></i>
                          @else
                            <small class="badge badge-danger">INACTIVE</small>
                          @endif --}}
                        </td>
                        <td>
                          <div class="btn-group" role="group">
                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              Action
                            </button>
                            <div class="dropdown-menu">
                              <a href="{{ url('/invoice/pdf/'.$billing->DocNum) }}" class="btn" title="Export Invoice">Download Invoice</a>
                              <a class="dropdown-item" href="{{ url('/invoice/sent/'.$billing->DocNum.'/'.$billing->Email) }}">Send Invoice</a>
                            </div>
                          </div>
                        </div>
                         {{--  <a href="{{ url('/billing/'.$billing->DocNum) }}" class="btn btn-info btn-xs" title="View Detail" target="_BLANK">Detail Bill</a> --}}
                        </td>
                        {{-- <td>{{ $billing->last_update }}</td> --}}
                       </tr>
                    @endforeach 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {{-- Modal Filter --}}
  <div class="modal fade" id="modal-filter">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Choose Invoice Period</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{ url('/billing') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <input type="text" class="form-control" name="inv_period" id="datepicker" />
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Submit">
        </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
  </div>
  {{-- Modal Filter --}}


<!-- For Datatables -->
<script>
  $(document).ready(function() {
    var table = $("#tableBilling").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": true,
      //"dom": '<"top"fli>rt<"bottom"p><"clear">',
      //"pagingType": "first_last_numbers",
      "order": [[ 0, "asc" ],[ 1, "asc" ]],
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    });
  });

  $("#datepicker").datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months"
});
</script>
@endsection