<!DOCTYPE html>
<html>

<head>
	<title>{{$pdf_header->CardName.'_'.$pdf_header->DocNum}}</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<style>
		#footer {
			position: fixed;
			padding: 10px 10px 0px 10px;
			bottom: 0;
			width: 100%;
			/* Height of the footer*/
			height: 40px;
		}

        table{
            font-size: 12px;
        }
		tr,
		td {
			padding-left: 2px;
            vertical-align: middle;
		}
        tr.hide_left_with > td, td.hide_left_with{
        border-left-style:hidden;
        border-bottom-style:hidden;
      }
      tr.hide_all > td, td.hide_all{
        border-left-style:hidden;
        border-right-style:hidden;
        border-top-style:hidden;
        border-bottom-style:hidden;
      }
      tr.hide_left > td, td.hide_left{
        border-left-style:hidden;
      }
      tr.hide_bottom > td, td.hide_bottom{
        border-bottom-style:hidden;
      }
      tr.hide_all_test > td, td.hide_all_test{
        border-top-style:hidden;
      }
        th{
            text-align: center;
        }

        .page-break {
            page-break-after: always;
        }
        .first-half {
    float: left;
    width: 50%;
}
.second-half {
    float: right;
    width: 50%;
}
        <?php include(public_path().'/dist2/css/pdf.css');?>
	</style>

</head>
<body style="font-size: 12px">
	<img <img src="{{public_path('dist2/img/header_mail_withNPWP.JPG')}}" alt=""width="705" height="140" />
    
    {{-- <table  border="1">
        <tr>
            
           
            <td class="hide_all" colspan="2" style="text-align:right; vertical-align:top"  width="70%">
                <strong style="padding-left: 265px; font-size: 18px;"><b>TAGIHAN / INVOICE <br> MATRIX NAP INFO</b></strong>
            </td>
        
            <td class="hide_all" style="padding-left: 20px;"  width="30%" colspan="2">
                <small>
                <strong>NPWP : 01.960.385.1-062.000 <br>PT NAP Info Lintas Nusa</strong>
                <p style="font-size: 10px;">Plaza Kuningan Annex Building. Suite 101 <br>Jl. HR. Rasuna Said Kav.11-14 <br>Kel. Karet Kuningan, Kec. Setiabudi, Jakarta Selatan</p>
               </small></td>
        </tr>
    </table> --}}
    
    <table border="1">
       
        <tr>
            <td width="347px" rowspan="6">
                <b>{{$pdf_header->PayToCode}}</b>
                <br>
                <p>@php
                echo $address;
               @endphp</p>
            </td>
            <td width="145px"><small>Total Tagihan / <i>Total Amount</i></small></td>  
            <td width="198px" style="border-left-style: hidden;"><small>: {{number_format($pdf_header->current_outstanding,0,",",".") }}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Tanggal Jatuh Tempo / <i>Due Date</i></small></td>  
            <td width="198px" style="border-left-style: hidden;"><small>: {{$due_date}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Nomor Pelanggan / <i>Customer ID</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>:  {{$pdf_header->CardCode}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Nomor Tagihan / <i>Invoice ID</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>: {{$pdf_header->DocNum}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Tanggal Tagihan / <i>Invoice Date</i></small></td> 
            <td width="198px" style="border-left-style: hidden;"><small>: {{date('d-M-Y', strtotime($pdf_header->DocDate))}}</small></td>
        </tr>
        <tr>
            <td width="145px"><small>Jenis Layanan / <i>Service Type</i></small></td>
            <td width="198px" style="border-left-style: hidden;"><small style="font-size: 9px">: {{$pdf_header->Service}}</small></td>
        </tr>
    </table>
    <br>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th width="30%"><small><strong>Tagihan Sebelumnya / <i>Prev. Balance</i></strong></small></th>
            <th width="20%"><small><strong>Pembayaran / <i>Payment</i></strong></small></th>
            <th width="25%"><small><strong>Tagihan Baru / <i>New Balance</i></strong></small></th>
            <th width="25%"><small><strong>Total Tagihan / <i>Total Balance</i></strong></small></th>
        </tr>
        <tr style="text-align: center;">
            <th width="30%">{{number_format($previous_bill_detail,0,",",".")}}</th>
            <th width="20%">{{number_format($payment,0,",",".")}}</th>
            <th width="25%">{{number_format($pdf_header->TotalAmount,0,",",".")}}</th>
            <th width="25%">{{number_format((($previous_bill_detail+$pdf_header->TotalAmount)-$payment),0,",",".") }}</th>
        </tr>
    </table>
    <br>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th width="20%">Tanggal / <i>Date</i></th>
            <th width="60%">Rincian Transaksi / <i>Detail Transaction</i></th>
            <th width="20%">Jumlah / <i>Amount</i> {{'('.$pdf_header->SOCurrency.')'}}</th>
        </tr>
        <tr>
            <td width="20%"><small>{{$date_desc_previous_bill}}</small></td>
            <td width="60%"><small>Tagihan Bulan Sebelumnya / <i>Prev. Balance</i></small></td>
            <td  style="text-align: right;"><small>{{number_format($previous_bill_detail,0,",",".")}}</small></td>
        </tr>
        <tr>
           <td width="20%"><small>{{$date_previous_bill}}</small></td>
            <td width="60%"><small>Pembayaran Diterima / <i>Payment Received (Bank Transfer)</i></small></td>
            <td  style="text-align: right;"><small>{{number_format($payment,0,",",".")}}</small></td>
        </tr>
        {{-- <tr>
            <td></td>
            <td width="60%">Jumlah Terhutang :</td>
            <td>{{$Debt}}</td>
        </tr> --}}
        @foreach ($descripton as $data)
        <tr>
            @php
            if (str_contains($data->Description, 'Period')) {
                $count_char =  strrpos($data->Description,"(");
            $select_date = substr($data->Description,$count_char,12);
            $deleteChar = substr($select_date,-11);
            $date_desc = str_replace(".","-",$deleteChar);
            } else {
                $date_desc = null;
            }
            
           
            @endphp
            <td width="20%"><small>{{$date_desc}}</small></td>
            <td width="60%"><small>{{$data->Description}}</small></td>
           
            <td  style="text-align: right;" ><small>{{number_format($data->TotalPrice,0,",",".")}}</small></td>
        </tr>
        @endforeach
        
        @php
        for ($x = 1; $x <= $count; $x++) {
           echo '   <tr> 
                    <td style="color: white" width="20%"><small>Matrix Broadband Invoice</small></td>
                    <td style="color: white" width="60%"><small>Matrix Broadband Invoice</small></td>
                    <td style="color: white"><small>Matrix Broadband Invoice</small></td>
                    </tr>';
          }
          @endphp
        <tr>
            <td width="20%" rowspan="4"></td>
            <td class="text-right" width="60%"><small><strong> Total / <i>Total</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->Amount,0,",",".")}}</small></td>
        </tr>
        <tr>
            <td class="text-right" width="60%"><small><strong>Potongan / <i>Discount</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->Rebate,0,",",".")}}</small></td>
        </tr>
        <tr>
            <td class="text-right" width="60%"><small><strong>PPN / <i>VAT</i> : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->PPN,0,",",".")}}</small></td>
        </tr>
        <tr>
            <td class="text-right" width="60%"><small><strong>Total Tagihan / <i>Total Balance</i> {{'('.$pdf_header->SOCurrency.')'}} : </strong></small></td>
            <td  style="text-align: right;"><small>{{number_format($pdf_footer->TotalAmount,0,",",".") }}</small></td>
        </tr>
    </table>
    <br>
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th colspan = "7" width="100%"><strong>METODE PEMBAYARAN / <i>PAYMENT METHOD</i></strong></th>
        </tr>
        <tr style="">
            <td class="hide_all_test" style="color: white;"  width="14%"><small><strong></strong></small></td>
            <td class="text-center"  width="18%"><small><strong>1. Virtual Account BCA</strong></small></td>
            <td class="text-center" width="18%"><small><strong>2. Virtual Account BRI</strong></small></td>
            <td class="text-center" colspan = "2"  width="25%"><small><strong>3. Indomaret / Ceria Mart / Apps i.Saku & KlikIndomaret, Web KlikIndomaret</strong></small></td>
            <td class="text-center" colspan = "2" width="25%"><small><strong>4. Aplikasi Dana</strong></small></td>
        </tr>
        <tr style="">
            <td  width="14%"><small>Nama / Acct. Name:</small></td>
            <td  class="text-center" colspan="2" width="18%"><small>{{$pdf_header->CardName}}<strong></strong></small></td>
            <td   width="19%"><small>Nama Penagih / Biller Name :</small></td>
            <td  class="hide_left"  width="31%"><small>Matrix NAP Info</small></td>
            <td   width="19%"><small>Nama Penagih / Biller Name :</small></td>
            <td  class="hide_left"  width="31%"><small>Matrix NAP Info</small></td>
        </tr>
        <tr style="">
            <td class="hide_bottom"  width="14%"><small>Nomor / Acct. No.:</small></td>
            <td class="hide_bottom"   width="18%"><small>{{$pdf_header->CustCodeVA}}<strong></strong></small></td>
            <td class="hide_bottom"   width="18%"><small>13946.{{substr($pdf_header->CardCode,2)}}<strong></strong></small></td>
            <td class="hide_bottom"   width="19%"><small>Kode Bayar / Payment Code :</small></td>
            <td class="hide_left_with"   width="18%"><small>{{substr($pdf_header->CardCode,2)}}<strong></strong></small></td>
            <td class="hide_bottom"   width="19%"><small>Kode Bayar / Payment Code :</small></td>
            <td  class="hide_left_with"  width="31%"><small>{{$pdf_header->CardCode}}</small></td>
        </tr>
    </table>
    <table  border="1" width="100%">
        <tr>
            <td style="vertical-align:top;" width="50%"><small><strong>Informasi Penting</strong><br>
            <span style="font-size: 8px;text-align: justify;display:inline-block">1. Pembayaran dapat juga dilakukan melalui Credit Card, Virtual Account Bank lainnya (Mandiri, BNI,    Permata) atau GoPay. Anda cukup mengakses Matrix NAP Info Customer Portal di https://portal.napinfo.co.id. Apabila Anda lupa atau belum pernah login sebelumnya, mohon menghubungi 24-Hr Customer Care untuk informasi login Anda. Setelah login berhasil, pilih metode pembayaran, cantumkan Customer Code: C091644, silakan buat Kode Bayar Anda masing-masing dan ikuti proses selanjutnya.
            <br>2.  Pelanggan dihimbau untuk tidak menitipkan pembayaran tagihan ke karyawan Matrix NAP Info dan hanya melakukan pembayaran melalui metode di atas.
            <br>3. Total tagihan dapat dilihat di masing-masing portal H+1 setelah tanggal invoice.
            <br>4. Pelanggan dihimbau untuk melakukan pembayaran tepat waktu untuk menghindari terisolirnya layanan secara sistem apabila pembayaran belum diterima sebelum Tanggal Jatuh Tempo.
            <br>5. Matrix NAP Info berhak melakukan usaha-usaha penagihan dan penghentian layanan apabila terdapat tunggakan atas tagihan.
            </span>
            </small>
            </td>
            <td style="vertical-align:top;" width="50%"><small><strong>Important Information</strong><br>
            <span style="font-size: 8.48px;text-align: justify;display:inline-block">1. Payment can be made via Credit Card, Virtual Account of other Banks (Mandiri, BNI, Permata) or GoPay. Customer can simply logon to Matrix NAP Info Customer Portal at https://portal.napinfo.co.id. Should you forget or never login prior to this, please contact our 24-Hr Customer Care for login credentials. Upon successful login, select payment, input your Customer Code: C091644, generate your payment code and follow the indicated steps.
            <br>2. Under no circumstances, Customer shall pass the payment to any Matrix NAP Info employees and only do the payment through payment method as indicated above.
            <br>3. Total Balance will only be reflected on each payment portal H+1 after the invoice date.
            <br>4. Customer is advised to make payment as scheduled to avoid service isolation which is triggered automatically by system if no payment is received before the Due Date.
            <br>5. Matrix NAP Info reserves the right to conduct collection attempts and suspend service shall there be outstanding balance overdue.
           </span>
            </small>
            </td>
        </tr>
        <tr style="text-align: center;">
            <td style="font-size: 11px;" colspan="2" ><small>Jika Anda memiliki pertanyaan lebih lanjut, mohon menghubungi 24-Hr Customer Care kami  via email di customer.care@napinfo.co.id atau telepon di nomor 1500787</small></td>
        </tr>
        <tr style="text-align: center;">
            <td class="font-italic" colspan="2" ><small>Should there be any other questions, please kindly contact 24-Hr Customer Care at customer.care@napinfo.co.id or 1500787</small></td>
        </tr>
    </table>
    <br>
    <footer>
    @if ($banner->rule_value != '0')
    <img src="{{public_path('dist2/img/'.$banner->rule_value)}}" alt=""width="705" height="120" />
    @endif
   </footer>
    

    <div class="page-break"></div>
    
    <img  src="{{public_path('dist2/img/header-2.JPG')}}" alt="" width="705" height="140" />
    <small><b>Melalui BCA Online (ATM, Internet Banking, Mobile Banking) - Dicek Otomatis</b></small>
    <table border="0" width="100%">
        <tr>
            <td><small>1.</small></td>
            <td><small>Pilih Menu Transfer</small></td>
        </tr>
        <tr>
            <td><small>2.</small></td>
            <td><small>Pilih Transfer ke BCA Virtual Account</small></td>
        </tr>
        <tr>
            <td><small>3.</small></td>
            <td><small>Masukan Nomor Rekening Virtual Account 02567+99+5 Angka Terakhir No. Customer+9999</small></td>
        </tr>
        <tr>
            <td><small>4.</small></td>
            <td><small>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</small></td>
        </tr>
        <tr>
            <td><small>5.</small></td>
            <td><small>Masukkan PIN Anda</small></td>
        </tr>
        <tr>
            <td><small>6.</small></td>
            <td><small>Transaksi Selesai, Silahkan Simpan Struk Anda</small></td>
        </tr>
    </table>
    <br>
    <small><b>Melalui BRI Online (ATM, Internet Banking, Mobile Banking) - Dicek Manual</b></small>
    <table border="0" width="100%">
        <tr>
            <td><small>1.</small></td>
            <td><small>Pilih menu BRIVA jika memiliki rekening BRI</small></td>
        </tr>
        <tr>
            <td><small>2.</small></td>
            <td><small>Pilih Menu Transfer Bank Lain Jika Tidak Memiliki Rekening BRI</small></td>
        </tr>
        <tr>
            <td><small>3.</small></td>
            <td><small>Masukan Nomor Rekening Virtual Account 13946+5 Angka Terakhir No. Customer</small></td>
        </tr>
        <tr>
            <td><small>4.</small></td>
            <td><small>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</small></td>
        </tr>
        <tr>
            <td><small>5.</small></td>
            <td><small>Masukkan PIN Anda</small></td>
        </tr>
        <tr>
            <td><small>6.</small></td>
            <td><small>Transaksi Selesai, Silahkan Simpan Struk Anda</small></td>
        </tr>
    </table>
    <br>
   <small><b>Melalui Indomaret, Ceriamart - Dicek Otomatis</b></small>
    <table border="0" width="100%">
        <tr>
            <td><small>1.</small></td>
            <td><small>Pelanggan Datang ke Gerai Indomaret atau Ceriamart Terdekat</small></td>
        </tr>
        <tr>
            <td><small>2.</small></td>
            <td><small>Sampaikan Kepada Kasir Pembayaran Biller Matrix NAP Info</small></td>
        </tr>
        <tr>
            <td><small>3.</small></td>
            <td><small>Sampaikan Kode Pembayaran: 5 Angka Terakhir No. Customer (Contoh : {{substr($pdf_header->CardCode,2)}})</small></td>
        </tr>
        <tr>
            <td><small>4.</small></td>
            <td><small>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</small></td>
        </tr>
        <tr>
            <td><small>5.</small></td>
            <td><small>Transaksi Selesai, Silahkan Simpan Struk Anda</small></td>
        </tr>
    </table>
    <br>
    <small><b>Via Apps Klikindomaret, website https://www.klikindomaret.com/ - Dicek Otomatis</b></small>
    <table border="0" width="100%">
        <tr>
            <td><small>1.</small></td>
            <td><small>Download Aplikasi Klikindomaret atau Kunjungi Web Klikindomaret</small></td>
        </tr>
        <tr>
            <td><small>2.</small></td>
            <td><small>Pilih Pembayaran Internet & TV</small></td>
        </tr>
        <tr>
            <td><small>3.</small></td>
            <td><small>Masukan Nama Biller Matrix NAP Info di Pencarian</small></td>
        </tr>
        <tr>
            <td><small>4.</small></td>
            <td><small>Masukan Nomor ID Pelanggan (5 Angka Terakhir No. Customer) - Contoh : {{substr($pdf_header->CardCode,2)}}</small></td>
        </tr>
        <tr>
            <td><small>5.</small></td>
            <td><small>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</small></td>
        </tr>
        <tr>
            <td><small>6.</small></td>
            <td><small>Transaksi Selesai, Silahkan Simpan Struk Anda</small></td>
        </tr>
    </table>
    <br>
    <small><b>Via Portal Customer (Virtual Account Mandiri, Virtual Account BNI, Virtual Account Permata, GoPay)</b></small>
    <table border="0" width="100%">
        <tr>
            <td><small>1.</small></td>
            <td><small>Pelanggan login ke Matrix Broadband Customer Portal (https://portal.napinfo.co.id)</small></td>
        </tr>
        <tr>
            <td><small>2.</small></td>
            <td><small>Masukkan User ID (Nomor Pelanggan)</small></td>
        </tr>
        <tr>
            <td><small>3.</small></td>
            <td><small>Masukkan Password yang Telah Dikirimkan Melalui Email Ketika Aktivasi Selesai Dilakukan</small></td>
        </tr>
        <tr>
            <td><small>4.</small></td>
            <td><small>Klik PayNow</small></td>
        </tr>
        <tr>
            <td><small>5.</small></td>
            <td><small>Pilih Metode Pembayaran (BNI Virtual Account, Mandiri Virtual Account, Permata Virtual Account, GoPay)</small></td>
        </tr>
        <tr>
            <td><small>6.</small></td>
            <td><small>Klik Bayar Lalu Akan Muncul Payment Message</small></td>
        </tr>
        <tr>
            <td><small>7.</small></td>
            <td><small>Klik Menu Payment Kemudia Silahkan Melakukan Pembayaran Sesuai Dengan Virtual Account Yang Tertera Pada Matrix Broadband Customer Portal</small></td>
        </tr>
        <tr>
            <td><small>8.</small></td>
            <td><small>Transaksi selesai, Silahkan Simpan Struk Anda</small></td>
        </tr>
    </table>
    <br>
    <small><b>Via Aplikasi DANA - Dicek Otomatis</b></small>
    <table border="0" width="100%">
        <tr>
            <td><small>1.</small></td>
            <td><small>Buka Aplikasi DANA</small></td>
        </tr>
        <tr>
            <td><small>2.</small></td>
            <td><small>Pilih menu pembayaran Internet & TV Kabel</small></td>
        </tr>
        <tr>
            <td><small>3.</small></td>
            <td><small>Masukkan nama Matrix NAP Info di kolom pencarian</small></td>
        </tr>
        <tr>
            <td><small>4.</small></td>
            <td><small>Masukkan ID pelanggan/ CID pelanggan</small></td>
        </tr>
        <tr>
            <td><small>5.</small></td>
            <td><small>Klik Cek Tagihan dan jika nominal sudah sesuai, klik Bayar</small></td>
        </tr>
        <tr>
            <td><small>6.</small></td>
            <td><small>Transaksi selesai, silahkan simpan bukti pembayaran Anda</small></td>
        </tr>
    </table>
</body>
</html>