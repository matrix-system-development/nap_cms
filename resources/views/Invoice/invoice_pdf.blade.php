<!DOCTYPE html>
<html>

<head>
	<title>REASON FOR OUTAGE</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<style>
		#footer {
			position: fixed;
			padding: 10px 10px 0px 10px;
			bottom: 0;
			width: 100%;
			/* Height of the footer*/
			height: 40px;
		}

        table{
            font-size: 12px;
        }
		tr,
		td {
			padding-left: 15px;
		}

        th{
            text-align: center;
        }

        .page-break {
            page-break-after: always;
        }
        <?php include(public_path().'/dist2/css/pdf.css');?>
	</style>

</head>
<body style="font-size: 12px">
	<img <img src="{{public_path('dist2/img/header_mail.JPG')}}" alt="" width="700" height="140" />
    <center><b>TAGIHAN MATRIX NAP INFO</b></center>
    <table border="1">
        <tr>
            <td width="50%" rowspan="5">
                <b>{{$pdf_header->CardName}}</b>
                <br>
                {{str_replace("$pdf_header->CardName","",$pdf_header->EnvAddress)}}
            </td>
            <td width="25%">Nomor Pelanggan</td>
            <td width="25%" style="border-left-style: hidden;">: {{$pdf_header->CardCode}}</td>
        </tr>
        <tr>
            <td width="25%">Nomor Tagihan</td>
            <td width="20%" style="border-left-style: hidden;">: {{$pdf_header->DocNum}}</td>
        </tr>
        <tr>
            <td width="25%">Tanggal Tagihan</td>
            <td width="20%" style="border-left-style: hidden;">: {{date('d-M-Y', strtotime($pdf_header->DocDate))}}</td>
        </tr>
        <tr>
            <td width="25%">Tanggal Jatuh Tempo</td>
            <td width="20%" style="border-left-style: hidden;">: {{$due_date}}</td>
        </tr>
        <tr>
            <td width="25%">Total Tagihan</td>
            <td width="20%" style="border-left-style: hidden;">: {{number_format(floor($pdf_header->current_outstanding),2,",",".") }}</td>
        </tr>
    </table>
    <br>
    
    <table border="1" width="100%">
        <tr style="text-align: center;">
            <th width="20%">Tanggal</th>
            <th width="60%">Rincian Transaksi</th>
            <th width="20%">Jumlah</th>
        </tr>
        <tr>
            <td width="20%">{{$date_desc_previous_bill}}</td>
            <td width="60%">Tagihan Bulan Sebelumnya :</td>
            <td>{{number_format(floor($previous_bill_detail),2,",",".")}}</td>
        </tr>
        <tr>
           <td width="20%">{{$date_previous_bill}}</td>
            <td width="60%">Pembayaran Diterima :</td>
            <td>{{number_format(floor($payment),2,",",".")}}</td>
        </tr>
        <tr>
            <td></td>
            <td width="60%">Jumlah Terhutang :</td>
            <td>{{number_format(floor($Debt),2,",",".") }}</td>
        </tr>
        @foreach ($descripton as $data)
        <tr>
            @php
            if (str_contains($data->Description, 'Period')) {
                $count_char =  strrpos($data->Description,"(");
            $select_date = substr($data->Description,$count_char,12);
            $deleteChar = substr($select_date,-11);
            $date_desc = str_replace(".","-",$deleteChar);
            } else {
                $date_desc = null;
            }
            
           
            @endphp
            <td width="20%">{{$date_desc}}</td>
            <td width="60%">{{$data->Description}}</td>
           
            <td>{{number_format($data->TotalPrice,2,",",".")}}</td>
        </tr>
        @endforeach
        <tr>
            <td width="20%" rowspan="3"></td>
            <td width="60%">Total :</td>
            <td>{{number_format($pdf_footer->Amount,2,",",".")}}</td>
        </tr>
        <tr>
            <td width="60%">PPN 11% :</td>
            <td>{{number_format($pdf_footer->PPN,2,",",".")}}</td>
        </tr>
        <tr>
            <td width="60%">Total Tagihan :</td>
            <td>{{number_format($pdf_footer->TotalAmount,2,",",".") }}</td>
        </tr>
    </table>
    <br>
    
    <center><b>METODE PEMBAYARAN</b></center>
    Pembayaran Matrix NAP Info dapat dilakukan melalui :
    <table border="0" width="100%">
        <tr>
            <td><b>1.</b></td>
            <td colspan="2"><b>Virtual Account BCA</b></td>
            <td><b>4.</b></td>
            <td colspan="2"><b>Apps Klikindomaret, Website Klikindomaret, Apps i.Saku</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Account Name: </td>
            <td>{{$pdf_header->VA}}</td>
            <td></td>
            <td>Nama Biller: </td>
            <td>Matrix NAP Info</td>
        </tr>
        <tr>
            <td></td>
            <td>Account Number: </td>
            <td>{{$pdf_header->CustCodeVA}}</td>
            <td></td>
            <td>Kode Pembayaran: </td>
            <td>{{substr($pdf_header->CardCode,2)}}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><b>2.</b></td>
            <td colspan="2"><b>Virtual Account BRI</b></td>
            <td><b>5.</b></td>
            <td colspan="2"><b>Midtrans (Bank Permata, Bank BNI, Bank Mandiri, GoPay)</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Account Name: </td>
            <td>{{$pdf_header->VA}}</td>
            <td></td>
            <td>User ID: </td>
            <td>{{$pdf_header->CardCode}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Account Number: </td>
            <td>{{ "13946.".substr($pdf_header->CardCode,2)}}</td>
            <td></td>
            <td>Password: </td>
            <td>Dikirimkan Melalui email</td>
        </tr>
        <tr>
            <td><b>3.</b></td>
            <td colspan="2"><b>Indomaret, Ceriamart</b></td>
            <td></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td></td>
            <td>Nama Biller: </td>
            <td>Matrix NAP Info</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>Nama Biller: </td>
            <td>Matrix NAP Info</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>Kode Pembayaran: </td>
            <td>{{substr($pdf_header->CardCode,2)}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
	<div id="footer">
		<img <img src="{{public_path('dist2/img/footer-new.png')}}" alt="" width="700" height="70" />
	</div>

    <div class="page-break"></div>

    <img <img src="{{public_path('dist2/img/header_mail.JPG')}}" alt="" width="700" height="140" />
	<br>
    <center><b>CARA PEMBAYARAN</b></center>
    <br>
    <b>Melalui BCA Online (ATM, Internet Banking, Mobile Banking) - Dicek Otomatis</b>
    <table border="0" width="100%">
        <tr>
            <td>1.</td>
            <td>Pilih Menu Transfer</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Pilih Transfer ke BCA Virtual Account</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Masukan Nomor Rekening Virtual Account 02567+99+5 Angka Terakhir No. Customer+9999</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Masukkan PIN Anda</td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Transaksi Selesai, Silahkan Simpan Struk Anda</td>
        </tr>
    </table>
    <br>
    <b>Melalui BRI Online (ATM, Internet Banking, Mobile Banking) - Dicek Manual</b>
    <table border="0" width="100%">
        <tr>
            <td>1.</td>
            <td>Pilih menu BRIVA jika memiliki rekening BRI</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Pilih Menu Transfer Bank Lain Jika Tidak Memiliki Rekening BRI</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Masukan Nomor Rekening Virtual Account 13946+5 Angka Terakhir No. Customer</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Masukkan PIN Anda</td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Transaksi Selesai, Silahkan Simpan Struk Anda</td>
        </tr>
    </table>
    <br>
    <b>Melalui Indomaret, Ceriamart - Dicek Otomatis</b>
    <table border="0" width="100%">
        <tr>
            <td>1.</td>
            <td>Pelanggan Datang ke Gerai Indomaret atau Ceriamart Terdekat</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Sampaikan Kepada Kasir Pembayaran Biller Matrix NAP Info</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Sampaikan Kode Pembayaran: 5 Angka Terakhir No. Customer (Contoh : {{substr($pdf_header->CardCode,2)}})</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Transaksi Selesai, Silahkan Simpan Struk Anda</td>
        </tr>
    </table>
    <br>
    <b>Via Apps Klikindomaret, website https://www.klikindomaret.com/ - Dicek Otomatis</b>
    <table border="0" width="100%">
        <tr>
            <td>1.</td>
            <td>Download Aplikasi Klikindomaret atau Kunjungi Web Klikindomaret</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Pilih Pembayaran Internet & TV</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Masukan Nama Biller Matrix NAP Info di Pencarian</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Masukan Nomor ID Pelanggan (5 Angka Terakhir No. Customer) - Contoh : {{substr($pdf_header->CardCode,2)}}</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Cek Nominal Tagihan, Jika Sudah Sesuai Maka Lanjutkan Pembayaran</td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Transaksi Selesai, Silahkan Simpan Struk Anda</td>
        </tr>
    </table>
    <br>
    <b>Via Midtrans (VA Bank Mandiri, VA Bank BNI, VA Bank Permata, GoPay)</b>
    <table border="0" width="100%">
        <tr>
            <td>1.</td>
            <td>Pelanggan login ke Matrix Broadband Customer Portal (https://portal.napinfo.co.id)</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Masukkan User ID (Nomor Pelanggan)</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Masukkan Password yang Telah Dikirimkan Melalui Email Ketika Aktivasi Selesai Dilakukan</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Klik PayNow</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Pilih Metode Pembayaran (BNI Virtual Account, Mandiri Virtual Account, Permata Virtual Account, GoPay)</td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Klik Bayar Lalu Akan Muncul Payment Message</td>
        </tr>
        <tr>
            <td>7.</td>
            <td>Klik Menu Payment Kemudian Silahkan Melakukan Pembayaran Sesuai Dengan Kode Atau Nomor Virtual Yang Tampil Pada Matrix Broadband Customer Portal</td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Transaksi selesai, Silahkan Simpan Struk Anda</td>
        </tr>
    </table>
	<div id="footer">
		<img <img src="{{public_path('dist2/img/footer-new.png')}}" alt="" width="700" height="70" />
	</div>

    <div class="page-break"></div>

    <img <img src="{{public_path('dist2/img/header_mail.JPG')}}" alt="" width="700" height="140" />
	<br>
    <center><b>INFORMASI PENTING</b></center>
    <br>
    <table border="0" width="100%" style="font-weight: bold">
        <tr>
            <td>1.</td>
            <td>Pelanggan dapat melakukan pembayaran melalui Virtual Account BCA (ATM BCA, Klik BCA, Mobile BCA)</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Pelanggan dapat melakukan pembayaran melalui Virtual Account BRI (ATM BRI, Mobile BRI, Internet Banking BRI)</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Pelanggan dapat melakukan pembayaran di Gerai Indomaret, Gerai Ceriamart, Gerai Indogrosir, Gerai Superindo, Website https://www.klikindomaret.com/ , Mobile Apps Klik Indomaret dan Mobile Apps i.saku</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Pembayaran yang sah hanya dapat dilakukan melalui metode pembayaran di atas, Matrix NAP Info tidak menerima pembayaran apapun yang diterima oleh Karyawan kami</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Nominal tagihan dapat dilihat pada metode pembayaran di atas minimal H+1 dari tanggal invoice</td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Dihimbau untuk seluruh pelanggan Matrix NAP Info agar melakukan pembayaran tepat waktu demi menghindari layanan Anda terisolir</td>
        </tr>
        <tr>
            <td>7.</td>
            <td>Matrix NAP Info tidak bertanggung jawab apabila pembayaran tidak diterima karena hal-hal seperti: nomor rekening bank tujuan salah, kartu kredit bermasalah, informasi dan identitas pelanggan yang tertera tidak ada atau tidak terindetinfikasi</td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Tanpa informasi billing statement ini, pelanggan tetap berkewajiban membayaran tagihan Matrix NAP Info sebelum tanggal jatuh tempo</td>
        </tr>
        <tr>
            <td>9.</td>
            <td>Tanggal jatuh tempo customer baru adalah H+7 dari tanggal invoice</td>
        </tr>
        <tr>
            <td>10.</td>
            <td>Tanggal jatuh customer lama adalah tanggal 25 setiap bulannya</td>
        </tr>
        <tr>
            <td>11.</td>
            <td>Matrix NAP Info berhak melakukan usaha-usaha penagihan dan penghentian layanan apabila terdapat tunggakan atas tagihan</td>
        </tr>
        <tr>
            <td>12.</td>
            <td>Peralatan yang terpasang adalah bersifat pinjaman pakai dan minimum berlangganan adalah 12 bulan</td>
        </tr>
        <tr>
            <td>13.</td>
            <td>Penghentian berlangganan sebelum 12 bulan maka akan dikenakan biaya penalty dan peralatan akan ditarik kembali</td>
        </tr>
        <tr>
            <td>14.</td>
            <td>Apabila terjadi kerusakan atau kehilangan pada peralatan yang terpasang, maka akan menjadi tanggung jawab pelanggan</td>
        </tr>
        <tr>
            <td>15.</td>
            <td>Jika Anda memiliki pertanyaan dan hal lain yang ingin disampaikan, mohon menghubungi Customer Care 24 jam kami via email di customer.care@napinfo.co.id atau telepon di nomor 1500787</td>
        </tr>
        <tr>
            <td>16.</td>
            <td>Silahkan abaikan tagihan ini apabila telah melakukan pembayaran</td>
        </tr>
    </table>
    <div id="footer">
		<img <img src="{{public_path('dist2/img/footer-new.png')}}" alt="" width="700" height="70" />
	</div>
</body>
</html>