<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//FINANCE INFO
Route::get('/billing-all/{cust_code}', 'App\Http\Controllers\API\ApiController@billingByCustCode');
Route::get('/billingcorp-all/{cust_code}', 'App\Http\Controllers\API\ApiController@billingCorpByCustCode');
Route::get('/payment-all/{cust_code}', 'App\Http\Controllers\API\ApiController@paymentByCustCode');
Route::get('/outstanding-all/{inv_period}', 'App\Http\Controllers\API\ApiController@outstandingByInvDate');
Route::get('/outstanding-cust/{cust_code}', 'App\Http\Controllers\API\ApiController@outstandingByCustCode');
Route::post('/outstanding-cust/search', 'App\Http\Controllers\API\ApiController@outstandingSearch');

//RADIUS
Route::get('/rad-info/{cust_code}', 'App\Http\Controllers\API\ApiController@radInfoByCustCode');

//CUSTOMER PORTAL
Route::get('/custportal-info/{cust_code}', 'App\Http\Controllers\API\CustPortalController@CustPortalByCustCode');
Route::post('/custportal-info/create', 'App\Http\Controllers\API\CustPortalController@AddUser');