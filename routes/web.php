<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BillingController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\QuarantineController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\apvTempController;
use App\Http\Controllers\DanaController;
use App\Http\Controllers\FtpController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MtTransactionController;
use App\Http\Controllers\BillingCorpsController;
use App\Http\Controllers\BillingApprovalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth
Route::get('/', [AuthController::class,'login'])->name('login');
Route::get('/redirect-cms/{q}', [AuthController::class,'postloginSSO']); //kalau pakai SSO
Route::post('/postlogin', [AuthController::class,'postlogin'])->middleware("throttle:5,1");
Route::get('/logout', [AuthController::class,'logout']);
//end Auth

//notif
Route::get('/notif', [AuthController::class,'notif']);
//end notif

//Reset password by customer self
Route::get('/user-cust/change-pass/{custCode}',[UserController::class, 'resetPassSelf']);
Route::post('/user-cust/submit-change-pass',[UserController::class, 'changePassSelf']);
Route::get('/user-cust/success-change-pass',[UserController::class, 'successChangePass']);
//end

//DOWNLOAD INVOICE
Route::get('/invoice/pdf/{DocNum}',[InvoiceController::class, 'downloadPdf']);
Route::get('/invoice/pdfCorp/{DocNum}/{DocDate}',[InvoiceController::class, 'downloadPdfCorp']);
//DOWNLOAD INVOICE

Route::group(['middleware'=>'auth'],function() {
    Route::get('/home', [HomeController::class, 'index']);
    // Route::get('/block-cust', [HomeController::class, 'menu']);
    // Route::get('/unblock-cust', [HomeController::class, 'menu']);

    //USER
    Route::resource('/user', 'App\Http\Controllers\UserController')->middleware(['checkRole:Super Admin']);
    Route::get('/user',[UserController::class, 'index'])->middleware(['checkRole:Super Admin']);
    Route::post('/user',[UserController::class, 'index'])->middleware(['checkRole:Super Admin']);
    Route::post('/user/store',[UserController::class, 'store'])->middleware(['checkRole:Super Admin']);
    Route::get('/user/revoke/{user}',[UserController::class, 'revoke'])->middleware(['checkRole:Super Admin']);
    Route::get('/user/access/{user}',[UserController::class, 'access'])->middleware(['checkRole:Super Admin']);
    Route::get('/user/access-ca/{user}',[UserController::class, 'accessCA'])->middleware(['checkRole:Super Admin']);
    Route::get('/user/revoke-ca/{user}',[UserController::class, 'revokeCA'])->middleware(['checkRole:Super Admin']);

    //USER CUSTOMER
    Route::resource('/user-cust', 'App\Http\Controllers\UserController')->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);
    Route::get('/user-cust', [UserController::class, 'indexCust'])->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);
    Route::post('/user-cust', [UserController::class, 'indexCust'])->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);
    Route::post('/user-cust/store',[UserController::class, 'storeCust'])->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);
    Route::post('/user-cust/change-email-cust',[UserController::class, 'changeEmailCust'])->middleware(['checkRole:Super Admin,Sales Support']);
    Route::get('/user-cust/revoke/{user}',[UserController::class, 'revokeCust'])->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);
    Route::get('/user-cust/access/{user}',[UserController::class, 'accessCust'])->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);
    Route::get('/user-cust/reset-pass-cust/{user}',[UserController::class, 'resetPassCust'])->middleware(['checkRole:Super Admin,Customer Care,Sales Support']);

    //CUSTOMER
    Route::get('/mon-cust',[CustomerController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance']);
    Route::post('/mon-cust',[CustomerController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance']);
    Route::get('/mon-cust/block/{cid}',[CustomerController::class, 'block'])->middleware(['checkRole:Super Admin,Customer Care,User Network,User Finance']);
    Route::get('/mon-cust/unblock/{cid}',[CustomerController::class, 'unblock'])->middleware(['checkRole:Super Admin,Customer Care,User Network,User Finance']);
    Route::get('/mon-cust/unblock2/{cid}',[CustomerController::class, 'unblock2'])->middleware(['checkRole:Super Admin,Customer Care,User Network,User Finance']);
    
    Route::get('/mst-cust',[CustomerController::class, 'indexMstCust'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance']);
    Route::post('/mst-cust',[CustomerController::class, 'indexMstCust'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance']);
    Route::get('/mst-broadband/',[CustomerController::class, 'indexBroadband'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance,Sales Broadband,Marcomm']);
    Route::post('/mst-broadband/',[CustomerController::class, 'indexBroadband'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance,Sales Broadband,Marcomm']);
    Route::get('/mst-broadband/net-status/{cid}',[CustomerController::class, 'detailNetStatus'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance,Sales Broadband,Marcomm']);
    Route::get('/mst-broadband/fin-status/{cid}',[CustomerController::class, 'detailFinStatus'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Network,User Finance,Sales Broadband,Marcomm']);
    // Route::get('/import',[CustomerController::class, 'importUpload']);
    // Route::post('/import-process',[ImportController::class, 'importlist']);
    // Route::get('/mass-block/{token}',[CustomerController::class, 'massBlock']);
    // Route::get('/mass-unblock/{token}',[CustomerController::class, 'massUnblock']);
    // Route::get('/mass-list-delete/{token}',[CustomerController::class, 'massListDelete']);
    Route::get('/quarantines',[QuarantineController::class, 'index'])->middleware(['checkRole:Super Admin,User,Customer Care,BOD,User Network,User Finance,Sales Broadband']);
    Route::post('/quarantines',[QuarantineController::class, 'index'])->middleware(['checkRole:Super Admin,User,Customer Care,BOD,User Network,User Finance,Sales Broadband']);
    Route::get('/apv-temp-unblock',[apvTempController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/apv-temp-unblock/log/{id}',[apvTempController::class, 'detailLog'])->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/apv-temp-unblock/reject/{id}/{custCode}',[apvTempController::class, 'rejectApproval'])->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/apv-temp-unblock/approve/{id}/{custCode}',[apvTempController::class, 'approveApproval'])->middleware(['checkRole:Super Admin,Customer Care']);

    //MONITORING MIDTRANS
    Route::get('/mt-monitor',[MtTransactionController::class, 'index'])->middleware(['checkRole:Super Admin,BOD,User Finance']);
    Route::post('/mt-monitor',[MtTransactionController::class, 'index'])->middleware(['checkRole:Super Admin,BOD,User Finance']);
    Route::get('/mt-monitor/detail/{order_id}',[MtTransactionController::class, 'show'])->middleware(['checkRole:Super Admin,BOD,User Finance']);

    //MONITORING DANA
    Route::get('/dana-monitor',[DanaController::class, 'index'])->middleware(['checkRole:Super Admin,BOD,User Finance']);
    Route::post('/dana-monitor',[DanaController::class, 'index'])->middleware(['checkRole:Super Admin,BOD,User Finance']);

    //ANNOUNCEMENT
    Route::resource('/announcement', 'App\Http\Controllers\AnnouncementController')->middleware(['checkRole:Super Admin,Marcomm']);
    Route::get('/announcement/revoke/{id}',[AnnouncementController::class, 'revoke'])->middleware(['checkRole:Super Admin,Marcomm']);
    Route::get('/announcement/active/{id}',[AnnouncementController::class, 'active'])->middleware(['checkRole:Super Admin,Marcomm']);

    //PAYMENT
    Route::get('/payment/list', 'App\Http\Controllers\PaymentController@listPayment')->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband']);
    Route::post('/payment/list', 'App\Http\Controllers\PaymentController@listPayment')->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband']);
    Route::get('/payment/auto/create/{reqID}', 'App\Http\Controllers\PaymentController@createAutoPayment')->middleware(['checkRole:Super Admin,User Finance']);
    Route::post('/payment/auto/store', 'App\Http\Controllers\PaymentController@storeAutoPayment')->middleware(['checkRole:Super Admin,User Finance']);
    Route::resource('/payment', 'App\Http\Controllers\PaymentController')->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband,Sales Corporate']);
    Route::post('/payment/result', 'App\Http\Controllers\PaymentController@checkPayment')->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband,Sales Corporate']);
    
    //BILLING
    //for period current month
    Route::get('/billing',[BillingController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband']);
    //for period by filter
    Route::post('/billing',[BillingController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband']);
    Route::post('/billing-search',[BillingController::class, 'indexSearch'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband']);
    Route::get('/billing/{inv}',[BillingController::class, 'show'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Broadband']);
    Route::get('/billing-adjustment', [BillingApprovalController::class, 'BillingAdjustment'])->name('billing.adjustment')->middleware(['checkRole:Super Admin,User Finance']);
    Route::post('/find-billing',[BillingApprovalController::class, 'FindBilling'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::post('/req-update',[BillingApprovalController::class, 'ReqUpdate'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::get('/billing-adjustment/cancel/{docnum}', [BillingApprovalController::class, 'CancelReq'])->middleware(['checkRole:Super Admin,User Finance'])->name('cancelDocument');
    Route::get('/history-req', [BillingApprovalController::class, 'HistoryReq'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::get('/acc-req/{docnum}', [BillingApprovalController::class, 'AccReq'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::post('/rej-req', [BillingApprovalController::class, 'RejReq'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::post('/history-search', [BillingApprovalController::class, 'HistorySearch'])->middleware(['checkRole:Super Admin,User Finance']);

    //INVOICE
    Route::get('/invoice',[InvoiceController::class, 'index'])->middleware(['checkRole:Super Admin']);
    Route::get('/invoice/sent/{DocNum}/{Email}',[InvoiceController::class, 'sentPdf'])->middleware(['checkRole:Super Admin']);
    Route::post('/invoice-search',[InvoiceController::class, 'indexSearch'])->middleware(['checkRole:Super Admin']);
    Route::post('/invoice/import-remarks',[InvoiceController::class, 'import_remark'])->middleware(['checkRole:Super Admin,User Finance']);;
    Route::post('/invoice/import-delete',[InvoiceController::class, 'import_delete'])->middleware(['checkRole:Super Admin,User Finance']);

    //LOG
    Route::get('/log',[LogController::class, 'index'])->middleware(['checkRole:Super Admin,User,Customer Care,BOD,User Network,User Finance,Sales Broadband']);
    Route::post('/log',[LogController::class, 'index'])->middleware(['checkRole:Super Admin,User,Customer Care,BOD,User Network,User Finance,Sales Broadband']);
    Route::post('/log-search',[LogController::class, 'indexSearch'])->middleware(['checkRole:Super Admin,User,Customer Care,BOD,User Network,User Finance,Sales Broadband']);
    Route::get('/api-log',[LogController::class, 'indexApi'])->middleware(['checkRole:Super Admin']);
    Route::post('/api-log',[LogController::class, 'indexApi'])->middleware(['checkRole:Super Admin']);
    Route::get('/audit-log',[LogController::class, 'indexAudit'])->middleware(['checkRole:Super Admin']);
    Route::get('/remind-billing-log',[LogController::class, 'indexRemindBilling'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::post('/remind-billing-log',[LogController::class, 'indexRemindBilling'])->middleware(['checkRole:Super Admin,User Finance']);
    Route::get('/remind-billing-detail/{id}',[LogController::class, 'detailRemindBilling'])->middleware(['checkRole:Super Admin,User Finance']);

    //RULE
    Route::resource('/rule', 'App\Http\Controllers\RuleController')->middleware(['checkRole:Super Admin']);

    //Service Job
    Route::get('/service/job',[JobController::class, 'index'])->middleware(['checkRole:Super Admin']);
    Route::get('/service/run/{service}',[JobController::class, 'runJobs'])->middleware(['checkRole:Super Admin']);

    //CHECKEMAIL USER
    Route::post('/checkemail',[HomeController::class, 'checkEmail']);

    //EXPORT
    Route::post('/export-log',[ExportController::class, 'exportLog'])->middleware(['checkRole:Super Admin,User,Customer Care,BOD,User Network,User Finance,Sales Broadband']);
    Route::post('/export-mt-trans',[ExportController::class, 'exportMT_trans'])->middleware(['checkRole:Super Admin,BOD,User Finance']);
    Route::post('/export-payment',[ExportController::class, 'exportPayment'])->middleware(['checkRole:Super Admin,BOD,User Finance,Sales Broadband']);
    Route::post('/export-dana-trans',[ExportController::class, 'exportdana_trans'])->middleware(['checkRole:Super Admin,BOD,User Finance']);
    Route::post('/export-billing',[ExportController::class, 'exportBilling'])->middleware(['checkRole:Super Admin,BOD,User Finance,Sales Broadband']);
    Route::post('/export-billing-corps',[ExportController::class, 'exportBillingCorps'])->middleware(['checkRole:Super Admin,BOD,User Finance,Sales Corporate']);

    //FTP
    Route::get('/ftp/indomaret',[FtpController::class, 'indexIDM'])->middleware(['checkRole:Super Admin']);
    Route::get('/tes/invoice/{DocNum}',[FtpController::class, 'downloadInv'])->middleware(['checkRole:Super Admin']);

    //BILLING CORPORATE
    //for period current month
    Route::get('/billing-corps',[BillingCorpsController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Corporate']);
    //for period by filter
    Route::post('/billing-corps',[BillingCorpsController::class, 'index'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Corporate']);
    Route::post('/billing-search-corps',[BillingCorpsController::class, 'indexSearch'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Corporate']);
    Route::get('/billing-corps/{inv}',[BillingCorpsController::class, 'show'])->middleware(['checkRole:Super Admin,Customer Care,BOD,User Finance,Sales Corporate']);
});
