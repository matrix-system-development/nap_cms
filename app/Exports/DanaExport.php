<?php

namespace App\Exports;

use App\Models\DanaOrder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class DanaExport implements FromCollection, WithHeadings
{
    protected $date_start;
    protected $date_finish;

    function __construct($date_start,$date_finish) {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
    }

    public function collection()
    {
        //pakai updated_at karna cari tgl paling updated
        $query=DanaOrder::whereBetween(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
        ->select(
            'cust_code',
            'DocNum',
            'requestId',
            'reqMsgId',
            'orderId',
            'function',
            'nominal_payment',
            'currency',
            'status',
            DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s")'));

        $danaTrans=$query->orderBy('created_at','asc')->get();

        return $danaTrans;
    }

    public function headings(): array
    {
        return[
            'Customer Code',
            'Inv Number',
            'Request ID',
            'Req Message ID',
            'Order ID',
            'Function',
            'Nominal Payment',
            'Currency',
            'Status',
            'Created At'
        ];
    }
}
