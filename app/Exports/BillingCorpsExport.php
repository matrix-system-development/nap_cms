<?php

namespace App\Exports;
use App\Models\BillingCorp;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class BillingCorpsExport implements FromCollection, WithHeadings
{protected $inv_period;

    function __construct($inv_period) {
        $this->inv_period = $inv_period;
    }

    public function collection()
    {
        $current_month = date('m', strtotime($this->inv_period));
        $current_year = date('Y', strtotime($this->inv_period));
        
        //dd($current_month);
        $query_billings=BillingCorp::whereMonth('DocDate',$current_month)
        ->whereYear('DocDate',$current_year)
        ->leftJoin('temp_check_payments','billing_corps.DocNum','temp_check_payments.inv_number')
        ->select(
            'DocNum',
            'DocDate',
            'CardName',
            'CardCode',
            'TotalAmount',
            'nominal_payment',
            'PaymentStatus',
            'Outstanding as outstanding',
            'NewCustomer',
            'Sales_Employee'
        )
        ->groupBy('DocNum')
        ->get();
        //dd($query_billings);
        return $query_billings;
    }

    public function headings(): array
    {
        return[
            'No Invoice',
            'Date',
            'Customer',
            'Customer Code',
            'Document Total',
            'Total Paid Sum',
            'Payment Status',
            'Sisa Tagihan',
            'New Customer',
            'Sales Name'
        ];
    }
}
