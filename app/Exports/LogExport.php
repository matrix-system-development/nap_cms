<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Log;
use Illuminate\Support\Facades\DB;

class LogExport implements FromCollection, WithHeadings
{
    protected $date_start;
    protected $date_finish;
    protected $action_by;

    function __construct($date_start,$date_finish,$action_by) {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->action_by = $action_by;
    }

    public function collection()
    {
        $query=Log::whereBetween(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
        ->select(
            'cid_log',
            'username_log',
            'message_log',
            'action_by_log',
            'created_by_log',
            'remarks_log',
            DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s")'));

        if($this->action_by <> ''){
            $query=$query -> where('action_by_log',$this->action_by);
        }

        $logs=$query->orderBy('created_at','asc')->get();

        return $logs;
    }

    public function headings(): array
    {
        return[
            'Customer Code',
            'Username',
            'Message',
            'action_by',
            'created_by',
            'Remarks',
            'Created At',
        ];
    }
}
