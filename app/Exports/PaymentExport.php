<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\TempCheckPayment;
use Illuminate\Support\Facades\DB;

class PaymentExport implements FromCollection, WithHeadings
{
    protected $date_start;
    protected $date_finish;
    protected $pay_channel;

    function __construct($date_start,$date_finish,$pay_channel) {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->pay_channel = $pay_channel;
    }

    public function collection()
    {
        //pakai updated_at karna cari tgl paling updated
        $query=TempCheckPayment::whereBetween(DB::raw("(STR_TO_DATE(temp_check_payments.created_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
        ->select(
            'inv_number',
            'cust_code',
            'billings.CardName',
            'nominal_payment',
            'payment_channel',
            DB::raw('DATE_FORMAT(billings.DocDate, "%Y-%m-%d")'),
            DB::raw('DATE_FORMAT(payment_date, "%Y-%m-%d %H:%i:%s")'),
            DB::raw('DATE_FORMAT(temp_check_payments.created_at, "%Y-%m-%d %H:%i:%s")'),
            'RequestID',
            'is_sent_SAP',
            'is_sync_SAP',
            'SAP_payment_no'
        )
        ->leftJoin('billings','temp_check_payments.inv_number','billings.DocNum');

        if($this->pay_channel <> ''){
            $query=$query -> where('payment_channel',$this->pay_channel);
        }

        $payTrans=$query->orderBy('temp_check_payments.created_at','asc')->get();

        return $payTrans;
    }

    public function headings(): array
    {
        return[
            'Invoice Number',
            'Customer Code',
            'Customer Name',
            'Nominal Payment',
            'Payment Channel',
            'Invoice Date',
            'Payment Date',
            'Payment Created Date',
            'Request ID',
            'Sent SAP Status',
            'Sync SAP Status',
            'SAP Payment Number'
        ];
    }
}
