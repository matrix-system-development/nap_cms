<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\MtTransaction;
use Illuminate\Support\Facades\DB;

class PGExport implements FromCollection, WithHeadings
{
    protected $date_start;
    protected $date_finish;
    protected $trans_status;

    function __construct($date_start,$date_finish,$trans_status) {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->trans_status = $trans_status;
    }

    public function collection()
    {
        //pakai updated_at karna cari tgl paling updated
        $query=MtTransaction::whereBetween(DB::raw("(STR_TO_DATE(updated_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
        ->select(
            'cust_code',
            'DocNum',
            'status_code',
            'status_message',
            'transaction_id',
            'order_id',
            'nominal_payment',
            'via',
            'transaction_status',
            DB::raw('DATE_FORMAT(updated_at, "%Y-%m-%d %H:%i:%s")'));

        if($this->trans_status <> ''){
            $query=$query -> where('transaction_status',$this->trans_status);
        }

        $mttrans=$query->orderBy('created_at','asc')->get();

        return $mttrans;
    }

    public function headings(): array
    {
        return[
            'Customer Code',
            'Inv No',
            'Status Code',
            'Message',
            'Transaction ID',
            'Order ID',
            'Nominal Payment',
            'Payment Method',
            'Transaction Status',
            'Created Date'
        ];
    }
}
