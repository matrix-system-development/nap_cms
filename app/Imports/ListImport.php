<?php

namespace App\Imports;

use App\Models\MassImport;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ListImport implements ToModel, WithHeadingRow
{
    protected $filename;
    protected $token;
    function __construct($filename,$token) {
        $this->filename = $filename;
        $this->token = $token;
    }

    public function model(array $row)
    {
        return new MassImport([
            'cid' => $row['cid'],
            'upload_by' => "User 1",
            'filename' => $this->filename,
            'token' => $this->token,
        ]);
    }
}
