<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Billing;

class ImportDeleteInvoice implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        /* dd($row); */
         return new Billing([
            'DocNum' => $row['docnum'],
        ]);
    }
}
