<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\BillingCorp;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImporRemarks implements ToModel, WithHeadingRow
{
  
    public function model(array $row)
    {
        /* dd($row); */
         return new BillingCorp([
            'DocNum'                 => $row['docnum'],
            'Remarks'                  => $row['remarks'], 
            'CustomerRef'             => $row['customerref'], 
        ]);
    }
}
