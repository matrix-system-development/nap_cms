<?php

namespace App\Traits;

use App\Models\AjpLog;
use App\Models\Billing;
use App\Models\Rule;
use App\Models\TempCheckPayment;
use Illuminate\Support\Facades\Http;

trait AJPTrait
{
    public function bankCharge($oustanding, $channel_type){
        if ($channel_type == 'Payments Channel Indomaret'){ //IDM
            if($oustanding > 100000.00){
                $bankCharge = 5550.00;
            }
            else{
                $bankCharge = 3330.00;
            }
        }
        elseif($channel_type == 'DANA'){ //DANA
            $bankCharge = 5550.00;
        }
        else{
            $bankCharge = 0;
        }

        return $bankCharge;
    }

    public function glAccount($channel_type){
        if ($channel_type == 'Payments Channel Indomaret'){ //IDM
            $GLAccount = '11200260-01';
        }
        elseif($channel_type == 'DANA'){ //DANA
            $GLAccount = '11200260-01';
        }
        elseif($channel_type == 'BCA - 6010' || $channel_type == 'BCA - 6011' || $channel_type == 'BCA - 6014' || $channel_type == 'BCA - 6016' || $channel_type == 'BCA - 6017' || $channel_type == 'BCA - 6018'){ //BCA
           $GLAccount = '11200230-01';
        }
        else{
            $GLAccount = '-';
        }

        return $GLAccount;
    }

    public function settlementAJP($id_payment, $payment_date){
        //cari data payment nya
        $payment        = TempCheckPayment::where('id', $id_payment)->first();
        $trans_date     = $payment_date;
        $docnum         = $payment->inv_number;
        $channel_type   = $payment->payment_channel;
        $reqID          = $payment->RequestID;

        //cari data billing nya
        $billing    = Billing::where('DocNum', $docnum)->first();
        $custcode   = $billing->CardCode;
        $custname   = $billing->CardName;
        $billamount = $billing->TotalAmount;
        $currency   = $billing->SOCurrency;

        //cari outstanding
        $lastDocNum     = TempCheckPayment::where('RequestID',$reqID)->orderBy('inv_number','desc')->first(); //cari docnum terakhir supaya dapet oustanding terakhir
        $q_oustanding   = Billing::where('DocNum',$lastDocNum->inv_number)->first();
        $oustanding     = $q_oustanding->current_outstanding;
        $bankCharge     = $this->bankCharge($oustanding, $channel_type);
        $GLAccount      = $this->glAccount($channel_type);
        $paidAmount     = $oustanding - $bankCharge;

        //dd($oustanding, $bankCharge, $paidAmount, $billamount, $GLAccount);
        //hit API Payment SAP
        //call url for api from rule
        $rule=Rule::where('rule_name','API Insert Payment')->first();
        $url_insert_payment=$rule->rule_value;

        $request_data = [
            'CustomerNumber'    => $custcode,
            'CustomerName'      => $custname,
            'ChanelType'        => $channel_type,
            'GLAccount'         => $GLAccount,
            'TransactionDate'   => $trans_date,
            'PaidAmount'        => $paidAmount,
            'TotalAmount'       => $oustanding,
            'CurrencyCode'      => $currency,
            'BillAmount'        => $billamount,
            'BillNumber'        => $docnum,
            'BillReference'     => $id_payment,
            'BiayaBank'         => $bankCharge,
            'RequestID'         => $reqID
        ];

        $response = Http::post($url_insert_payment, $request_data);
        if($response['success']=="true"){
            //update is_sent_SAP = 1
            $updateSentStatus = TempCheckPayment::where('id', $id_payment)->update([
                'is_sent_SAP' => '1'
            ]);
            $message = 'success';
            return $message;
        }
        else{
            //store log auto journal payment => log cuma utk yang gagal
            $storeLog = AjpLog::create([
                'id_payment' => $id_payment,
                'error_desc' => $response['message']
            ]);
            $message = 'failed';
            return $message;
        }
    }

    public function checkAJP($id_payment){
        //hit API Payment SAP
        //call url for api from rule
        $rule=Rule::where('rule_name','API Inquiry Temp Payment')->first();
        $url_inquiry_payment=$rule->rule_value;

        $request_data = [
            'BillReference' => $id_payment
        ];

        $response = Http::post($url_inquiry_payment, $request_data);
        if($response['success']=="true"){
            //lakukan loop update status is_sync_SAP = 1 untuk yang IsSync =  Y *Sampe sini
            $results = $response['results'];
            foreach ($results as $result) {
                $id_payment = $result['BillReference'];
                $isSync = $result['IsSync'];
                $SAPPaymentNo = $result['SAPPaymentNo'];

                //validasi yang isync nya bukan "N"
                if($isSync == 'Y'){
                    $updateSyncStatus = TempCheckPayment::where('id', $id_payment)
                        ->update([
                            'is_sync_SAP' => '1',
                            'SAP_payment_no' => $SAPPaymentNo
                        ]);
                }
            }
            $message = 'success';
            return $message;
        }
        else{
            $message = 'failed';
            return $message;
        }
    }
}
