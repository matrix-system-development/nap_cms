<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempUnblockApprovalDetail extends Model
{
    use HasFactory;
    protected $fillable = [ 
        'id_approval', 
        'action', 
        'action_by'
    ];
}
