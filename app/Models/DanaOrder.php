<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DanaOrder extends Model
{
    use HasFactory;
    protected $table = 'dana_create_order';
}
