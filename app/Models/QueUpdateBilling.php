<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueUpdateBilling extends Model
{
    use HasFactory;
    protected $table = 'que_update_billings';
    protected $fillable = [
        'docnum'
    ];
}
