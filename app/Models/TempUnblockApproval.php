<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempUnblockApproval extends Model
{
    use HasFactory;
    protected $fillable = [ 
        'cust_code', 
        'tmp_unblock_period', 
        'approval_status', 
        'request_by', 
        'request_date', 
        'response_by', 
        'response_date'
    ];
}
