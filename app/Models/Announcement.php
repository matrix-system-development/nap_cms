<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_announcement',
        'description',
        'description_en',
        'description_cn',
        'announce_type',
        'status',
        'created_by'
    ];
}
