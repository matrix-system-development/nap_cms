<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRad extends Model
{
    use HasFactory;
    protected $fillable = [
        'cid',
        'username',
        'value',
        'id',
        'groupname',
        'attribute',
        'firstname',
        'lastname',
        'disabled',
    ];
}
