<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempCheckPayment extends Model
{
    use HasFactory;
    protected $fillable = [
      'inv_number',
      'status_check',
      'cust_code',
      'nominal_payment',
      'payment_date',
      'payment_channel',
      'RequestID',
      'is_sent_SAP',
      'is_sync_SAP',
      'SAP_payment_no'
    ];
}
