<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quarantine extends Model
{
    use HasFactory;
    protected $fillable = [
        'cid',
        'temporary_unblock_flag',
        'temporary_unblock_exp',
        'created_by',
        'message',
        'is_approval',
        'id_approval',
    ];
}
