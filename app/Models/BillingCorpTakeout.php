<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingCorpTakeout extends Model
{
    use HasFactory;
    protected $fillable = [
        'CardCode',
        'CardName',
        'PayToCode',
        'CustAddress',
        'EnvAddress',
        'Attention',
        'DocNum',
        'LineNum',
        'DocDate',
        'DocDueDate',
        'CustomerOrderNo',
        'SalesID',
        'Description',
        'SOCurrency',
        'UnitPrice',
        'TotalPrice',
        'Amount',
        'Rebate',
        'PPH23',
        'PPN',
        'TotalAmount',
        'VA',
        'CompanyCodeVA',
        'CustCodeVA',
        'PaymentStatus',
        'CID',
        'CreatedDateINV',
        'Outstanding',
        'NewCustomer',
        'checker',
        'Remarks',
        'CustomerRef'
    ];
}
