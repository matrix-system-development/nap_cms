<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueReminder extends Model
{
    use HasFactory;
    protected $fillable = [
        'cust_code',
        'email_address',
    ];
}
