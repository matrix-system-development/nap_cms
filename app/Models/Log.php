<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;
    protected $fillable = [
        'cid_log',
        'username_log',
        'message_log',
        'action_by_log',
        'created_by_log',
        'remarks_log',
    ];
}
