<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingCorp extends Model
{
    use HasFactory;
    protected $fillable = [
        'CardCode',
        'CardName',
        'PayToCode',
        'CustAddress',
        'EnvAddress',
        'Attention',
        'DocNum',
        'LineNum',
        'DocDate',
        'DocDueDate',
        'CustomerOrderNo',
        'SalesID',
        'Description',
        'SOCurrency',
        'UnitPrice',
        'TotalPrice',
        'Amount',
        'Rebate',
        'PPH23',
        'PPN',
        'TotalAmount',
        'VA',
        'CompanyCodeVA',
        'CustCodeVA',
        'PaymentStatus',
        'CID',
        'CreatedDateINV',
        'Outstanding',
        'NewCustomer',
        'Email',
        'PhoneNo',
        'Service',
        'Kapasitas',
        'Kapasitas_UOM',
        'Asuransi',
        'Billing_Pay_Type',
        'Regional',
        'Sales_Code',
        'Sales_Employee',
        'Advance',
        'OutstandingBulanLalu',
        'current_outstanding',
        'checker',
        'Remarks',
        'CustomerRef'
    ];
}
