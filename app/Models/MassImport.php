<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MassImport extends Model
{
    use HasFactory;
    protected $fillable = [
        'cid',
        'upload_by',
        'filename',
        'token',
    ];
}
