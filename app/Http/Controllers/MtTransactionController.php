<?php

namespace App\Http\Controllers;

use App\Models\Dropdown;
use App\Models\MtTransaction;
use App\Models\MtTransactionDetail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use function Ramsey\Uuid\v1;

class MtTransactionController extends Controller
{
    public function index(Request $request)
    {
        //dd($request->all());

        if($request->date_start=='' && $request->date_finish==''){
            $now=Carbon::now();
            $date_start=Carbon::now()->startOfMonth()->subMonth(6);
            $date_finish=Carbon::now()->lastOfMonth();
        }
        else{
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now=Carbon::now();
            $date_start=$request->date_start;
            $date_finish=$request->date_finish;
        }

        $dropdowns = array();

        $dropdowns['trans status']=Dropdown::where('category','MT Trans Status')
        ->orderBy('name_value','asc')
        ->get();

        $transStatus=$request->trans_status;

        $query=MtTransaction::whereBetween(DB::raw("(STR_TO_DATE(mt_transactions.updated_at,'%Y-%m-%d'))"), [$date_start, $date_finish]);

        if($transStatus != ''){
            $query=$query -> where('mt_transactions.transaction_status',$transStatus);
        }
        else{
            $query=$query -> where('mt_transactions.transaction_status','!=',$transStatus);
        }

        $mtTrans=$query->select(
            'mt_transactions.*',
            'b.firstname',
        )
        ->leftjoin('user_rads as b','mt_transactions.cust_code','=','b.cid')
        ->orderBy('id','desc')
        ->get();

        //dd($mtTrans);

        return view('pg.mt_index',compact(
            'mtTrans',
            'date_start',
            'date_finish',
            'transStatus',
            'dropdowns'
        ));
    }

    public function show($order_id)
    {
        //dd($order_id);

        $mtTransDetails=MtTransactionDetail::select(
            'mt_transaction_details.*',
            'b.firstname',
        )
        ->leftjoin('user_rads as b','mt_transaction_details.cust_code','=','b.cid')
        ->where('order_id',$order_id)
        ->orderBy('id','desc')
        ->get();

        return view('pg.mt_detail',compact(
            'mtTransDetails',
            'order_id'
        ));
    }
}
