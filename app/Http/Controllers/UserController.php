<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use Illuminate\Support\Facades\Hash;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustCreateMail;
use App\Mail\CustResetPassMail;
use App\Mail\notifPassChangeMail;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $query=User::where('user_type','1') //hanya menampilkan user internal
            ->orderBy('users.id','desc');

            if($request->search_method=='any'){
                $search=$query->where('users.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('users.'.$request->search_by, $request->search_value);
            }

            $users=$search->get();

            $dropdowns = array();
            $dropdowns['role']=DB::table('dropdowns')
            ->where('category','=','Role')
            ->orderBy('name_value','asc')
            ->get();

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View List User Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view('User.index',compact('users','dropdowns'));
        }
        else{
            $users=[];
            $dropdowns = array();
            $dropdowns['role']=DB::table('dropdowns')
            ->where('category','=','Role')
            ->orderBy('name_value','asc')
            ->get();
            return view('User.index',compact('users','dropdowns'));
        }  
    }

 
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email|exists:employee,int_emp_email_nap',
            'role' => 'required'
        ]);

        //dd($request);
        $employee=Employee::where('int_emp_email_nap',$request->email)
        ->leftjoin('department','employee.int_emp_department','=','department.department_id')
        ->first();
        //dd($employee);
        
        if($request->pic=='1'){
            $pic='1';
        }
        else{
            $pic='0';
        }
        //dd($pic);
        $users=User::create([
            'id_employee' => $employee->int_emp_id,
            'department' => $employee->department_name,
            'name' => $employee->int_emp_name,
            'prefix_name' => $employee->int_emp_pref_name,
            'email' => $request->email,
            'password' => Hash::make('-'),
            'active_status' => '1',
            'pic' => $pic,
            'role' => $request->role,
            'user_type' => '1', //ini untuk user internal
        ]);

        if($users){
                //Audit Log
                $username= auth()->user()->email; 
                $ipAddress=$_SERVER['REMOTE_ADDR'];
                $location='0';
                $access_from=Browser::browserName();
                $activity='Create User';
                $activity='View List User Menu';

                //dd($location);
                $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
                return redirect('/user')->with('status','Success Create User');
        }
    }

    public function update(Request $request, $id)
    {
        //dd($request);
        $request->validate([
            'role' => 'required'
        ]);

        if($request->pic=='1'){
            $pic='1';
        }
        else{
            $pic='0';
        }

        $update= User::where('id',$id)
        ->update([
            'role' => $request->role,
            'pic' => $pic,
        ]);

        if($update){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Update User';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            return redirect('/user')->with('status','Success Update User');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function revoke($id)
    {
        $revoke= User::where('id',$id)
        ->update([
            'active_status' => '0',
        ]);

        if($revoke){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Revoke User';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/user')->with('status','Success Revoke User');
        }
    }

    public function access($id)
    {
        $access= User::where('id',$id)
        ->update([
            'active_status' => '1',
        ]);

        if($access){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Give User Access';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            return redirect('/user')->with('status','Success Give User Access');
        }
    }

    public function indexCust(Request $request)
    {
        //dd('hai');
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $query=User::where('user_type','2') //hanya menampilkan user customer
            ->orderBy('users.id','desc');

            if($request->search_method=='any'){
                $search=$query->where('users.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('users.'.$request->search_by, $request->search_value);
            }

            $users=$search->get();

            $dropdowns = array();
            $dropdowns['role']=DB::table('dropdowns')
            ->where('category','=','Role')
            ->orderBy('name_value','asc')
            ->get();
            
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View List User Customer Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view('User.indexcust',compact('users','dropdowns'));
        }
        else{
            $users=[];
            $dropdowns = array();
            $dropdowns['role']=DB::table('dropdowns')
            ->where('category','=','Role')
            ->orderBy('name_value','asc')
            ->get();
            return view('User.indexcust',compact('users','dropdowns'));
        }
    }

    public function storeCust(Request $request)
    {
        //return view('Mail.CustCreate');
        $request->validate([
            'cust_code' => 'required',
            'email_cust' => 'required|email'
        ]);

        $rule=Rule::where('rule_name','API Rad Search')->first();
        $url_search_rads=$rule->rule_value;

        $response = Http::post($url_search_rads, [
            'cid' => $request->cust_code,
        ]);
        //dd($response->json());
        $success=$response['success'];

        if($success=='true'){
            $datas=$response['results'];
            foreach ($datas as $data) {
                //simpan ke table users
                $password=Str::random(8);

                $userCust=User::create([
                    'name' => $data['firstname'],
                    'email' => $data['cid'],
                    'email_cust' => $request->email_cust,
                    'password' => Hash::make($password),
                    'active_status' => '1',
                    'pic' => '1',
                    'role' => 'Customer',
                    'user_type' => '2', //ini untuk user Customer
                    'login_counter' => '0',
                ]);

                if($userCust){
                    $rule=Rule::where('rule_name','Customer Portal URL')->first();
                    $url_cust_portal=$rule->rule_value;

                    $details = [
                        'cust_name' => $data['firstname'],
                        'url' => $url_cust_portal,
                        'username' => $data['cid'],
                        'password' => $password
                    ];
                    $recipient=$request->email_cust;

                    Mail::to($recipient)
                    ->send(new CustCreateMail($details));

                    $message='Success Create Customer'; 
                }
            }
        }
        else{
            $message=$response['message'];
        }
        
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Create User Customer';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/user-cust')->with('status',$message);
    }

    public function revokeCust($id)
    {
        $revoke= User::where('id',$id)
        ->update([
            'active_status' => '0',
        ]);

        if($revoke){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Revoke User Customer';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/user-cust')->with('status','Success Revoke User');
        }
    }

    public function accessCust($id)
    {
        $access= User::where('id',$id)
        ->update([
            'active_status' => '1',
        ]);

        if($access){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Give User Access Customer';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            return redirect('/user-cust')->with('status','Success Give User Access');
        }
    }

    public function resetPassCust($id)
    {
        $password=Str::random(8);

        //dd($password);
        $resetPass=User::where('id',$id)
        ->update([
            'password' => Hash::make($password)
        ]);

        $profileCust=User::where('id',$id)->first();

        if($resetPass){
            $rule=Rule::where('rule_name','Customer Portal URL')->first();
            $url_cust_portal=$rule->rule_value;

            $details = [
                'cust_name' => $profileCust->name,
                'url' => $url_cust_portal,
                'username' => $profileCust->email,
                'password' => $password
            ];
            $recipient=$profileCust->email_cust;

            Mail::to($recipient)
            ->send(new CustResetPassMail($details));

            $message='Success Reset Password Customer';

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Submit Reset Password Customer';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            return redirect('/user-cust')->with('status',$message);
        }
    }

    public function resetPassSelf($custCode)
    {
        $custCode=base64_decode($custCode);
        return view('User.resetpass',compact('custCode'));
    }

    public function changePassSelf(Request $request)
    {
        //dd($request->all());
        $password=$request->password_new;
        $username=$request->cust_code;

        //dd($password);
        $resetPass=User::where('email',$username)
        ->update([
            'password' => Hash::make($password)
        ]);

        $profileCust=User::where('email',$username)->first();

        if($resetPass){
            $rule=Rule::where('rule_name','Customer Portal URL')->first();
            $url_cust_portal=$rule->rule_value;

            $details = [
                'cust_name' => $profileCust->name,
                'url' => $url_cust_portal,
                'username' => $profileCust->email,
                'password' => $password
            ];
            $recipient=$profileCust->email_cust;

            Mail::to($recipient)
            ->send(new notifPassChangeMail($details));

            $message='Success Change Password Customer '.$username;
            return redirect('/user-cust/success-change-pass');
        }
    }

    public function successChangePass()
    {
        return view('User.successChangePass');
    }

    public function accessCA($id)
    {
        $accessCA= User::where('id',$id)
        ->update([
            'is_ca' => '1',
        ]);

        if($accessCA){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Give User Access as CA to User ID '.$id;

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/user')->with('status','Success Give Access User As CA');
        }
    }

    public function revokeCA($id)
    {
        $revokeCA= User::where('id',$id)
        ->update([
            'is_ca' => '0',
        ]);

        if($revokeCA){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Revoke Access as CA for User ID '.$id;

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/user')->with('status','Success Revoke Access User As CA');
        }
    }

    public function changeEmailCust(Request $request)
    {
        //dd('hai');
        //dd($request->all());
        $id=$request->user_id;

        //dd($password);    
        $changeEmail=User::where('id',$id)
        ->update([
            'email_cust' => $request->email_cust
        ]);

        if($changeEmail){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Success Change Email Customer';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            return redirect('/user-cust')->with('status',$activity);
        }
    }
}
