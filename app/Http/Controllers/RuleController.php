<?php

namespace App\Http\Controllers;

use App\Models\Rule;
use Illuminate\Http\Request;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class RuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index()
    {
        $rules=Rule::get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Rules App Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Rule.index',compact('rules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rule_name' => 'required',
            'rule_value' => 'required',
        ]);

        $addrule=Rule::create([
            'rule_name' => $request->rule_name,
            'rule_value' => $request->rule_value,
        ]);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Create Rule';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        if ($addrule) {
            return redirect('/rule')->with('status','Success Add Rule');
        }else{
            return redirect('/rule')->with('status','Failed Add Rule');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function show(Rule $rule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function edit(Rule $rule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'rule_name' => 'required',
            'rule_value' => 'required',
        ]);

        $updaterule=Rule::where('id',$id)
        ->update([
            'rule_name' => $request->rule_name,
            'rule_value' => $request->rule_value,
        ]);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Update Rule';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        if ($updaterule) {
            return redirect('/rule')->with('status','Success Update Rule');
        }else{
            return redirect('/rule')->with('status','Failed Update Rule');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleterule=Rule::where('id',$id)
        ->delete();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Delete Rule';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        if ($deleterule) {
            return redirect('/rule')->with('status','Success Delete Rule');
        }else{
            return redirect('/rule')->with('status','Failed Delete Rule');
        }
    }
}
