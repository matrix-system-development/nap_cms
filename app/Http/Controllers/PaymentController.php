<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Billing;
use App\Models\Dropdown;
use App\Models\Rule;
use App\Models\TempCheckPayment;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Traits\AJPTrait;
use Illuminate\Support\Facades\Crypt;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait, AJPTrait;

    public function index()
    {
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Check Payment Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Payment.index');
    }

    public function checkPayment(Request $request)
    {
        $request->validate([
            'inv_number' => 'required|Numeric',
        ]);

        $inv_number=$request->inv_number;

        //call url for api
        $rule=Rule::where('rule_name','API Inquiry Payment')->first();
        $url_payment=$rule->rule_value;

        $response = Http::get($url_payment.$inv_number);
        $conn_status=$response['success'];
    
        if($conn_status=='true'){
            $valid='1';

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Check Payment';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view('Payment.result',compact('valid','inv_number'),['datas' => $response['results']]);
        }else{
            $valid='0';
            $message=$response['message'];

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Check Payment';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view('Payment.result',compact('valid','inv_number','message'));
        }

        
    }

    public function listPayment(Request $request)
    {
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            dd($request->all());
            if($request->pay_period==''){
                $date_start='';
                $date_finish='';
                $query=TempCheckPayment::groupBy('temp_check_payments.id');
            }
            else{
                //dd($request->pay_period);
                $date_start='';
                $date_finish='';
                $query=TempCheckPayment::where(DB::raw("(DATE_FORMAT(temp_check_payments.created_at,'%Y-%m'))"), $request->pay_period)
                    ->groupBy('temp_check_payments.id');    
            }
            $dropdowns = array();
    
            $dropdowns['channel']=TempCheckPayment::select('payment_channel')
            ->groupBy('payment_channel')
            ->orderBy('payment_channel','asc')
            ->get();
    
            $payChannel=$request->pay_channel;
            
            if($payChannel != ''){
                $query=$query -> where('temp_check_payments.payment_channel',$payChannel);
            }
            else{
                $query=$query -> where('temp_check_payments.payment_channel','!=',$payChannel);
            }

            if($request->search_method=='any'){
                if($request->search_by=='CardName'){
                    $searchby=$query->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
                else{
                    $searchby=$query->where('temp_check_payments.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
            }
            else if($request->search_method=='exact'){
                if($request->search_by=='CardName'){
                    $searchby=$query->where('billings.'.$request->search_by, $request->search_value);
                }
                else{
                    $searchby=$query->where('temp_check_payments.'.$request->search_by, $request->search_value);
                }
            }
    
            $q_listpays=$query->select(DB::raw('temp_check_payments.*,billings.CardName,billings.DocDate'))
            ->leftJoin('billings','temp_check_payments.inv_number','billings.DocNum')
            ->get();
    
            //dd($q_listpays);
    
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View List Payment Menu';
    
            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
    
            return view('Payment.list',compact(
                'q_listpays',
                'date_start',
                'date_finish',
                'payChannel',
                'dropdowns'
            ));
        }
        else{
            //dd($request->all());
            if($request->date_start=='' && $request->date_finish==''){
                $now=Carbon::now();
                // $date_start=Carbon::now()->startOfMonth();
                // $date_finish=Carbon::now()->lastOfMonth();
                $date_start='';
                $date_finish='';
            }
            else{
                $request->validate([
                    'date_start' => 'required|date',
                    'date_finish' => 'required|date|after_or_equal:date_start'
                ]);
    
                $now=Carbon::now();
                $date_start=$request->date_start;
                $date_finish=$request->date_finish;
            }
    
            $dropdowns = array();
    
            $dropdowns['channel']=TempCheckPayment::select('payment_channel')
            ->groupBy('payment_channel')
            ->orderBy('payment_channel','asc')
            ->get();
    
            $payChannel=$request->pay_channel;
    
            $query=TempCheckPayment::whereBetween(DB::raw("(STR_TO_DATE(temp_check_payments.created_at,'%Y-%m-%d'))"), [$date_start, $date_finish]);
    
            if($payChannel != ''){
                $query=$query -> where('temp_check_payments.payment_channel',$payChannel);
            }
            else{
                $query=$query -> where('temp_check_payments.payment_channel','!=',$payChannel);
            }
            
            $searchKey = 'payment_list_' . md5($payChannel.$date_start.$date_finish);
            $q_listpays = Cache::remember($searchKey, 180, function () use ($query) {
                return $query->select(DB::raw(
                    'temp_check_payments.*,
                    billings.SOCurrency,
                    billings.CardName,
                    billings.DocDate'
                ))
                ->leftJoin('billings','temp_check_payments.inv_number','billings.DocNum')
                ->groupBy('temp_check_payments.id')
                ->get();
            });
    
            //dd($q_listpays);
    
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View List Payment Menu';
    
            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
    
            return view('Payment.list',compact(
                'q_listpays',
                'date_start',
                'date_finish',
                'payChannel',
                'dropdowns'
            ));
        }
    }

    public function createAutoPayment($reqID){
        $reqID = decrypt($reqID);

        //cek dulu ada yg is_sent_SAP nya 1 atau gak
        $isSentSAP = TempCheckPayment::where('RequestID', $reqID)->where('is_sent_SAP', '1')->exists();
        if($isSentSAP){
            // Code block related to is_sent_SAP being 1
            $isSentSAP = '1';
        }
        else{
            // Code block related to is_sent_SAP not being 1
            $isSentSAP = '0';
        }

        $datas = TempCheckPayment::where('temp_check_payments.RequestID', $reqID)
        ->select(
            'temp_check_payments.*',
            'billings.SOCurrency',
            'billings.CardName',
            'billings.DocDate'
        )
        ->leftJoin('billings','temp_check_payments.inv_number','billings.DocNum')
        ->orderBy('billings.DocNum','desc')
        ->get();
    
        //cari outstanding
        $paymentData    = TempCheckPayment::where('RequestID',$reqID)->orderBy('inv_number','desc')->first(); //cari docnum terakhir supaya dapet oustanding terakhir
        $q_oustanding   = Billing::where('DocNum',$paymentData->inv_number)->first();
        $oustanding     = $q_oustanding->current_outstanding;
        $bankCharge     = $this->bankCharge($oustanding, $paymentData->payment_channel);
        $GLAccount      = $this->glAccount($paymentData->payment_channel);
        $paidAmount     = $oustanding - $bankCharge;

        return view('Payment.create_ap', compact(
            'reqID',
            'isSentSAP',
            'datas',
            'q_oustanding',
            'oustanding',
            'bankCharge',
            'paidAmount',
            'paymentData'
        ));
    }

    public function storeAutoPayment(Request $request){
        $request->validate([
            'request_id' => 'required',
            'transaction_date' => 'required',
            'payment_date' => 'required|before_or_equal:today|after_or_equal:transaction_date',
        ]);
        
        $reqID = $request->request_id;
        $paymentDate = $request->payment_date;

        //cek dulu ada yg is_sent_SAP nya 1 atau gak
        $isSentSAP = TempCheckPayment::where('RequestID', $reqID)->where('is_sent_SAP', '1')->exists();
        if($isSentSAP){
            // Code block related to is_sent_SAP being 1
            // dikembalikan ke modul created AJP supaya validasi tidak berulang
            return redirect('/payment/auto/create/' . encrypt($reqID));
        }
        else{
            // Code block related to is_sent_SAP not being 1
            //1. seleksi data temp check payment
            //get data pembayaran yang akan di push ke SAP berdasarkan Request ID
            $datas = TempCheckPayment::where('RequestID', $reqID)->where('is_sent_SAP', '0')->get();

            //2. lakukan looping untuk di post ke API Payment SAP & update  is_sent_SAP = 1
            foreach ($datas as $data) {
                $id_payment = $data->id;
    
                $settleAJP = $this->settlementAJP($id_payment, $paymentDate);
            }

            // dikembalikan ke modul created AJP supaya validasi tidak berulang
            return redirect('/payment/auto/create/' . encrypt($reqID));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
