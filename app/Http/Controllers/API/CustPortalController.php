<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\CustCreateMail;
use App\Models\Rule;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Browser;
use App\Traits\AuditLogsTrait;

class CustPortalController extends Controller
{
    use AuditLogsTrait;
    public function CustPortalByCustCode($cust_code)
    {
        $countUserPortal=User::where('email',$cust_code)->count();
        //return $countUserPortal;

        if($countUserPortal > 0){

            $userPortal = User::where('email',$cust_code)
            ->get();

            $status_active = $userPortal[0]->active_status;

            //return $status_active;

            if($status_active == '1'){
                $message = 'User Customer Portal for '.$cust_code.' is ACTIVE';
                $response = [
                    'success' => true,
                    'message' => $message,
                    'status' => 'ACTIVE',
                    'data count' => $countUserPortal,
                    'data user' => $userPortal
                ];

                return response()->json($response, 200);
            }
            else{
                $message = 'User Customer Portal for '.$cust_code.' is INACTIVE';
                $response = [
                    'success' => true,
                    'message' => $message,
                    'status' => 'INACTIVE',
                    'data count' => $countUserPortal,
                    'data user' => $userPortal
                ];

                return response()->json($response, 200);
            }
       }
       else{
            $message = 'User Customer Portal for '.$cust_code.' Not Found';
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
       }
    }

    public function AddUser(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'cust_code' => 'required',
            'email_cust' => 'required|email'
        ]);

        //cek cust exist
        $checkCust=User::where('email',$request->cust_code)->count();

        if($checkCust > 0){
            $message="Customer ".$request->cust_code." Already Registered";
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
        }
        else{
            $rule=Rule::where('rule_name','API Rad Search')->first();
            $url_search_rads=$rule->rule_value;

            $response = Http::post($url_search_rads, [
                'cid' => $request->cust_code,
            ]);
            //dd($response->json());
            $success=$response['success'];

            if($success=='true'){
                $datas=$response['results'];
                foreach ($datas as $data) {
                    //simpan ke table users
                    $password=Str::random(8);

                    $userCust=User::create([
                        'name' => $data['firstname'],
                        'email' => $data['cid'],
                        'email_cust' => $request->email_cust,
                        'password' => Hash::make($password),
                        'active_status' => '1',
                        'pic' => '1',
                        'role' => 'Customer',
                        'user_type' => '2', //ini untuk user Customer
                        'login_counter' => '0',
                    ]);

                    if($userCust){
                        $rule=Rule::where('rule_name','Customer Portal URL')->first();
                        $url_cust_portal=$rule->rule_value;

                        // $details = [
                        //     'cust_name' => $data['firstname'],
                        //     'url' => $url_cust_portal,
                        //     'username' => $data['cid'],
                        //     'password' => $password
                        // ];
                        // $recipient=$request->email_cust;

                        // Mail::to($recipient)
                        // ->send(new CustCreateMail($details));

                        //Audit Log
                        $username= 'Sales Dashboard'; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from='API Create User Customer Portal';
                        $activity='Create User Customer';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        $message = 'Success Create Customer '.$request->cust_code;
                        $response = [
                            'success' => true,
                            'message' => $message,
                            'status' => 'ACTIVE',
                            'username' => $request->cust_code,
                            'password' => $password
                        ];

                        return response()->json($response, 200);
                    }
                    else{
                        $message = 'Failed Create Customer '.$request->cust_code;
                        $response = [
                            'success' => false,
                            'message' => $message,
                        ];

                        return response()->json($response, 200);
                    }
                }
            }
            else{
                $message=$response['message'];
                $response = [
                    'success' => false,
                    'message' => $message,
                ];

                return response()->json($response, 200);
            }
        }
    }
}
