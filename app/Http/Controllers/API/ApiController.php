<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Billing;
use App\Models\BillingCorp;
use App\Models\TempCheckPayment;
use App\Models\User;
use App\Models\UserRad;
use Illuminate\Support\Carbon;

class ApiController extends Controller
{
    //FOR FINANCIAL INFO
    public function billingByCustCode($cust_code)
    {
       $countBilligs=Billing::where('CardCode',$cust_code)->count();

       if($countBilligs > 0){

            $listBilligs = Billing::where('CardCode',$cust_code)
            ->get();

            $message = 'Billing For Customer Code '.$cust_code.' Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countBilligs,
                'data billing' => $listBilligs
            ];

            return response()->json($response, 200);
       }
       else{
            $message = 'Billing For Customer Code '.$cust_code.' Not Found';
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
       }
    }

    public function billingCorpByCustCode($cust_code)
    {
       $countBilligs=BillingCorp::where('CardCode',$cust_code)->count();

       if($countBilligs > 0){

            $listBilligs = BillingCorp::where('CardCode',$cust_code)
            ->get();

            $message = 'Billing For Customer Code '.$cust_code.' Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countBilligs,
                'data billing' => $listBilligs
            ];

            return response()->json($response, 200);
       }
       else{
            $message = 'Billing For Customer Code '.$cust_code.' Not Found';
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
       }
    }

    public function paymentByCustCode($cust_code)
    {
        $countPayment=TempCheckPayment::where('cust_code',$cust_code)->count();

        //return $countPayment;

        if($countPayment > 0){

            $listPayment = TempCheckPayment::where('cust_code',$cust_code)
            ->orderBy('id','desc')
            ->get();

            $message = 'Payment For Customer Code '.$cust_code.' Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countPayment,
                'data payment' => $listPayment
            ];

            return response()->json($response, 200);
       }
       else{
            $message = 'Payment For Customer Code '.$cust_code.' Not Found';
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
       }
    }

    public function outstandingByInvDate($inv_period)
    {
        $period = Carbon::createFromFormat('Y-m', $inv_period);

        $query=Billing::where('PaymentStatus','Not Paid')
            ->whereMonth('DocDate',$period)
            ->whereYear('DocDate',$period)
            ->groupBy('DocNum');

        $listBilligs=$query->get();
        $countBilligs=$listBilligs->count();

        if($countBilligs > 0){
            $message = 'Data Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countBilligs,
                'data billing' => $listBilligs
            ];

            return response()->json($response, 200);
        }
        else{
            $message = 'Data Not Found';
            $response = [
                'success' => false,
                'message' => $message,
                'data count' => $countBilligs,
            ];

            return response()->json($response, 202);
        }
    }

    public function outstandingByCustCode($cust_code)
    {
        $query=Billing::where('PaymentStatus','Not Paid')
            ->where('CardCode',$cust_code);

        $listBilligs=$query->get();
        $countBilligs=$query->count();

        if($countBilligs > 0){
            $message = 'Data Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countBilligs,
                'data billing' => $listBilligs
            ];

            return response()->json($response, 200);
        }
        else{
            $message = 'Data Not Found';
            $response = [
                'success' => false,
                'message' => $message,
                'data count' => $countBilligs,
            ];

            return response()->json($response, 202);
        }
    }

    public function outstandingSearch(Request $request)
    {
        if($request->search_method=='any'){
           $query=Billing::where('PaymentStatus','Not Paid')
           ->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%')
           ->groupBy('DocNum');
        }
        else{
            $query=Billing::where('PaymentStatus','Not Paid')
            ->where('billings.'.$request->search_by, $request->search_value)
            ->groupBy('DocNum');
        }

        $listBilligs=$query->get();
        $countBilligs=$listBilligs->count();

        if($countBilligs > 0){
            $message = 'Data Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countBilligs,
                'data billing' => $listBilligs
            ];

            return response()->json($response, 200);
        }
        else{
            $message = 'Data Not Found';
            $response = [
                'success' => false,
                'message' => $message,
                'data count' => $countBilligs,
            ];

            return response()->json($response, 202);
        }
    }

    //FOR RADIUS INFO
    public function radInfoByCustCode($cust_code)
    {
        $countRadUser=UserRad::where('cid',$cust_code)->count();
        //return $countRadUser;

        if($countRadUser > 0){

            $userRadInfo = UserRad::where('cid',$cust_code)
            ->get();

            $message = 'User Radius for '.$cust_code.' Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countRadUser,
                'data radius' => $userRadInfo
            ];

            return response()->json($response, 200);
       }
       else{
            $message = 'User Radius for '.$cust_code.' Not Found';
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 202);
       }
    }
}
