<?php

namespace App\Http\Controllers;

use App\Models\Quarantine;
use App\Models\Rule;
use App\Models\TempUnblockApproval;
use App\Models\TempUnblockApprovalDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class apvTempController extends Controller
{
    public function index()
    {
        // BEGIN CEK NO ACTION
        $cekReqcount=TempUnblockApproval::where('approval_status','1')->count();

        if($cekReqcount >= 1){
            $cekReqExists=TempUnblockApproval::where('approval_status','1')->get();
            
            foreach ($cekReqExists as $data) {
                $id=$data->id;
                //cek ke table quarantine
                $cekQuarantines=Quarantine::where('id_approval',$id)->count();

                //jika di quarantine tidak ada id approval yg dicari (artinya sudah ke block lagi) maka update status REQUEST jadi NO ACTION
                if($cekQuarantines==0){
                    $now=Carbon::now();
                    $response_by="CMS";

                    //update status approval
                    $updateApvStatus=TempUnblockApproval::where('id',$id)
                    ->update([
                        'approval_status' => '4', //NO ACTION
                        'response_by' => $response_by,
                        'response_date' => $now,
                    ]);

                    //insert di table temp unblock approval Detail
                    $temp_unblock_approval_detail=TempUnblockApprovalDetail::create([
                        'id_approval' => $id, 
                        'action' => 'NO ACTION', 
                        'action_by' => $response_by
                    ]);
                }
            }
        }
        // END CEK NO ACTION

        $ApprovalLists=TempUnblockApproval::
        select(
            'temp_unblock_approvals.*',
            'b.firstname',
        )
        ->leftjoin('user_rads as b','temp_unblock_approvals.cust_code','=','b.cid')
        ->orderBy('temp_unblock_approvals.id','desc')
        ->get();
        return view('Customer.index_apv_temp_unblock',compact('ApprovalLists'));
    }

    public function detailLog($id)
    {
        $custInfo=TempUnblockApproval::where('temp_unblock_approvals.id',$id)
        ->select(
            'temp_unblock_approvals.*',
            'b.firstname',
        )
        ->leftjoin('user_rads as b','temp_unblock_approvals.cust_code','=','b.cid')
        ->first();
        
        $detailLogs=TempUnblockApprovalDetail::where('id_approval',$id)
        ->orderBy('id','desc')
        ->get();
        
        return view('Customer.detailLog_approval',compact('custInfo','detailLogs'));
    }

    public function rejectApproval($id,$custCode)
    {
        //dd($id);
        $response_by=auth()->user()->name;
        $now=Carbon::now();

        //update status approval
        $updateApvStatus=TempUnblockApproval::where('id',$id)
        ->update([
            'approval_status' => '3',
            'response_by' => $response_by,
            'response_date' => $now,
        ]);

        //insert di table temp unblock approval Detail
        $temp_unblock_approval_detail=TempUnblockApprovalDetail::create([
            'id_approval' => $id, 
            'action' => 'REJECTED', 
            'action_by' => $response_by
        ]);

        //update is_approval di quarantines karna di reject
        $updateQuarantine=Quarantine::where('cid',$custCode)
        ->update([
            'is_approval' => '0', //menghilangkan flagging is_upproval karna sudah di rejected
        ]);

        return redirect('/apv-temp-unblock')->with('status','Success Rejected Customer Request');
    }

    public function approveApproval($id,$custCode)
    {
        //dd($custCode);
        $response_by=auth()->user()->name;
        $now=Carbon::now();

        //update status approval
        $updateApvStatus=TempUnblockApproval::where('id',$id)
        ->update([
            'approval_status' => '2',
            'response_by' => $response_by,
            'response_date' => $now,
        ]);

        //insert di table temp unblock approval Detail
        $temp_unblock_approval_detail=TempUnblockApprovalDetail::create([
            'id_approval' => $id, 
            'action' => 'APPROVED', 
            'action_by' => $response_by
        ]);

        //Rule Due
        $rule_query=Rule::where('rule_name', 'Temporary Unblock 2')->first();
        $due_hours=$rule_query->rule_value;

        //Rule Max Temp Unblock
        $rule_query=Rule::where('rule_name', 'Maximum Temporary Unblock 2')->first();
        $max_temp=$rule_query->rule_value;

        $exp_unblock=Carbon::parse($now)->addHours($due_hours);

        //update is_approval dan tanggal temp unblock until  di quarantines
        $updateQuarantine=Quarantine::where('cid',$custCode)
        ->update([
            'is_approval' => '0', //menghilangkan flagging is_upproval karna sudah di approved
            'temporary_unblock_exp' => $exp_unblock,
        ]);

        return redirect('/apv-temp-unblock')->with('status','Success Approved Customer Request');
    }
}
