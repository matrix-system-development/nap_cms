<?php

namespace App\Http\Controllers;

use App\Models\QueReminder;
use App\Models\Billing;
use Illuminate\Http\Request;

class QueReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QueReminder  $queReminder
     * @return \Illuminate\Http\Response
     */
    public function show(QueReminder $queReminder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QueReminder  $queReminder
     * @return \Illuminate\Http\Response
     */
    public function edit(QueReminder $queReminder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QueReminder  $queReminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QueReminder $queReminder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QueReminder  $queReminder
     * @return \Illuminate\Http\Response
     */
    public function destroy(QueReminder $queReminder)
    {
        //
    }
}
