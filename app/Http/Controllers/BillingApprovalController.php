<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billing;
use App\Models\BillingApproval;
use Illuminate\Support\Facades\Http;
use App\Models\Rule;
use App\Models\QueUpdateBilling;
use App\Traits\AuditLogsTrait;
use Browser;
use Illuminate\Support\Carbon;

class BillingApprovalController extends Controller
{
    use AuditLogsTrait;
    public function BillingAdjustment(Request $request)
    {
        $type =  $this->auth();
        if($type == null){
            return redirect('/home');
        }
        $username= auth()->user()->email;
        if ($type  == "Dept Head") {
            $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
            ->where('billing_approvals.status', 0)
            ->orderBy('billing_approvals.created_at', 'ASC')
            ->where('billings.LineNum', 1)
            ->get();
    
        } else {
            $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
        ->where('billing_approvals.status', 0)
        ->where('request_by',$username)
        ->orderBy('billing_approvals.created_at', 'ASC')
        ->where('billings.LineNum', 1)
        ->get();

        }
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Adjustment Customer Billing';
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.adjustment',compact('AdjusmentApproval','type'));
    }

    public function  HistoryReq(Request $request)
    {
        $type =  $this->auth();
        $username= auth()->user()->email;
        if ($type  == "Dept Head") {
            $AdjusmentApproval = BillingApproval::leftJoin('billings', 'billing_approvals.docnum', 'billings.DocNum')
            ->whereNotIn('billing_approvals.status', [0])
            ->orderBy('billing_approvals.created_at', 'DESC')
            ->where('billings.LineNum', '1')
            ->get();
    
        } else {
            $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
        ->whereNotIn('billing_approvals.status', [0])
        ->where('request_by',$username)
        ->orderBy('billing_approvals.created_at', 'ASC')
        ->where('billings.LineNum', 1)
        ->get();

        }
       
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View history Request Update Billing';
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.historical',compact('AdjusmentApproval','type'));
    }
   

    public function FindBilling(Request $request)
    {
        $info = Billing::where('docnum',$request->inv_no)
        ->first();
        if ($info == null) {
            return redirect()->route('billing.adjustment')->with('danger', 'Invoice Not Found');
        }

        $current_billings=Billing::where('DocNum',$request->inv_no)
        ->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Find Inv No to Edit';
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.edit_billing',compact('info','current_billings'));
    }

    public function ReqUpdate(Request $request)
    {
        $info = Billing::where('docnum', $request->inv)->first();
        $email = auth()->user()->email;
        $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
        ->orderByDesc('billing_approvals.created_at')
        ->get();
    
        $existingApproval = BillingApproval::where('docnum', $request->inv)
        ->orderByDesc('created_at')
        ->first();
        
            if ($existingApproval != null && $existingApproval->status == 0) {
                return redirect()->route('billing.adjustment')->with('warning', 'A request for this invoice No. number has already been submitted but not approved yet.');
            }
        

        $insert = BillingApproval::insert([
            'docnum' => $request->inv,
            'status' => 0,
            'approval_time' => null,
            'reason_reject' => null,
            'remark_request' => $request->remarks,
            'request_by' => $email,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Request Update Billing';
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);


        if ($insert) {
            return redirect()->route('billing.adjustment')->with('success', 'Your request has been submitted successfully!');
        }
    }

    public function CancelReq($docnum)
    {
        
            // find the billing approval record with the given docnum
            $billingApproval = BillingApproval::where('docnum', $docnum)
            ->orderBy('created_at', 'DESC')
            ->first();

            if (!$billingApproval) {
                return redirect()->route('billing.adjustment')->with('danger', 'Billing approval record not found.');
            }

            $status = $billingApproval->status;
         
            // check if status is not pending
            if ($status != 0) {
                return redirect()->route('billing.adjustment')->with('danger', 'This billing approval request has already been processed and cannot be deleted.');
            }

            // delete the billing approval record
            $deleted = $billingApproval->delete();

            if (!$deleted) {
                return redirect()->route('billing.adjustment')->with('danger', 'Failed to delete billing approval record.');
            }

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Delete Req';
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect()->route('billing.adjustment')->with('success', 'Billing approval record deleted successfully.');

    }
    public function AccReq($docnum)
    {
        $now=Carbon::now()->format('YmdHis');
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Acc Request';
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
      
            $billingApproval = BillingApproval::where('docnum', $docnum)
            ->where('status',0)
            ->first();
            if($billingApproval) {
                $billingApproval->status = 1;
                $billingApproval->approval_time = $now;
                $billingApproval->approval_by = $username;
                $billingApproval->save();

                $existingRow = QueUpdateBilling::where('docnum', $docnum)->first();

                if ($existingRow) {
                    // docnum already exists, do not insert
                    return redirect()->route('billing.adjustment')->with('success', 'Invoice No. already in que update, it will take 10 minutes to update');
                   }

                // docnum does not exist, insert new row
                $inserted = QueUpdateBilling::create([
                    'docnum' => $docnum,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
                // success message
                return redirect()->route('billing.adjustment')->with('success', 'Billing updated approve.');
            } else {
                // error message if the document is not found
                return redirect()->route('billing.adjustment')->with('danger', 'No Invoice not found!');
            }
        
    }

    public function RejReq(Request $request)
    {
        $now=Carbon::now()->format('YmdHis');
         //Audit Log
         $username= auth()->user()->email; 
         $ipAddress=$_SERVER['REMOTE_ADDR'];
         $location='0';
         $access_from=Browser::browserName();
         $activity='Reject Request Adjustment Billing';
         $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        $billingApproval = BillingApproval::where('docnum', $request->inv)
        ->where('status',0)
        ->first();

        if($billingApproval) {
            $billingApproval->status = 2;
            $billingApproval->reason_reject = $request->remarks;
            $billingApproval->approval_time = $now;
            $billingApproval->approval_by = $username;
            $billingApproval->save();
            // success message
            return redirect()->route('billing.adjustment')->with('danger', 'Billing updated Rejected.');
        } else {
            // error message if the document is not found
            redirect()->route('billing.adjustment')->with('danger', 'No Invoice not found!');
        }
    }

    public function HistorySearch(Request $request)
    {
        $type =  $this->auth();
        $email= auth()->user()->email;

         //Audit Log
         $username= auth()->user()->email; 
         $ipAddress=$_SERVER['REMOTE_ADDR'];
         $location='0';
         $access_from=Browser::browserName();
         $activity='Serach History Request Adjustment Billing';
         $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

         
        if ($request->search_by == 'DocNum') {
            if ($type  == "Dept Head") {
                $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
                ->where('billing_approvals.docnum', $request->search_value)
                ->orderBy('billing_approvals.created_at', 'ASC')
                ->where('billings.LineNum', 1)
                ->get();
                    if ($AdjusmentApproval->isEmpty()) {
                        redirect()->route('billing.adjustment')->with('danger', 'No Invoice not found!');
                    }
                return view('Billing.adjustment',compact('AdjusmentApproval','type'));
            } else {
                $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
                ->where('billing_approvals.docnum', $request->search_value)
                ->orderBy('billing_approvals.created_at', 'ASC')
                ->where('billings.LineNum', 1)
                ->where('billing_approvals.request_by',$email)
                ->get();
                    if ($AdjusmentApproval->isEmpty()) {
                        redirect()->route('billing.adjustment')->with('danger', 'No Invoice not found!');
                    }
                return view('Billing.adjustment',compact('AdjusmentApproval','type'));
            }
        }

        if ($request->search_by == 'Status') {
                if ($type  == "Dept Head") {
                    $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
                    ->where('billing_approvals.status', $request->status)
                    ->orderBy('billing_approvals.created_at', 'ASC')
                    ->where('billings.LineNum', 1)
                    ->get();
                    return view('Billing.adjustment',compact('AdjusmentApproval','type'));
                } else {
                    $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
                    ->where('billing_approvals.status', $request->status)
                    ->orderBy('billing_approvals.created_at', 'ASC')
                    ->where('billings.LineNum', 1)
                    ->where('billing_approvals.request_by',$email)
                    ->get();
                    return view('Billing.adjustment',compact('AdjusmentApproval','type'));
                }
            }

        if ($request->search_by == 'Created_at') {
            
            if ($type  == "Dept Head") {
                $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
                ->whereDate('billing_approvals.created_at', date($request->created_at))
                ->orderBy('billing_approvals.created_at', 'ASC')
                ->where('billings.LineNum', 1)
                ->get();
                    if ($AdjusmentApproval->isEmpty()) {
                        redirect()->route('billing.adjustment')->with('danger', 'No request record found!');
                    }
                return view('Billing.adjustment',compact('AdjusmentApproval','type'));
            } else {
                $AdjusmentApproval = BillingApproval::leftJoin('billings','billing_approvals.docnum','billings.DocNum')
                ->whereDate('billing_approvals.created_at', date($request->created_at))
                ->orderBy('billing_approvals.created_at', 'ASC')
                ->where('billings.LineNum', 1)
                ->where('billing_approvals.request_by',$email)
                ->get();
                    if ($AdjusmentApproval->isEmpty()) {
                        redirect()->route('billing.adjustment')->with('danger', 'No request record found!');
                    }
                return view('Billing.adjustment',compact('AdjusmentApproval','type'));
            }
        }

    }
    

    public function Auth(){
        $endpoint_getToken = Rule::where('rule_name','API HRIS Get Token')->pluck('rule_value');
        $endpoint_getStatus = Rule::where('rule_name','API HRIS Get Employee Status')->pluck('rule_value');
        $email = Rule::where('rule_name','username API HRIS')->pluck('rule_value');
        $password = Rule::where('rule_name','password API HRIS')->pluck('rule_value');

        $response = Http::post($endpoint_getToken[0], [
            'email' => $email[0],
            'password' => $password[0]
        ]);
        $jsonResponse = $response->json();
        $token = $jsonResponse['data']['token'];

        $email= auth()->user()->email; 
        
        $status = Http::withToken($token)
        ->get($endpoint_getStatus[0].$email);
        
        $status_employee = $status->json();
        if($status_employee['success'] == false){
            $type = null;
            return $type;
        }
        $type = $status_employee['data']['type'];
        return $type;

    }
}
