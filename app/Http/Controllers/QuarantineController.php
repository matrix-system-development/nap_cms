<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quarantine;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class QuarantineController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){

            $query=Quarantine::select(
                'quarantines.*',
                'user_rads.firstname',
                'billings.Regional', 
                'billings.Sales_Employee'
            )
            ->leftJoin('user_rads','quarantines.cid','user_rads.cid')
            ->leftJoin('billings','quarantines.cid','billings.CardCode')
            ->groupBy('quarantines.cid')
            ->orderBy('quarantines.id','desc');

            if($request->search_method=='any'){
                if($request->search_by == 'cid'){
                    $search=$query->where('quarantines.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
                elseif($request->search_by == 'firstname'){
                    $search=$query->where('user_rads.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
                elseif($request->search_by == 'Sales_Employee'){
                    $search=$query->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
                elseif($request->search_by == 'Regional'){
                    $search=$query->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
            }
            else if($request->search_method=='exact'){
                if($request->search_by == 'cid'){
                    $search=$query->where('quarantines.'.$request->search_by, $request->search_value);
                }
                elseif($request->search_by == 'firstname'){
                    $search=$query->where('user_rads.'.$request->search_by, $request->search_value);
                }
                elseif($request->search_by == 'Sales_Employee'){
                    $search=$query->where('billings.'.$request->search_by, $request->search_value);
                }
                elseif($request->search_by == 'Regional'){
                    $search=$query->where('billings.'.$request->search_by, $request->search_value);
                }
            }

            $quarantines=$search->get();

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View List Quarantine Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view('Customer.index_qua',compact('quarantines'));
        }
        else{
            $quarantines=[];
            return view('Customer.index_qua',compact('quarantines'));
        }
    }
}
