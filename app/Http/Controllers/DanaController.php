<?php

namespace App\Http\Controllers;

use App\Models\DanaOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DanaController extends Controller
{
    public function index(Request $request)
    {
        if($request->date_start=='' && $request->date_finish==''){
            $now=Carbon::now();
            $date_start=Carbon::now()->startOfMonth()->subMonth(6);
            $date_finish=Carbon::now()->lastOfMonth();
        }
        else{
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now=Carbon::now();
            $date_start=$request->date_start;
            $date_finish=$request->date_finish;
        }
        $query=DanaOrder::whereBetween(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), [$date_start, $date_finish]);

        $danaOrders=$query->orderBy('id','desc')->get();
        return view('Dana.index',compact(
            'danaOrders',
            'date_start',
            'date_finish',
        ));
    }
}
