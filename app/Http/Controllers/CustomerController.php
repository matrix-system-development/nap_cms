<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\RequestTempUnblockMail;
use App\Models\UserRad;
use App\Models\Log;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\MassImport;
use App\Models\Quarantine;
use App\Models\Rule;
use App\Models\ApiLog;
use App\Models\Billing;
use App\Models\TempUnblockApproval;
use App\Models\TempUnblockApprovalDetail;
use Illuminate\Support\Carbon;
use App\Traits\AuditLogsTrait;
use Browser;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        //dd($request->all());
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $request->validate([
                'search_by' => 'required',
                'search_method' => 'required',
                'search_value' => 'required',
            ]);

            //call url for api
            $rule=Rule::where('rule_name','API Rad Users')->first();
            $url_user_rads=$rule->rule_value;

            $response = Http::get($url_user_rads);
            //dd($response);
            $datas=$response['results'];
            $conn_status=$response['success'];
            //dd($response['success']);

            if($conn_status=='true'){
                //clear table userrads
                //$deleteUrad=UserRad::truncate();

                //insert into table User_rads
                foreach($datas as $data)
                {
                    //dd($data['cid']);
                    $cek=UserRad::where('cid',$data['cid'])->first();
                    
                    if($cek==''){
                        $userRad=UserRad::create([
                            'cid' => $data['cid'],
                            'username' => $data['username'],
                            'value' => $data['value'],
                            'id' => $data['id'],
                            'groupname' => $data['groupname'],
                            'attribute' => $data['attribute'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'disabled' => $data['disabled'],
                        ]);
                    }
                    else{
                        if($cek->disabled <> $data['disabled']){
                            $userRad=UserRad::where('cid',$data['cid'])
                            ->update([
                                'disabled' => $data['disabled'],
                            ]);
                        }
                    }
                }

                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Success Parse Radius List',
                    'conn_status' => "true",
                ]);
            }
            else{
                //clear table userrads
                //$deleteUrad=UserRad::truncate();
                
                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Failed Parse Radius List',
                    'conn_status' => 'false',
                ]);
            }
            
            //Include Rule Max Temp Unblock for validation in front end
            $rule_query=Rule::where('rule_name', 'Maximum Temporary Unblock')->first();
            $max_temp=$rule_query->rule_value;

            $query=DB::table('user_rads')
            ->select(
                'user_rads.*',
                'quarantines.temporary_unblock_exp',
                'logs.max_temp_unblock',
                'billings_info.CardName',
                'billings_info.last_inv',
                'billings_info.last_service',
            )
            ->leftJoin('quarantines','user_rads.cid','quarantines.cid')
            ->leftJoin( DB::raw( '(SELECT username_log,count(*) AS max_temp_unblock FROM logs
            WHERE message_log = "Temporary Unblock user successfully" 
            AND MONTH(created_at) = MONTH(CURRENT_DATE())
            AND YEAR(created_at) = YEAR(CURRENT_DATE())
            GROUP BY 1)
            logs' ), function( $log_join ) {
            $log_join->on('user_rads.username', '=', 'logs.username_log');
            })
            ->leftJoin( DB::raw( '(SELECT *,max(DocNum)as last_inv,max(Service)as last_service FROM billings where LineNum="1" GROUP BY CardCode)
            billings_info' ), function( $billingInfo_join ) {
            $billingInfo_join->on('billings_info.CardCode', '=','user_rads.cid');
            })
            ->leftJoin( DB::raw( '(SELECT CardName,CardCode,PaymentStatus FROM billings
            WHERE PaymentStatus = "Not Paid" GROUP BY 1)
            billings' ), function( $billing_join ) {
            $billing_join->on('billings.CardCode', '=','user_rads.cid');
            })
            ->groupBy('user_rads.cid');

            if($request->search_method=='any'){
                if($request->search_by=='user_rads.disabled'){
                    if($request->search_value=='active' || $request->search_value=='Active' || $request->search_value=='ACTIVE'){
                        $search=$query->where($request->search_by, 'LIKE', '%0%');
                    }
                    else{
                        $search=$query->where($request->search_by, 'NOT LIKE', '%0%');
                    }
                }
                else{
                    $search=$query->where($request->search_by, 'LIKE', '%'.$request->search_value.'%');
                }
            }
            else if($request->search_method=='exact'){
                if($request->search_by=='user_rads.disabled'){
                    if($request->search_value=='active' || $request->search_value=='Active' || $request->search_value=='ACTIVE'){
                        $search=$query->where($request->search_by, '0');
                    }
                    else{
                        $search=$query->where($request->search_by, '!=', '0');
                    }
                }
                else{
                    $search=$query->where($request->search_by, $request->search_value);
                }
            }

            $UserRads=$search->get();

            //dd($UserRads);
            // $cek=DB::select( '(SELECT username_log,count(*) AS max_temp_unblock FROM logs
            // WHERE message_log = "Temporary Unblock user successfully" AND MONTH(created_at) = MONTH(CURRENT_DATE()) GROUP BY 1)');

            //dd($UserRads);
            //return view('Customer.index',['response'=>$response['results']]);

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Customer Access Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view('Customer.index',compact('UserRads','max_temp'));
        }
        else{
            $UserRads=[];
            $max_temp=[];
            return view('Customer.index',compact('UserRads','max_temp'));
        }
    }

    public function indexMstCust(Request $request)
    {
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $query=Billing::select(
                DB::raw(
                    '
                    *,
                    CardCode,
                    CardName,
                    max(DocNum)as last_inv,
                    max(Service)as last_service,
                    max(Sales_Employee)as am,
                    max(CustAddress)as Address,
                    max(Billing_Pay_Type)as BPT,
                    max(Asuransi)as asuransi,
                    max(PhoneNo)as PhoneNo,
                    max(Email)as Email',
                )
            )
            ->where('LineNum','1')
            ->orderBy('CardCode','asc')
            ->groupBy('CardCode');

            if($request->search_method=='any'){
                $search=$query->where($request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where($request->search_by, $request->search_value);
            }

            $customers=$query->get();
            return view('Customer.index_mst_cust',compact('customers'));
        }
        else{
            $customers=[];
            return view('Customer.index_mst_cust',compact('customers'));
        }
    }

    public function indexBroadband(Request $request)
    {
        //call url for api
        $rule=Rule::where('rule_name','API Rad Users')->first();
        $url_user_rads=$rule->rule_value;

        $response = Http::get($url_user_rads);
        //dd($response);
        $datas=$response['results'];
        $conn_status=$response['success'];
        //dd($response['success']);

        if($conn_status=='true'){
            //clear table userrads
            //$deleteUrad=UserRad::truncate();

            //insert into table User_rads
            foreach($datas as $data)
            {
                //dd($data['cid']);
                $cek=UserRad::where('cid',$data['cid'])->first();
                
                if($cek==''){
                    $userRad=UserRad::create([
                        'cid' => $data['cid'],
                        'username' => $data['username'],
                        'value' => $data['value'],
                        'id' => $data['id'],
                        'groupname' => $data['groupname'],
                        'attribute' => $data['attribute'],
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'disabled' => $data['disabled'],
                    ]);
                }
                else{
                    if($cek->disabled <> $data['disabled']){
                        $userRad=UserRad::where('cid',$data['cid'])
                        ->update([
                            'disabled' => $data['disabled'],
                        ]);
                    }
                }
            }

            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Success Parse Radius List',
                'conn_status' => "true",
            ]);
        }
        else{
            //clear table userrads
            //$deleteUrad=UserRad::truncate();
            
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Failed Parse Radius List',
                'conn_status' => 'false',
            ]);
        }

        //API Master Data
        $ruleAuthMstData = Rule::where('rule_name', 'API Get Token Master Data')->first();
        $url_AuthMstData = $ruleAuthMstData->rule_value;

        $ruleAMNameMstData = Rule::where('rule_name', 'API AM Name Master Data')->first();
        $url_AMNameMstData = $ruleAMNameMstData->rule_value;

        $ruleServicesMstData = Rule::where('rule_name', 'API Service List Master Data')->first();
        $url_ServicesMstData = $ruleServicesMstData->rule_value;

        $ruleStatusMstData = Rule::where('rule_name', 'API Status List Master Data')->first();
        $url_StatusMstData = $ruleStatusMstData->rule_value;

        $ruleEmailMstData = Rule::where('rule_name', 'Username API Master Data')->first();
        $emailMstData = $ruleEmailMstData->rule_value;

        $rulePasswordMstData = Rule::where('rule_name', 'Password API Master Data')->first();
        $passwordMstData = $rulePasswordMstData->rule_value;

        $response = Http::post($url_AuthMstData, [
            'email' => $emailMstData,
            'password' => $passwordMstData,
        ]);

        $data = $response['data'];
        $token = $data['token'];

        $getAMLists = Http::withToken($token)
        ->get($url_AMNameMstData);
        $AMLists = $getAMLists['data'];

        $getServiceLists = Http::withToken($token)
        ->get($url_ServicesMstData);
        $serviceLists = $getServiceLists['data'];

        $getStatusLists = Http::withToken($token)
        ->get($url_StatusMstData);
        $statusLists = $getStatusLists['data'];

        //dd($statusLists);

        //query user rad
        $radUsers=UserRad::get();

        //query quarantine
        $quarantines=Quarantine::get();

        //dd($radUsers);

        if($request->search_by !='' || $request->search_method !='' || $request->search_value !='' || $request->search_value_am !='' || $request->search_value_service !=''){
            //dd($request->all());
            $ruleSearchBB = Rule::where('rule_name', 'API Search Master Data Broadband')->first();
            $url_SearchBB = $ruleSearchBB->rule_value;

            if($request->search_value !=''){
                $search_value = $request->search_value;
                $search_method = $request->search_method;
            }
            else if($request->search_value_am !=''){
                $search_value = $request->search_value_am;
                $search_method = 'exact';
            }
            else if($request->search_value_service !=''){
                $search_value = $request->search_value_service;
                $search_method = 'exact';
            }
            else if($request->search_value_status !=''){
                $search_value = $request->search_value_status;
                $search_method = 'exact';
            }
            

            $getCust = Http::withToken($token)
            ->post($url_SearchBB, [
                'search_by' => $request->search_by,
                'search_method' => $search_method,
                'search_value' => $search_value,
            ]);

            $success = $getCust['success'];

            if($success =='true'){
                //dd($success,$customers);
                $message = $getCust['message'];
                $customers = $getCust['data'];
                return view('Customer.index_mst_bb',compact('quarantines','radUsers','customers','AMLists','serviceLists','statusLists'));
            }
            else{
                $message = $getCust['message'];
                $customers = [];
                return view('Customer.index_mst_bb',compact('quarantines','radUsers','customers','AMLists','serviceLists','statusLists'))->with('status',$message);
            }
        }
        else{
            $customers=[];
            return view('Customer.index_mst_bb',compact('quarantines','radUsers','customers','AMLists','serviceLists','statusLists'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function block(Request $request,$cid)
    {
        $request->validate([
            'remark_block' => 'required',
        ]);

        $action_by=auth()->user()->name;

        // cek table Quarantine dulu
        $cek_quarantine=Quarantine::where('cid',$cid)->count();

        // jika di table quarntine cid nya null maka dia isolate cid karna ketika di block maka cid wajib di Isolate
        if($cek_quarantine =='0'){
            $quarantine=Quarantine::create([
                'cid' => $cid,
                'temporary_unblock_flag' => '0',
                'temporary_unblock_exp' => '',
                'created_by' => $action_by,
                'message' => $request->remark_block,
                'is_approval' => '0' //tidak butuh approval
            ]);
        }
        else{
            $quarantine=Quarantine::where('cid',$cid)
                ->update([
                    'temporary_unblock_flag' => '0',
                    'temporary_unblock_exp' => '',
                    'created_by' => $action_by,
                    'message' => $request->remark_block,
                ]);
        }

        if($quarantine){

            //call url for api
            $rule=Rule::where('rule_name','API Rad Block')->first();
            $url_block_rads=$rule->rule_value;

            $response = Http::post($url_block_rads, [
                'cid' => $cid,
            ]);
            //dd($response);
            //$cid=$response['cid'];
            $username=$response['user'];
            $message=$response['message'];
            $created_by="rad-block";
    
            if($response['success']=="true"){
                $logs=Log::create([
                    'cid_log' => $cid,
                    'username_log' => $username,
                    'message_log' => $message,
                    'action_by_log' => $action_by,
                    'created_by_log' => $created_by,
                    'remarks_log' => $request->remark_block,
                ]);
    
                if($logs){
                    //Audit Log
                    $username= auth()->user()->email; 
                    $ipAddress=$_SERVER['REMOTE_ADDR'];
                    $location='0';
                    $access_from=Browser::browserName();
                    $activity='Block Customer Access';

                    //dd($location);
                    $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                    return redirect('/mon-cust')->with('status','Success Block Customer');
                }
                else{
                    //Audit Log
                    $username= auth()->user()->email; 
                    $ipAddress=$_SERVER['REMOTE_ADDR'];
                    $location='0';
                    $access_from=Browser::browserName();
                    $activity='Block Customer Access';

                    //dd($location);
                    $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                    return redirect('/mon-cust')->with('status','Failed Create Log Success Block');
                }
            }
            else{
                $logs=Log::create([
                    'cid_log' => $cid,
                    'username_log' => $username,
                    'message_log' => $message,
                    'action_by_log' => $action_by,
                    'created_by_log' => $created_by,
                ]);
    
                if($logs){
                    //Audit Log
                    $username= auth()->user()->email; 
                    $ipAddress=$_SERVER['REMOTE_ADDR'];
                    $location='0';
                    $access_from=Browser::browserName();
                    $activity='Block Customer Access';

                    //dd($location);
                    $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                    return redirect('/mon-cust')->with('status','Failed Block Customer');
                }
                else{
                    //Audit Log
                    $username= auth()->user()->email; 
                    $ipAddress=$_SERVER['REMOTE_ADDR'];
                    $location='0';
                    $access_from=Browser::browserName();
                    $activity='Block Customer Access';

                    //dd($location);
                    $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                    return redirect('/mon-cust')->with('status','Failed Create Log Invalid Block');
                }
            }
        }
        else{
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Block Customer Access';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/mon-cust')->with('status','Failed Isolate Customer Code');
        }
    }
    public function unblock(Request $request,$cid)
    {
        //dd($request);
        $request->validate([
            'remark_unblock' => 'required',
        ]);

        $now=Carbon::now();

        //Rule Due
        $rule_query=Rule::where('rule_name', 'Temporary Unblock')->first();
        $due_hours=$rule_query->rule_value;

        //Rule Max Temp Unblock
        $rule_query=Rule::where('rule_name', 'Maximum Temporary Unblock')->first();
        $max_temp=$rule_query->rule_value;

        $action_by=auth()->user()->name;

        $cek_quarantine=Quarantine::where('cid',$cid)->count();

        $max_unblocktemp=log::where('cid_log',$cid)
        ->where('message_log','Temporary Unblock user successfully')
        ->whereMonth('created_at',$now)
        ->whereYear('created_at',$now)
        ->count();

        // dd($max_unblocktemp);
        
        $exp_unblock=Carbon::parse($now)->addHours($due_hours);

        //cek max unblock temp dulu kalau sudah mencapai 2 maka tidak bisa di unblock temp lagi
        if($max_unblocktemp==$max_temp){
            return redirect('/mon-cust')->with('status','User has reached the maximum temporary unblock limit this month');
        }
        else{
            // jika cid di quarantine tidak ada maka di isolate cid nya karna temp unblock
            if($cek_quarantine =='0'){
                $update_quarantine=Quarantine::create([
                    'cid' => $cid,
                    'temporary_unblock_flag' => '1',
                    'temporary_unblock_exp' => $exp_unblock,
                    'created_by' => $action_by,
                    'message' => $request->remark_unblock,
                    'is_approval' => '0' //tidak butuh approval
                ]);
            }
            else{
                $update_quarantine=Quarantine::where('cid',$cid)
                ->update([
                    'temporary_unblock_flag' => '1',
                    'temporary_unblock_exp' => $exp_unblock,
                    'created_by' => $action_by,
                    'message' => $request->remark_unblock,
                    'is_approval' => '0' //tidak butuh approval
                ]);
            }
            
            if($update_quarantine){
                //call url for api
                $rule=Rule::where('rule_name','API Rad Unblock')->first();
                $url_unblock_rads=$rule->rule_value;

                $response = Http::post($url_unblock_rads, [
                    'cid' => $cid,
                ]);
        
        
                $username=$response['user'];
                $message=$response['message'];
                $created_by="rad-unblock";
        
                if($response['success']=="true"){
                    $logs=Log::create([
                        'cid_log' => $cid,
                        'username_log' => $username,
                        'message_log' => "Temporary ".$message,
                        'action_by_log' => $action_by,
                        'created_by_log' => $created_by,
                        'remarks_log' => $request->remark_unblock . " - Temporary Unblock",
                    ]);
        
                    if($logs){
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Success Unblock Customer');
                    }
                    else{
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Failed Create Log Success Unblock');
                    }
                }
                else{
                    $logs=Log::create([
                        'cid_log' => $cid,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' => $action_by,
                        'created_by_log' => $created_by,
                        'remarks_log' => $request->remark_unblock,
                    ]);
        
                    if($logs){
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Failed Unblock Customer');
                    }
                    else{
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Failed Create Log Invalid Unblock');
                    }
                }
            }
            else{
                //Audit Log
                $username= auth()->user()->email; 
                $ipAddress=$_SERVER['REMOTE_ADDR'];
                $location='0';
                $access_from=Browser::browserName();
                $activity='Unblock Customer Access';

                //dd($location);
                $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
                
                return redirect('/mon-cust')->with('status','Failed Isolate Customer Code');
            }
        }
    }

    public function unblock2(Request $request,$cid)
    {
        $request->validate([
            'remark_unblock' => 'required',
        ]);

        $now=Carbon::now();

        //Rule Due
        $rule_query=Rule::where('rule_name', 'Temporary Unblock')->first();
        $due_hours=$rule_query->rule_value;

        //Rule Max Temp Unblock
        $rule_query=Rule::where('rule_name', 'Maximum Temporary Unblock')->first();
        $max_temp=$rule_query->rule_value;

        $action_by=auth()->user()->name;
        $unblockPeriod='7 days';
        //dd($unblockPeriod);

        $cek_quarantine=Quarantine::where('cid',$cid)->count();

        $max_unblocktemp=log::where('cid_log',$cid)
        ->where('message_log','Temporary Unblock user successfully')
        ->whereMonth('created_at',$now)
        ->whereYear('created_at',$now)
        ->count();

        // dd($max_unblocktemp);
        
        $exp_unblock=Carbon::parse($now)->addHours($due_hours);

        //cek max unblock temp dulu kalau sudah mencapai 2 maka tidak bisa di unblock temp lagi
        if($max_unblocktemp==$max_temp){
            return redirect('/mon-cust')->with('status','User has reached the maximum temporary unblock limit this month');
        }
        else{

            //insert di table temp unblock approval
            $temp_unblock_approval=TempUnblockApproval::create([
                'cust_code' => $cid, 
                'tmp_unblock_period' => $unblockPeriod, 
                'approval_status' => '1', //REQUESTED 
                'request_by' => $action_by, 
                'request_date' => $now,
                'response_by' => '', 
                'response_date' => ''
            ]);

            $id_approval=$temp_unblock_approval->id;

            //insert di table temp unblock approval Detail
            $temp_unblock_approval_detail=TempUnblockApprovalDetail::create([
                'id_approval' => $id_approval, 
                'action' => 'REQUESTED', 
                'action_by' => $action_by
            ]);

            // jika cid di quarantine tidak ada maka di isolate cid nya karna temp unblock
            if($cek_quarantine =='0'){
                $update_quarantine=Quarantine::create([
                    'cid' => $cid,
                    'temporary_unblock_flag' => '1',
                    'temporary_unblock_exp' => $exp_unblock,
                    'created_by' => $action_by,
                    'message' => $request->remark_unblock,
                    'is_approval' => '1', //butuh approval
                    'id_approval' => $id_approval,
                ]);
            }
            else{
                $update_quarantine=Quarantine::where('cid',$cid)
                ->update([
                    'temporary_unblock_flag' => '1',
                    'temporary_unblock_exp' => $exp_unblock,
                    'created_by' => $action_by,
                    'message' => $request->remark_unblock,
                    'is_approval' => '1', //butuh approval
                    'id_approval' => $id_approval,
                ]);
            }
            
            if($update_quarantine){
                //call url for api
                $rule=Rule::where('rule_name','API Rad Unblock')->first();
                $url_unblock_rads=$rule->rule_value;

                $response = Http::post($url_unblock_rads, [
                    'cid' => $cid,
                ]);
        
        
                $username=$response['user'];
                $message=$response['message'];
                $created_by="rad-unblock";
        
                if($response['success']=="true"){
                    $logs=Log::create([
                        'cid_log' => $cid,
                        'username_log' => $username,
                        'message_log' => "Temporary ".$message,
                        'action_by_log' => $action_by,
                        'created_by_log' => $created_by,
                        'remarks_log' => $request->remark_unblock . " - Temporary Unblock",
                    ]);
        
                    if($logs){
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        //SENT EMAIl FOR APPROVAL

                        $rule=Rule::where('rule_name','Email Request Temp Unblock 2')->first();
                        $email_reqApproval=$rule->rule_value;

                        //Cust Name
                        $userRad=UserRad::where('cid',$cid)->first();
                        $custName=$userRad->firstname;
                        
                        $details = [
                            'cust_code' => $cid,
                            'cust_name' => $custName,
                            'notes' => $request->remark_unblock,
                        ];

                        $recipient=$email_reqApproval;

                        Mail::mailer('smtp2')
                        ->to($recipient)
                        ->send(new RequestTempUnblockMail($details));

                        return redirect('/mon-cust')->with('status','Success Unblock Customer');
                    }
                    else{
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Failed Create Log Success Unblock');
                    }
                }
                else{
                    $logs=Log::create([
                        'cid_log' => $cid,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' => $action_by,
                        'created_by_log' => $created_by,
                        'remarks_log' => $request->remark_unblock,
                    ]);
        
                    if($logs){
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Failed Unblock Customer');
                    }
                    else{
                        //Audit Log
                        $username= auth()->user()->email; 
                        $ipAddress=$_SERVER['REMOTE_ADDR'];
                        $location='0';
                        $access_from=Browser::browserName();
                        $activity='Unblock Customer Access';

                        //dd($location);
                        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                        return redirect('/mon-cust')->with('status','Failed Create Log Invalid Unblock');
                    }
                }
            }
            else{
                //Audit Log
                $username= auth()->user()->email; 
                $ipAddress=$_SERVER['REMOTE_ADDR'];
                $location='0';
                $access_from=Browser::browserName();
                $activity='Unblock Customer Access';

                //dd($location);
                $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
                
                return redirect('/mon-cust')->with('status','Failed Isolate Customer Code');
            }
        }
    }

    public function massBlock($token)
    {
        $inq_mass_list=MassImport::where('token',$token)
        ->get();
        //dd($inq_mass_list);

        foreach ($inq_mass_list as $list) {
            $cid = $list->cid;

            //call url for api
            $rule=Rule::where('rule_name','API Rad Block')->first();
            $url_block_rads=$rule->rule_value;

            $response = Http::post($url_block_rads, [
                'cid' => $cid,
            ]);

            $username=$response['user'];
            $message=$response['message'];
            $action_by=auth()->user()->name;
            $created_by="mass-rad-block/".$list->token."/".$list->filename;

            if($response['success']=="true"){
                $logs=Log::create([
                    'cid_log' => $cid,
                    'username_log' => $username,
                    'message_log' => $message,
                    'action_by_log' => $action_by,
                    'created_by_log' => $created_by,
                ]);

                if($logs){
                    $del_list=MassImport::where('token',$token)
                    ->where('cid',$cid)
                    ->delete();
                }
            }
            else{
                $logs=Log::create([
                    'cid_log' => $cid,
                    'username_log' => $username,
                    'message_log' => $message,
                    'action_by_log' => $action_by,
                    'created_by_log' => $created_by,
                ]);
            }
        }

        return redirect('/import')->with('status','Process Block Customer Done');
    }

    public function massUnblock($token)
    {
        $inq_mass_list=MassImport::where('token',$token)
        ->get();
        //dd($inq_mass_list);

        foreach ($inq_mass_list as $list) {
            $cid = $list->cid;

            //call url for api
            $rule=Rule::where('rule_name','API Rad Unblock')->first();
            $url_unblock_rads=$rule->rule_value;

            $response = Http::post($url_unblock_rads, [
                'cid' => $cid,
            ]);

            $username=$response['user'];
            $message=$response['message'];
            $action_by=auth()->user()->name;
            $created_by="mass-rad-unblock/".$list->token."/".$list->filename;

            if($response['success']=="true"){
                $logs=Log::create([
                    'cid_log' => $cid,
                    'username_log' => $username,
                    'message_log' => $message,
                    'action_by_log' => $action_by,
                    'created_by_log' => $created_by,
                ]);

                if($logs){
                    $del_list=MassImport::where('token',$token)
                    ->where('cid',$cid)
                    ->delete();
                }
            }
            else{
                $logs=Log::create([
                    'cid_log' => $cid,
                    'username_log' => $username,
                    'message_log' => $message,
                    'action_by_log' => $action_by,
                    'created_by_log' => $created_by,
                ]);
            }
        }

        return redirect('/import')->with('status','Process Unblock Customer Done');
    }

    public function massListDelete($token)
    {
        $del_list=MassImport::where('token',$token)
        ->delete();
        return redirect('/import')->with('status','Process Delete File Done');
    }

    public function importUpload()
    {
        $upload_lists = DB::table('mass_imports')
             ->select(DB::raw('token,filename,created_at,count(*) as row_count'))
             ->where('upload_by', 'User 1')
             ->groupBy('token')
             ->orderBy('created_at','desc')
             ->get();

             //dd($upload_lists);
        return view('MassManagement.import',compact('upload_lists'));
    }

    public function detailNetStatus($cid)
    {
        $userRads=UserRad::where('cid',$cid)->get();
        $quarantines=Quarantine::where('cid',$cid)->get();
        
        return view('Customer.show_net_status',compact('userRads','quarantines'));
    }

    public function detailFinStatus($cid)
    {
        //dd($cid);
        $billings=Billing::where('CardCode',$cid)->groupBy('DocNum')->get();
        //dd($billings);
        return view('Billing.show_fin_status',compact('billings'));
    }
}
