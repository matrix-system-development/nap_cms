<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\ListImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

class ImportController extends Controller
{
    protected $fileName;

    public function importlist(Request $request)
    {
        $request->validate([
            'file_list' => 'required|mimes:csv,txt',
        ]);

        $token = Str::random(64);;
        $filename = $request->file('file_list')->getClientOriginalName();

        $file=$request->file('file_list');
        $import= Excel::import(new ListImport($filename,$token),$file);
             
        if($import){
            return redirect('import')->with('status','Success Upload');
        }
        else{
            return redirect('import')->with('status','Failed Upload');
        }
        
    }
}
