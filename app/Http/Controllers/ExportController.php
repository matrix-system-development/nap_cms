<?php

namespace App\Http\Controllers;

use App\Exports\BillingExport;
use App\Exports\DanaExport;
use Illuminate\Http\Request;
use App\Models\Log;
use App\Traits\AuditLogsTrait;
use App\Exports\LogExport;
use App\Exports\PaymentExport;
use App\Exports\BillingCorpsExport;
use App\Exports\PGExport;
use Browser;
use Stevebauman\Location\Facades\Location;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    use AuditLogsTrait;
    public function exportLog(Request $request)
    {
        //dd($request->all());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Export Monitoring Logs';

        //dd($activity);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return Excel::download(new LogExport($request->date_start,$request->date_finish,$request->action_by),'Monitoring Logs.xlsx');
    }

    public function exportMT_trans(Request $request)
    {
        //dd($request->all());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Export Midtrans Transction Logs';

        //dd($activity);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return Excel::download(new PGExport($request->date_start,$request->date_finish,$request->trans_status),'Midtrans Transaction.xlsx');
    }

    public function exportPayment(Request $request)
    {
        //dd($request->all());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Export Midtrans Transction Logs';

        //dd($activity);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return Excel::download(new PaymentExport($request->date_start,$request->date_finish,$request->pay_channel),'Payment Transaction.xlsx');
    }

    public function exportdana_trans(Request $request)
    {
        //dd($request->all());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Export DANA Transaction Logs';

        //dd($activity);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return Excel::download(new DanaExport($request->date_start,$request->date_finish),'Dana Transaction.xlsx');
    }

    public function exportBilling(Request $request)
    {
        //dd($request->all());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Export Billing Report Logs';

        //dd($activity);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
        //dd($request->inv_period);
        return Excel::download(new BillingExport($request->inv_period),'Billing Report.xlsx');
    }
    public function exportBillingCorps(Request $request)
    {
        //dd($request->all());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Export Billing Report Logs';

        //dd($activity);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
        //dd($request->inv_period);
        return Excel::download(new BillingCorpsExport($request->inv_period),'Billing Report.xlsx');
    }
}
