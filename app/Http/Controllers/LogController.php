<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Log;
use App\Models\ApiLog;
use App\Models\auditLog;
use App\Models\Rule;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;

class LogController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        //dd($request->all());

        if($request->date_start=='' && $request->date_finish==''){
            $now=Carbon::now();
            // $date_start=Carbon::now()->startOfMonth();
            // $date_finish=Carbon::now()->lastOfMonth();
            $date_start=Carbon::now()->subDays(14);
            $date_finish=Carbon::now();
        }
        else{
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now=Carbon::now();
            $date_start=$request->date_start;
            $date_finish=$request->date_finish;
        }

        //dd($date_start."until".$date_finish);

        $dropdowns = array();

        $dropdowns['action by']=Log::where('action_by_log','LIKE','%cron%')
        ->select('action_by_log')
        ->groupBy('action_by_log')
        ->orderBy('action_by_log','asc')
        ->get();

        //dd($dropdowns['action by']);
        $actionBy=$request->action_by;

        $query=Log::whereBetween(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), [$date_start, $date_finish]);
        if($request->action_by <> ''){
            $query=$query -> where('action_by_log',$actionBy);
        }

        $logs=$query->orderBy('id','desc')->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Monitoring Log Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Log.index',compact(
            'logs',
            'dropdowns',
            'date_start',
            'date_finish',
            'actionBy'
        ));
    }

    public function indexSearch(Request $request)
    {
        //dd($request->all());
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $request->validate([
                'search_by' => 'required',
                'search_method' => 'required',
                'search_value' => 'required',
            ]);

            if($request->date_start=='' && $request->date_finish==''){
                $now=Carbon::now();
                // $date_start=Carbon::now()->startOfMonth();
                // $date_finish=Carbon::now()->lastOfMonth();
                $date_start=Carbon::now()->subDays(14);
                $date_finish=Carbon::now();
            }
            else{
                $request->validate([
                    'date_start' => 'required|date',
                    'date_finish' => 'required|date|after_or_equal:date_start'
                ]);
    
                $now=Carbon::now();
                $date_start=$request->date_start;
                $date_finish=$request->date_finish;
            }
    
            //dd($date_start."until".$date_finish);
    
            $dropdowns = array();
    
            $dropdowns['action by']=Log::where('action_by_log','LIKE','%cron%')
            ->select('action_by_log')
            ->groupBy('action_by_log')
            ->orderBy('action_by_log','asc')
            ->get();
    
            //dd($dropdowns['action by']);
            $actionBy=$request->action_by;
    
            $query=Log::orderBy('id','desc');
    
            if($request->search_method=='any'){
                $search=$query->where('logs.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('logs.'.$request->search_by, $request->search_value);
            }
    
            $logs=$search->get();
    
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Monitoring Log Menu';
    
            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
    
            return view('Log.index',compact(
                'logs',
                'dropdowns',
                'date_start',
                'date_finish',
                'actionBy'
            ));
        }
        else{
            if($request->date_start=='' && $request->date_finish==''){
                $now=Carbon::now();
                // $date_start=Carbon::now()->startOfMonth();
                // $date_finish=Carbon::now()->lastOfMonth();
                $date_start=Carbon::now()->subDays(14);
                $date_finish=Carbon::now();
            }
            else{
                $request->validate([
                    'date_start' => 'required|date',
                    'date_finish' => 'required|date|after_or_equal:date_start'
                ]);
    
                $now=Carbon::now();
                $date_start=$request->date_start;
                $date_finish=$request->date_finish;
            }
            
            $dropdowns = array();
    
            $dropdowns['action by']=Log::where('action_by_log','LIKE','%cron%')
            ->select('action_by_log')
            ->groupBy('action_by_log')
            ->orderBy('action_by_log','asc')
            ->get();

            $logs=[];

            return view('Log.index',compact(
                'logs',
                'dropdowns',
                'date_start',
                'date_finish'
            ));
        }
    }

    public function indexApi(Request $request)
    {
        if($request->date_start=='' && $request->date_finish==''){
            $now=Carbon::now();
            $date_start=Carbon::now()->subDays(14);
            $date_finish=Carbon::now();
        }
        else{
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now=Carbon::now();
            $date_start=$request->date_start;
            $date_finish=$request->date_finish;
        }

        $query=ApiLog::whereBetween(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), [$date_start, $date_finish]);

        $logs=$query->orderBy('id','desc')->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View API Log Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Log.index_api',compact('logs','date_start','date_finish'));
    }

    public function indexAudit()
    {
        $now=Carbon::now();
       
        $logs=auditLog::whereMonth('created_at',$now)
        ->whereYear('created_at',$now)
        ->orderBy('id','desc')
        ->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Audit Log Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Log.index_audit',compact('logs'));
    }

    public function indexRemindBilling(Request $request)
    {
        if($request->has('_token')) {
            //dd($request->all());
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $rule_url=Rule::where('rule_name','API Mandrill All Sent Info All')->first();
            $url=$rule_url->rule_value;

            $rule_key=Rule::where('rule_name','Mandrill API Key')->first();
            $api_key=$rule_key->rule_value;

            $label_date_start=$request->date_start;
            $date_start=Carbon::create($request->date_start)->subDays(1)->format('Y-m-d');//dikurangi 1 hari karna H+7
            $date_finish=$request->date_finish;

            $response = Http::post($url,[
                'key' => $api_key,
                'query' => 'Billing Notification',
                'date_from' => $date_start,
                'date_to' => $date_finish,
                'limit' => '100000'
            ]);

            $result=json_decode($response);

            if(isset($result->status)){
                $result=[];
                $status='error';
                return view('Log.index_remind_billing',compact('status','result','label_date_start','date_finish'));
            }
            else{
                $status='success';
                return view('Log.index_remind_billing',compact('status','result','label_date_start','date_finish'));
            }
        } 
        else{
            $rule_url=Rule::where('rule_name','API Mandrill All Sent Info All')->first();
            $url=$rule_url->rule_value;

            $rule_key=Rule::where('rule_name','Mandrill API Key')->first();
            $api_key=$rule_key->rule_value;

            //menampilkan data H-7
            $label_date_start=Carbon::now()->subDays(7)->format('Y-m-d');
            $date_start=Carbon::now()->subDays(8)->format('Y-m-d');
            $date_finish=Carbon::now()->format('Y-m-d');

            //dd($label_date_start,$date_start,$date_finish);

            $response = Http::post($url,[
                'key' => $api_key,
                'query' => 'Billing Notification',
                'date_from' => $date_start,
                'date_to' => $date_finish,
                'limit' => '100000'
            ]);

            $result=json_decode($response);

            if(isset($result->status)){
                $result=[];
                $status='error';
                return view('Log.index_remind_billing',compact('status','result','label_date_start','date_finish'));
            }
            else{
                $status='success';
                return view('Log.index_remind_billing',compact('status','result','label_date_start','date_finish'));
            }
        }
    }

    public function detailRemindBilling($id)
    {
        //dd($id);
        $rule_url=Rule::where('rule_name','API Mandril Sent Info Detail')->first();
        $url=$rule_url->rule_value;

        $rule_key=Rule::where('rule_name','Mandrill API Key')->first();
        $api_key=$rule_key->rule_value;

        $response = Http::get($url,[
            'key' => $api_key,
            'id' => $id,
        ]);
        //dd($tes);
        return view('Log.detail_remind_billing',compact('response'));
    }
}
