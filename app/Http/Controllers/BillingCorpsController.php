<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\Rule;
use App\Models\BillingCorp;
use App\Models\ApiLog;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class BillingCorpsController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        /* dd("hi"); */
        //dd($request);
        if($request->inv_period==""){
            $current_month=Carbon::now();
        }
        else{
            $period=$request->inv_period;
            $current_month = Carbon::createFromFormat('Y-m', $period);
        }

        //call url for api
        $rule=Rule::where('rule_name','API Rad Users')->first();
        $url_user_rads=$rule->rule_value;

        $response = Http::get($url_user_rads);
        //dd($response);
        $datas=$response['results'];
        $conn_status=$response['success'];
        //dd($response['success']);

      /*   if($conn_status=='true'){
            //clear table userrads
            //$deleteUrad=UserRad::truncate();

            //insert into table User_rads
            foreach($datas as $data)
            {
               //dd($data['cid']);
                $cek=UserRad::where('cid',$data['cid'])->first();
				
				if($cek==''){
					$userRad=UserRad::create([
						'cid' => $data['cid'],
						'username' => $data['username'],
						'value' => $data['value'],
						'id' => $data['id'],
						'groupname' => $data['groupname'],
						'attribute' => $data['attribute'],
						'firstname' => $data['firstname'],
						'lastname' => $data['lastname'],
						'disabled' => $data['disabled'],
					]);
				}
				else{
					if($cek->disabled <> $data['disabled']){
						$userRad=UserRad::where('cid',$data['cid'])
						->update([
							'disabled' => $data['disabled'],
						]);
					}
				}
            }

            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Success Parse Radius List',
                'conn_status' => "true",
            ]);
        }
        else{
            //clear table userrads
            //$deleteUrad=UserRad::truncate();
            
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Failed Parse Radius List',
                'conn_status' => 'false',
            ]);
        } */

        $query_billings=BillingCorp::select(DB::raw('*,billing_corps.updated_at as last_update'))
        ->whereMonth('DocDate',$current_month)
        ->whereYear('DocDate',$current_month)
        //->leftJoin('user_rads','billings.CardCode','user_rads.cid')
        //->leftJoin('quarantines','user_rads.cid','quarantines.cid')
        ->groupBy('DocNum')
        ->get();

        //dd($query_billings);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Billing Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.corps',compact('query_billings','current_month'));
    }

    public function indexSearch(Request $request)
    {
        
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $request->validate([
                'search_by' => 'required',
                'search_method' => 'required',
                'search_value' => 'required',
            ]);

            //call url for api
           /*  $rule=Rule::where('rule_name','API Rad Users')->first();
            $url_user_rads=$rule->rule_value;

            $response = Http::get($url_user_rads);
            //dd($response);
            $datas=$response['results'];
            $conn_status=$response['success'];
            //dd($response['success']);

            if($conn_status=='true'){
                //clear table userrads
                //$deleteUrad=UserRad::truncate();

                //insert into table User_rads
                foreach($datas as $data)
                {
                //dd($data['cid']);
                    $cek=UserRad::where('cid',$data['cid'])->first();
                    
                    if($cek==''){
                        $userRad=UserRad::create([
                            'cid' => $data['cid'],
                            'username' => $data['username'],
                            'value' => $data['value'],
                            'id' => $data['id'],
                            'groupname' => $data['groupname'],
                            'attribute' => $data['attribute'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'disabled' => $data['disabled'],
                        ]);
                    }
                    else{
                        if($cek->disabled <> $data['disabled']){
                            $userRad=UserRad::where('cid',$data['cid'])
                            ->update([
                                'disabled' => $data['disabled'],
                            ]);
                        }
                    }
                }

                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Success Parse Radius List',
                    'conn_status' => "true",
                ]);
            }
            else{
                //clear table userrads
                //$deleteUrad=UserRad::truncate();
                
                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Failed Parse Radius List',
                    'conn_status' => 'false',
                ]);
            } */

            $query=BillingCorp::select(DB::raw('*,billing_corps.updated_at as last_update'))
          /*   ->leftJoin('user_rads','billings.CardCode','user_rads.cid')
            ->leftJoin('quarantines','user_rads.cid','quarantines.cid') */
            ->groupBy('DocNum');

            if($request->search_method=='any'){
                $search=$query->where('billing_corps.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('billing_corps.'.$request->search_by, $request->search_value);
            }

            $query_billings=$search->get();
            //dd($query_billings);

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Billing Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            $current_month=Carbon::now();
            return view('Billing.corps',compact('query_billings','current_month'));
        }
        else{
            $query_billings=[];
            $current_month=Carbon::now();
            return view('Billing.corps',compact('query_billings','current_month'));
        }
    }

    public function show($inv)
    {
        $info=BillingCorp::select(DB::raw('*,billing_corps.updated_at as last_update'))
        ->where('DocNum',$inv)
        ->first();

        $current_billings=BillingCorp::where('DocNum',$inv)
        ->get();

        //dd($current_billings);
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Show Customer Billing';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.corps_show',compact('info','current_billings'));
    }
}
