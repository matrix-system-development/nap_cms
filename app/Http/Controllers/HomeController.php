<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\UserRad;
use App\Models\Quarantine;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class HomeController extends Controller
{
    use AuditLogsTrait;
    public function index()
    {
        $qtotalUser=UserRad::groupBy('cid')->get();
        $totalUser=count($qtotalUser);

        $qactiveUser=UserRad::where('disabled','0')->groupBy('cid')->get();
        $activeUser=count($qactiveUser);

        $qtempUnblock=Quarantine::where('temporary_unblock_flag','1')->get();
        $tempUnblock=count($qtempUnblock);

        $qblockUser=UserRad::where('disabled','<>','0')->groupBy('cid')->get();
        $blockUser=count($qblockUser);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Home Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('home',compact('totalUser','activeUser','tempUnblock','blockUser'));
    }

    public function menu()
    {
        return view("Menu.menu1");
    }

    public function checkEmail(Request $request)
    {
        $email = $request->input('email');
        $isExists = Employee::where('int_emp_email_nap',$email)->first();
        if($isExists){
            return response()->json(array("exists" => true));
        }else{
            return response()->json(array("exists" => false));
        }
    }
}
