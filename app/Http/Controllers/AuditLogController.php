<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\auditLog;
use App\Traits\AuditLogsTrait;
use Browser;
use Illuminate\Support\Carbon;
use Stevebauman\Location\Facades\Location;

class AuditLogController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        $now=Carbon::now();
        $audit_logs=auditLog::whereMonth('created_at',$now)
        ->whereYear('created_at',$now)
        ->orderBy('id','desc')
        ->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Audit Logs';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('audit_log.index',compact('audit_logs'));
    }
}
