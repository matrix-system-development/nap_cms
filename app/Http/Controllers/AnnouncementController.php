<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('hai');
        $announcements=Announcement::orderBy('status','desc')
        ->orderBy('id','desc')
        ->get();

        $dropdowns = array();
        $dropdowns['ann_types']=DB::table('dropdowns')
        ->where('category','=','Announce Type')
        ->orderBy('name_value','asc')
        ->get();

        //dd($dropdowns['ann_types']);

        return view('Announcement.index',compact('announcements','dropdowns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'ann_name' => 'required',
            'ann_description'  => 'required',
            'ann_description_en'  => 'required',
            'ann_description_cn'  => 'required',
            'ann_type'  => 'required',
        ]);

        $announcements=Announcement::create([
            'name_announcement' => $request->ann_name,
            'description' => $request->ann_description,
            'description_en' => $request->ann_description_en,
            'description_cn' => $request->ann_description_cn,
            'announce_type' => $request->ann_type,
            'status' => '1',
            'created_by' => auth()->user()->name,
        ]);

        return redirect('/announcement')->with('status','Success Add Announcement');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        $request->validate([
            'ann_name' => 'required',
            'ann_description'  => 'required',
            'ann_description_en'  => 'required',
            'ann_description_cn'  => 'required',
            'ann_type'  => 'required',
        ]);

        $updates=Announcement::where('id',$announcement->id)
        ->update([
            'name_announcement' => $request->ann_name,
            'description' => $request->ann_description,
            'description_en' => $request->ann_description_en,
            'description_cn' => $request->ann_description_cn,
            'announce_type' => $request->ann_type
        ]);

        return redirect('/announcement')->with('status','Success Update Announcement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }

    public function revoke($id)
    {
        //dd($id);
        $announcements=Announcement::where('id',$id)
        ->update([
            'status' => '0',
        ]);
        
        if($announcements){
            return redirect('/announcement')->with('status','Success Revoke Announcement');
        }
        else{
            return redirect('/announcement')->with('status','Failed Revoke Announcement');
        }
    }

    public function active($id)
    {
        //dd('hai');
        $announcements=Announcement::where('id',$id)
        ->update([
            'status' => '1',
        ]);
        
        if($announcements){
            return redirect('/announcement')->with('status','Success Activated Announcement');
        }
        else{
            return redirect('/announcement')->with('status','Failed Activated Announcement');
        }
    }
}
