<?php

namespace App\Http\Controllers;

use App\Mail\ReminderMail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\QueReminder;
use App\Models\Billing;
use App\Models\BillingTakeout;
use App\Models\ApiLog;
use App\Models\Rule;
use App\Models\Quarantine;
use App\Models\Log;
use App\Models\TempCheckPayment;
use App\Models\UserRad;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Mail;

class JobController extends Controller
{
    use AuditLogsTrait;
    public function index()
    {
        $q_jobs=Rule::where('rule_value','service job')->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Jobs Service Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Job.index',compact('q_jobs'));
    }

    public function runJobs($service)
    {
        //dd($service);
        ini_set('max_execution_time', 3000);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Run Services '.$service;

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        //dd($service);
        if($service=='billing-daily'){
            $now=Carbon::now()->format('Y-m-d');
            $month=Carbon::now()->format('Y-m');
            $year=Carbon::now()->format('Y');
            $exec_time=Carbon::now()->format('Y-m-d H:i:s');

            //call url for api
            $rule=Rule::where('rule_name','API Inquiry Billing All')->first();
            $url_billing_all=$rule->rule_value;

            $response = Http::get($url_billing_all.$now);
            $datas=$response['results'];
            $count_inv=$response['row_count'];

            foreach($datas as $data)
            {
                if($data['Advance']=='N'){
                    //cek apakah line sudah ada
                    $cek_billing=Billing::where(DB::raw('CONCAT(DocNum,LineNum)'),$data['DocNum'].$data['LineNum'])->get();
                    $count_cek=count($cek_billing);

                    if($count_cek=='0'){
                        //jika belum ada di insert dan checker 1
                        $query_billing=Billing::create([
                            'CardCode'=> $data['CardCode'],
                            'CardName'=> $data['CardName'],
                            'PayToCode'=> $data['PayToCode'],
                            'CustAddress'=> $data['Cust Address'],
                            'EnvAddress'=> $data['Env Address'],
                            'Attention'=> $data['Attention'],
                            'DocNum'=> $data['DocNum'],
                            'LineNum'=> $data['LineNum'],
                            'DocDate'=> $data['DocDate'],
                            'DocDueDate'=> $data['DocDueDate'],
                            'CustomerOrderNo'=> $data['Cust Order No'],
                            'SalesID'=> $data['SalesID'],
                            'Description'=> $data['Description'],
                            'SOCurrency'=> $data['SO Currency'],
                            'UnitPrice'=> $data['Unit Price'],
                            'TotalPrice'=> $data['Total Price'],
                            'Amount'=> $data['Amount'],
                            'Rebate'=> $data['Rebate'],
                            'PPH23'=> $data['PPH23'],
                            'PPN'=> $data['PPN'],
                            'TotalAmount'=> $data['Total Amount'],
                            'VA'=> $data['VA'],
                            'CompanyCodeVA'=> $data['Company Code VA'],
                            'CustCodeVA'=> $data['Cust Code VA'],
                            'PaymentStatus'=> $data['PaymentStatus'],
                            'CID'=> $data['CID'],
                            'CreatedDateINV'=> $data['CreateDate'],
                            'Outstanding' => $data['Outstanding'],
                            'NewCustomer' => $data['NewCustomer'],
                            'checker'=> '1',
                            'Email' => $data['Email'],
                            'PhoneNo' => $data['PhoneNo'],
                            'Service' => $data['Service'],
                            'Kapasitas' => $data['Kapasitas'],
                            'Kapasitas_UOM' => $data['Kapasitas UOM'],
                            'Asuransi' => $data['Asuransi'],
                            'Billing_Pay_Type' => $data['Billing Pay Type'],
                            'Regional' => $data['Regional'],
                            'Sales_Code' => $data['Sales Code'],
                            'Sales_Employee' => $data['Sales Employee'],
                            'Advance' => $data['Advance'],
                        ]);     
                    }else{
                        //jika ada maka update checker 1
                        $update_checker=Billing::where(DB::raw('CONCAT(DocNum,LineNum)'),$data['DocNum'].$data['LineNum'])
                        ->update([
                            'NewCustomer' => $data['NewCustomer'],
                            'checker' => '1',
                            //'Outstanding' => $data['Outstanding'], //outstanding di update juga karna SAP selalu update outstanding sesuai tgl billing
                            'Email' => $data['Email'],
                            'PhoneNo' => $data['PhoneNo'],
                            'Service' => $data['Service'],
                            'Kapasitas' => $data['Kapasitas'],
                            'Kapasitas_UOM' => $data['Kapasitas UOM'],
                            'Asuransi' => $data['Asuransi'],
                            'Billing_Pay_Type' => $data['Billing Pay Type'],
                            'Regional' => $data['Regional'],
                            'Sales_Code' => $data['Sales Code'],
                            'Sales_Employee' => $data['Sales Employee'],
                            'Advance' => $data['Advance'],
                        ]);
                    }
                }
            }

            //yang checker nya nol dipindahkan ke table billing takeout
            $cek_billing_uncheck=Billing::whereMonth('DocDate',Carbon::now())
            ->whereYear('DocDate',Carbon::now())
            ->where('checker','0')
            ->get();

            foreach ($cek_billing_uncheck as $data_out) {
            //insert ke table billing takeout
                $insert_billingTakeout=BillingTakeout::create([
                    'CardCode'=> $data_out['CardCode'],
                    'CardName'=> $data_out['CardName'],
                    'PayToCode'=> $data_out['PayToCode'],
                    'CustAddress'=> $data_out['CustAddress'],
                    'EnvAddress'=> $data_out['EnvAddress'],
                    'Attention'=> $data_out['Attention'],
                    'DocNum'=> $data_out['DocNum'],
                    'LineNum'=> $data_out['LineNum'],
                    'DocDate'=> $data_out['DocDate'],
                    'DocDueDate'=> $data_out['DocDueDate'],
                    'CustomerOrderNo'=> $data_out['CustomerOrderNo'],
                    'SalesID'=> $data_out['SalesID'],
                    'Description'=> $data_out['Description'],
                    'SOCurrency'=> $data_out['SOCurrency'],
                    'UnitPrice'=> $data_out['UnitPrice'],
                    'TotalPrice'=> $data_out['TotalPrice'],
                    'Amount'=> $data_out['Amount'],
                    'Rebate'=> $data_out['Rebate'],
                    'PPH23'=> $data_out['PPH23'],
                    'PPN'=> $data_out['PPN'],
                    'TotalAmount'=> $data_out['TotalAmount'],
                    'VA'=> $data_out['VA'],
                    'CompanyCodeVA'=> $data_out['CompanyCodeVA'],
                    'CustCodeVA'=> $data_out['CustCodeVA'],
                    'PaymentStatus'=> $data_out['PaymentStatus'],
                    'CID'=> $data_out['CID'],
                    'CreatedDateINV'=> $data_out['CreatedDateINV'],
                    'Outstanding' => $data['Outstanding'],
                    'NewCustomer' => $data['NewCustomer'],
                    'checker'=> $data_out['checker'],
                    'Email' => $data_out['Email'],
                    'PhoneNo' => $data['PhoneNo'],
                    'Service' => $data['Service'],
                    'Kapasitas' => $data['Kapasitas'],
                    'Kapasitas_UOM' => $data['Kapasitas UOM'],
                    'Asuransi' => $data['Asuransi'],
                    'Billing_Pay_Type' => $data['Billing Pay Type'],
                    'Regional' => $data['Regional'],
                    'Sales_Code' => $data['Sales Code'],
                    'Sales_Employee' => $data['Sales Employee'],
                    'Advance' => $data['Advance'],
                ]);
            }

            $delete_uncheck=Billing::whereMonth('DocDate',Carbon::now())
                    ->whereYear('DocDate',Carbon::now())
                    ->where('checker','0')
                    ->delete();

            //update billing checker jd nol
            $update_checker=Billing::whereMonth('DocDate',Carbon::now())
                    ->whereYear('DocDate',Carbon::now())
                    ->update([
                        'checker' => '0'
                    ]);

            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'inq-billing',
                'description' => 'Success Parse '.$count_inv.' Billing Invoices',
                'conn_status' => "true",
            ]);

            $success= $count_inv." rows, success job at ".$exec_time;

            if($api_logs){
                return redirect('/service/job')->with('status','Success Run Billing Daily Service, '.$success);
            }
        }
        elseif($service=='temporary-unblock'){
            //dd('hai');
            $now=Carbon::now();
            $cek_quarantine=Quarantine::where('temporary_unblock_flag','1')
            ->get();
    
            foreach ($cek_quarantine as $data) {
                if($data->temporary_unblock_exp <= $now){
    
                    //cek ke billing
                    $cek_billing=Billing::where('CardCode',$data->cid)
                    ->orderBy('DocNum','desc')
                    ->first();
    
                    if(($cek_billing->PaymentStatus=='Not Paid') && ($now >= $cek_billing->DocDueDate))
                    {
                        //call url for api block
                        $rule=Rule::where('rule_name','API Rad Block')->first();
                        $url_block_rads=$rule->rule_value;
    
                        $response = Http::post($url_block_rads, [
                            'cid' => $data->cid,
                        ]);
                        //dd($response);
                        $cid=$response['cid'];
    
                        if($response['success']=="true"){
                            $username=$response['user'];
                            $message=$response['message'];
                            $created_by="rad-block";

                            $updateQuarantine=Quarantine::where('cid',$data->cid)
                            ->update([
                                'temporary_unblock_flag' => '0',
                                'temporary_unblock_exp' => '',
                                'created_by' => 'tempUnblock:cron',
                                'message' => 'Temporary Unblock Expired',
                                'is_approval' => '0', //tidak butuh approval
                                'id_approval' => '0',
                            ]);
    
                            if($updateQuarantine){
                                $logs=Log::create([
                                    'cid_log' => $data->cid,
                                    'username_log' => $username,
                                    'message_log' => $message,
                                    'action_by_log' => 'tempUnblock:cron',
                                    'created_by_log' => $created_by,
                                    'remarks_log' => 'Temporary Unblock Expired',
                                ]);
                            }
                            else{
                                $logs=Log::create([
                                    'cid_log' => $data->cid,
                                    'username_log' => $username,
                                    'message_log' => $message,
                                    'action_by_log' => 'tempUnblock:cron',
                                    'created_by_log' => $created_by,
                                    'remarks_log' => 'Failed Update Quarantine',
                                ]);
                            }
                        }
                        else{
                            $username=$response['error'];
                            $message=$response['message'];
                            $created_by="rad-block";

                            $logs=Log::create([
                                'cid_log' => $data->cid,
                                'username_log' => $username,
                                'message_log' => $message,
                                'action_by_log' => 'tempUnblock:cron',
                                'created_by_log' => $created_by,
                                'remarks_log' => 'Failed Update Quarantine',
                            ]);
                        }
                    }
                    else{
                        //unblock permanen
                        $rule=Rule::where('rule_name','API Rad Unblock')->first();
                        $url_unblock_rads=$rule->rule_value;
    
                        $response = Http::post($url_unblock_rads, [
                            'cid' => $data->cid,
                        ]);
    
                        if($response['success']=="true"){
                            $username=$response['user'];
                            $message=$response['message'];
                            $created_by="rad-unblock";

                            $logs=Log::create([
                                'cid_log' => $data->cid,
                                'username_log' => $username,
                                'message_log' => $message,
                                'action_by_log' => 'tempUnblock:cron',
                                'created_by_log' => $created_by,
                                'remarks_log' => 'Temporary Unblock Expired',
                            ]);
        
                            $delete_quarantine=Quarantine::where('cid',$data->cid)
                            ->delete();
                        }
                        else{
                            $username=$response['error'];
                            $message=$response['message'];
                            $created_by="rad-unblock";

                            $logs=Log::create([
                                'cid_log' => $data->cid,
                                'username_log' => $username,
                                'message_log' => $message,
                                'action_by_log' =>'tempUnblock:cron',
                                'created_by_log' => $created_by,
                                'remarks_log' => 'Failed Update Log',
                            ]);
                        }
                    }
                }
            }
            return redirect('/service/job')->with('status','Success Run Temporary Unblock Service');
        }
        elseif($service=='check-payment'){
            //dd('hai');
            //cek Billing yang Not Paid
            $query_billing=Billing::where('PaymentStatus','Not Paid')
            ->groupBy('DocNum')
            ->get();        

            foreach ($query_billing as $data) {
                //call url for api
                $rule=Rule::where('rule_name','API Inquiry Payment')->first();
                $url_payment=$rule->rule_value;

                //cek ke api inq-payment
                $response = Http::get($url_payment.$data->DocNum);
                $conn_status=$response['success'];
                
                if($conn_status=="true"){
                    $data_payment=$response['results'];
                    //dd($data_payment);
                    foreach ($data_payment as $payment) {
                        $income_payment=$data->DocNum.$payment['Code'].$payment['Nominal Payment'];
                        //dd($income_payment);
                        $cek_temp=TempCheckPayment::where(DB::raw('CONCAT(inv_number,cust_code,nominal_payment,"0000")'),$income_payment)->get();
                        //dd($cek_temp);
                        $count_cek_temp=count($cek_temp);
                        
                        if($count_cek_temp=='0'){
                            //insert ke table temp
                            $temp_check=TempCheckPayment::create([
                                'inv_number' => $data->DocNum,
                                'status_check' => '0',
                                'cust_code' => $payment['Code'],
                                'nominal_payment' => $payment['Nominal Payment'],
                                'payment_date' => $payment['PaymentDate'],
                                'payment_channel' => 'SAP',
                            ]);
                        }
                    }
                }
            }
            //dd('hai');
            //cek temp yang status payment 0
            $temp_table=TempCheckPayment::where('status_check','0')->get();

            //update table billing
            foreach ($temp_table as $item) {
                $cek_outstanding=Billing::where('DocNum',$item->inv_number)
                ->groupBy('DocNum')
                ->first();

                //$upd_outstanding=$data->Outstanding - $data_payment[->Nominal Payment];
                $update_billing=Billing::where('DocNum',$item->inv_number)
                ->update([
                    'PaymentStatus' => 'Paid',
                    'Outstanding' => $cek_outstanding->Outstanding - $cek_outstanding->Outstanding,
                ]);

                if($update_billing){
                    $update_check_status=TempCheckPayment::where('inv_number',$item->inv_number)
                    ->where('status_check','0')
                    ->update([
                        'status_check' => '1',
                    ]);
                }

                // Cek jka cid di Quarantine ada atau tidak
                $cek_quarantine=Quarantine::where('cid',$item->cust_code)->count();

                if($cek_quarantine>0){  
                    //walaupun cid nya tdk ada di table Quarantine maka fungsi unblock tetap berjalan karna buka temp unblock
                    //call url for api
                    $rule=Rule::where('rule_name','API Rad Unblock')->first();
                    $url_unblock_rads=$rule->rule_value;

                    $response = Http::post($url_unblock_rads, [
                        'cid' => $item->cust_code,
                    ]);
            
                    if($response['success']=="true"){
                        $username=$response['user'];
                        $message=$response['message'];
                        $created_by="inq_payment";

                        $logs=Log::create([
                            'cid_log' => $item->cust_code,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' => 'CheckPayment:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Status Paid',
                        ]);

                        $delete_quarantine=Quarantine::where('cid',$item->cust_code)
                        ->delete();
                    }
                    else{
                        $username=$response['error'];
                        $message=$response['message'];
                        $created_by="inq_payment";

                        $logs=Log::create([
                            'cid_log' => $item->cust_code,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' =>'CheckPayment:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Failed Unblock',
                        ]);
                    }
                }
                else{
                    //call url for api
                    $rule=Rule::where('rule_name','API Rad Unblock')->first();
                    $url_unblock_rads=$rule->rule_value;

                    $response = Http::post($url_unblock_rads, [
                        'cid' => $item->cust_code,
                    ]);
            
                    if($response['success']=="true"){
                        $username=$response['user'];
                        $message=$response['message'];
                        $created_by="inq_payment";

                        $logs=Log::create([
                            'cid_log' => $item->cust_code,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' => 'CheckPayment:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Status Paid',
                        ]);
                    }
                    else{
                        $username=$response['error'];
                        $message=$response['message'];
                        $created_by="inq_payment";

                        $logs=Log::create([
                            'cid_log' => $item->cust_code,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' =>'CheckPayment:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Failed Unblock',
                        ]);
                    }
                }
            }

            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'inq-payment',
                'description' => 'Success Check Payment',
                'conn_status' => "true",
            ]);

            return redirect('/service/job')->with('status','Success Run Check Payment Service');
        }
        elseif($service=='block-customer-daily'){
            //dd('test');
            
            //call url for api
            $rule=Rule::where('rule_name','API Rad Users')->first();
            $url_user_rads=$rule->rule_value;

            $response = Http::get($url_user_rads);
            $datas=$response['results'];
            $conn_status=$response['success'];
            //dd($datas);

            if($conn_status=='true'){
                //dd('hai');
                //Cek berdasarkan Customer ID di Radius
                foreach($datas as $data)
                {
                    //dd($data['cid']);
                    $nowsub=Carbon::now()->subDays(7)->format('Y-m-d');
                    //dd($nowsub);
                    //cek ke table billing ambil yg Customer baru yang docdate nya H+7 dan belum bayar
                    $cek_billing=Billing::where(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), $nowsub)
                    //$cek_billing=Billing::whereBetween(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), [$now_start, $now_end])
                    ->where('NewCustomer','YES')
                    ->where('CardCode',$data['cid'])
                    ->groupBy('CardCode')
                    ->get();
                
                    //dd($cek_billing);

                    foreach ($cek_billing as $billing) {    
                        //cek apakah payment statusnya Not Paid
                        if($billing->PaymentStatus=='Not Paid'){
                            //dd($data['cid']);

                            //jika Cust ID Not Paid maka jalankan API Block
                            //call url for api
                            $rule=Rule::where('rule_name','API Rad Block')->first();
                            $url_block_rads=$rule->rule_value;

                            //dd($data['cid']);
                            $response = Http::post($url_block_rads, [
                                'cid' => $data['cid'],
                            ]);
                            
                            //$cid=$response['cid'];
                            //$cid=$data['cid'];
                            //$username=$response['user'];
                            //$message=$response['message'];
                            //$created_by="rad-block";

                            //dd($cid);
                    
                            if($response['success']=="true"){
								$cid=$data['cid'];
								$username=$response['user'];
								$message=$response['message'];
								$created_by="rad-block";

                                // cek table Quarantine dulu
                                $cek_quarantine=Quarantine::where('cid',$data['cid'])->count();
                                //dd($cek_quarantine);

                                // jika di table quarntine cid nya null maka dia isolate cid karna ketika di block maka cid wajib di Isolate
                                if($cek_quarantine =='0'){
                                    $quarantine=Quarantine::create([
                                        'cid' => $data['cid'],
                                        'temporary_unblock_flag' => '0',
                                        'temporary_unblock_exp' => '',
                                        'created_by' => 'BlockCustNew:cron',
                                        'message' => 'Customer Not Paid',
                                    ]);
                                }
                                else{
                                    //dd($data['cid']);
                                    $quarantine=Quarantine::where('cid',$data['cid'])
                                        ->update([
                                            'temporary_unblock_flag' => '0',
                                            'temporary_unblock_exp' => '',
                                            'created_by' => 'BlockCustNew:cron',
                                            'message' => 'Customer Not Paid',
                                        ]);
                                }

                                if($quarantine){
                                    $logs=Log::create([
                                        'cid_log' => $cid,
                                        'username_log' => $username,
                                        'message_log' => $message,
                                        'action_by_log' => 'BlockCustNew:cron',
                                        'created_by_log' => $created_by,
                                        'remarks_log' => 'Customer Not Paid',
                                    ]);

                                    if($logs){
                                        //insert api_log
                                        $api_logs=ApiLog::create([
                                            'api_name' => 'Service Block Cust',
                                            'description' => 'Success Block Customer',
                                            'conn_status' => "true",
                                        ]);
                                    }
                                }
                            }
                            else{
								$cid=$data['cid'];
                            	$username=$response['error'];
                            	$message=$response['message'];
                            	$created_by="rad-block";

                                $logs=Log::create([
                                    'cid_log' => $cid,
                                    'username_log' => $username,
                                    'message_log' => $message,
                                    'action_by_log' => 'BlockCustNew:cron',
                                    'created_by_log' => $created_by,
                                ]);
                                
                                if($logs){
                                    //insert api_log
                                    $api_logs=ApiLog::create([
                                        'api_name' => 'Service Block Cust',
                                        'description' => 'Success Block Customer',
                                        'conn_status' => "true",
                                    ]);
                                }
                            }
                        }
                        //echo $quarantine['cid'];
                    }
                }
                return redirect('/service/job')->with('status','Success Run Block Daily Service');
            }
            else{
                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'Service Block Cust',
                    'description' => 'Failed Block Customer',
                    'conn_status' => "true",
                ]);
                return redirect('/service/job')->with('status','Success Run Block Daily Service');
            }
        }
        elseif($service=='block-customer'){
            //dd('test');
            //dd($data['cid']);
            $now_start=Carbon::now()->format('Y-m-05');
            $now_end=Carbon::now()->format('Y-m-20');
        
            //cek ke table billing ambil yg DocDate nya tanggal 5-20 di bulan berjalan
            //$cek_billing=Billing::where(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), $now)
            $cek_billing=Billing::whereBetween(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), [$now_start, $now_end])
            ->where('NewCustomer','NO')
            //->where('CardCode','C090326')
            ->where('PaymentStatus','Not Paid')
            ->groupBy('CardCode')
            ->get();
        
            //dd($cek_billing);

            foreach ($cek_billing as $billing) {    
                //call url for api
                $rule=Rule::where('rule_name','API Rad Block')->first();
                $url_block_rads=$rule->rule_value;

                //dd($billing->CardCode);
                $response = Http::post($url_block_rads, [
                    'cid' => $billing->CardCode,
                ]);

                //dd($cid);
        
                if($response['success']=="true"){
					//$cid=$response['cid'];
					//dd($response['user']);
					$cid=$billing->CardCode;
					$username=$response['user'];
					$message=$response['message'];
					$created_by="rad-block";
                    // cek table Quarantine dulu
                    $cek_quarantine=Quarantine::where('cid',$billing->CardCode)->count();
                    //dd($cek_quarantine);

                    // jika di table quarntine cid nya null maka dia isolate cid karna ketika di block maka cid wajib di Isolate
                    if($cek_quarantine =='0'){
                        $quarantine=Quarantine::create([
                            'cid' => $billing->CardCode,
                            'temporary_unblock_flag' => '0',
                            'temporary_unblock_exp' => '',
                            'created_by' => 'BlockCust:cron',
                            'message' => 'Customer Not Paid',
                        ]);
                    }
                    else{
                        //dd($billing->CardCode);
                        $quarantine=Quarantine::where('cid',$billing->CardCode)
                            ->update([
                                'temporary_unblock_flag' => '0',
                                'temporary_unblock_exp' => '',
                                'created_by' => 'BlockCust:cron',
                                'message' => 'Customer Not Paid',
                            ]);
                    }

                    if($quarantine){
						//dd($response['user']);
                        $logs=Log::create([
                            'cid_log' => $cid,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' => 'BlockCust:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Customer Not Paid',
                        ]);

                        if ($logs) {
                            //insert api_log
                            $api_logs=ApiLog::create([
                                'api_name' => 'Service Block Cust',
                                'description' => 'Success Block Customer',
                                'conn_status' => "true",
                            ]);
                        }
                    }
                }
                else{
                    //dd($billing->CardCode);
                    $cid=$billing->CardCode;
                    $username=$response['error'];
                    $message=$response['message'];
                    $created_by="rad-block";
                    
                    $logs=Log::create([
                        'cid_log' => $cid,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' => 'BlockCust:cron',
                        'created_by_log' => $created_by,
                    ]);

                    if ($logs) {
                        //insert api_log
                        $api_logs=ApiLog::create([
                            'api_name' => 'Service Block Cust',
                            'description' => 'Success Block Customer',
                            'conn_status' => "true",
                        ]);
                    }
                }
            }
            return redirect('/service/job')->with('status','Success Run Block Daily Service');
            
        }
        elseif($service=='reminder-billing'){
            $month=Carbon::now()->format('m');
            $year=Carbon::now()->format('Y');

            //delete queue
            $delete_queue=QueReminder::truncate();

            //cari yang unpaid
            $unpaid_bills=Billing::whereYear('DocDate',$year)
            ->whereMonth('DocDate',$month)
            ->where('NewCustomer','NO')
            ->where('PaymentStatus','Not Paid')
            ->groupBy('CardCode')
            //->take(10)
            ->get();

            //dd($unpaid_bills);
            
            foreach ($unpaid_bills as $data) {
                $cust_emails=explode(";",$data->Email);
                //dd($cust_emails);
                foreach ($cust_emails as $email) {
                    $store_quereminder=QueReminder::create([
                        'cust_code' => $data->CardCode,
                        'email_address' => $email
                        //'email_address' => "sysdev.napinfo@gmail.com"
                    ]);
                }
            }

            return redirect('/service/job')->with('status','Success Run Reminder Billing Service');
        }
        elseif($service=='send-queue-reminder-email'){
            $cekrow_que=QueReminder::count();

            if($cekrow_que > 0){
                $queues=QueReminder::get();
            
                foreach ($queues as $queue) {
                    $billing_unpaid=Billing::where('CardCode',$queue->cust_code)
                    ->where('PaymentStatus','Not Paid')
                    ->where('NewCustomer','NO')
                    ->orderBy('DocNum','desc')
                    ->orderBy('LineNum','asc')
                    ->first();

                    if($billing_unpaid){
                        $details = [
                            'cust_name' => $billing_unpaid->CardName,
                            'cust_code' => $billing_unpaid->CardCode,
                            'url' => 'http://127.0.0.1:8000/',
                            'inv_date' => $billing_unpaid->DocDate,
                            'inv_duedate' => $billing_unpaid->DocDueDate,
                            'inv_no' => $billing_unpaid->DocNum,
                            'currency' => $billing_unpaid->SOCurrency,
                            'amount' => $billing_unpaid->Outstanding
                        ];
                        $recipient=$queue->email_address;
            
                        $sent=Mail::to($recipient)
                        ->send(new ReminderMail($details));

                        if(count(Mail::failures()) > 0 ){
                            $message_log='Failed Sent Email Reminder Billing Notification';
                            $remark_log='Failed Sent Email';
                        } 
                        else{
                            $message_log='Success Sent Email Reminder Billing Notification';
                            $remark_log='Success Sent Email';
                        }
                    }

                    //cek username
                    $user_rad=UserRad::where('cid',$queue->cust_code)->first();
                    $count_user_rad=UserRad::where('cid',$queue->cust_code)->count();

                    if($count_user_rad > 0){
                        $username=$user_rad->username;
                    }
                    else{
                        $username='Username Not Found';
                    }

                    //Log
                    $logs=Log::create([
                        'cid_log' => $queue->cust_code,
                        'username_log' => $username,
                        'message_log' => $message_log,
                        'action_by_log' => 'SentEmailReminder',
                        'created_by_log' => 'send-queue-reminder-email',
                        'remarks_log' => $remark_log,
                    ]);

                    //delete queue
                    $delete_queue=QueReminder::where('cust_code',$queue->cust_code)->delete();
                }
                
                $message='Success Sent Queue Reminder';
            
                return redirect('/service/job')->with('status',$message);
            }
            else{
                $message='Queues Already Sent';
            
                return redirect('/service/job')->with('status',$message);
            }
        }
    }
}
