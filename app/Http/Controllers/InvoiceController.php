<?php

namespace App\Http\Controllers;

use App\Imports\ImportDeleteInvoice;
use Illuminate\Http\Request;
use PDF;
use App\Models\Billing;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\Rule;
use App\Models\UserRad;
use App\Models\ApiLog;
use App\Models\QueInvoice;
use App\Models\BillingCorp;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use DateTime;
use App\Models\TempCheckPayment;
use Maatwebsite\Excel\Facades\Excel;
use Stevebauman\Location\Facades\Location;
use App\Imports\ImporRemarks;

class InvoiceController extends Controller
{
   
    use AuditLogsTrait;
    public function index(Request $request)
    {
        //dd($request);
        if($request->inv_period==""){
            $current_month=Carbon::now();
        }
        else{
            $period=$request->inv_period;
            $current_month = Carbon::createFromFormat('Y-m', $period);
        }

        //call url for api
        $rule=Rule::where('rule_name','API Rad Users')->first();
        $url_user_rads=$rule->rule_value;

        $response = Http::get($url_user_rads);
        //dd($response);
        $datas=$response['results'];
        $conn_status=$response['success'];
        //dd($response['success']);

        if($conn_status=='true'){
            //clear table userrads
            //$deleteUrad=UserRad::truncate();

            //insert into table User_rads
            foreach($datas as $data)
            {
               //dd($data['cid']);
                $cek=UserRad::where('cid',$data['cid'])->first();
				
				if($cek==''){
					$userRad=UserRad::create([
						'cid' => $data['cid'],
						'username' => $data['username'],
						'value' => $data['value'],
						'id' => $data['id'],
						'groupname' => $data['groupname'],
						'attribute' => $data['attribute'],
						'firstname' => $data['firstname'],
						'lastname' => $data['lastname'],
						'disabled' => $data['disabled'],
					]);
				}
				else{
					if($cek->disabled <> $data['disabled']){
						$userRad=UserRad::where('cid',$data['cid'])
						->update([
							'disabled' => $data['disabled'],
						]);
					}
				}
            }

            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Success Parse Radius List',
                'conn_status' => "true",
            ]);
        }
        else{
            //clear table userrads
            //$deleteUrad=UserRad::truncate();
            
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Failed Parse Radius List',
                'conn_status' => 'false',
            ]);
        }

        $query_billings=[];

        //dd($query_billings);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Billing Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Invoice.index',compact('query_billings','current_month'));
    }

    public function indexSearch(Request $request)
    { /* dd($request); */
        /* dd($request->search_by !='' and $request->search_method !='' and $request->inv_period !='' and $request->search_value !='');
        */ //dd('hai');
        if($request->search_by !='' and $request->search_method !='' and $request->inv_period !='' and $request->search_value !=''){
            /* dd('hi'); */
            $request->validate([
                'search_by' => 'required',
                'search_method' => 'required',
                'inv_period' => 'required',
                'search_value' => 'required',
            ]);

            //call url for api
            $rule=Rule::where('rule_name','API Rad Users')->first();
            $url_user_rads=$rule->rule_value;

            $response = Http::get($url_user_rads);
            //dd($response);
            $datas=$response['results'];
            $conn_status=$response['success'];
            //dd($response['success']);

            if($conn_status=='true'){
                //clear table userrads
                //$deleteUrad=UserRad::truncate();

                //insert into table User_rads
                foreach($datas as $data)
                {
                //dd($data['cid']);
                    $cek=UserRad::where('cid',$data['cid'])->first();
                    
                    if($cek==''){
                        $userRad=UserRad::create([
                            'cid' => $data['cid'],
                            'username' => $data['username'],
                            'value' => $data['value'],
                            'id' => $data['id'],
                            'groupname' => $data['groupname'],
                            'attribute' => $data['attribute'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'disabled' => $data['disabled'],
                        ]);
                    }
                    else{
                        if($cek->disabled <> $data['disabled']){
                            $userRad=UserRad::where('cid',$data['cid'])
                            ->update([
                                'disabled' => $data['disabled'],
                            ]);
                        }
                    }
                }

                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Success Parse Radius List',
                    'conn_status' => "true",
                ]);
            }
            else{
                //clear table userrads
                //$deleteUrad=UserRad::truncate();
                
                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Failed Parse Radius List',
                    'conn_status' => 'false',
                ]);
            }

            $query=Billing::select(DB::raw('*,billings.updated_at as last_update'))
            ->leftJoin('user_rads','billings.CardCode','user_rads.cid')
            ->leftJoin('quarantines','user_rads.cid','quarantines.cid')
            ->groupBy('DocNum');

            $period=$request->inv_period;
            $current_month = Carbon::createFromFormat('Y-m', $period);


            if($request->search_method=='any'){
                $search=$query->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%')
                ->whereMonth('DocDate',$current_month)
                ->whereYear('DocDate',$current_month)
                ->orderBy('DocNum', 'DESC');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('billings.'.$request->search_by, $request->search_value)
                ->whereMonth('DocDate',$current_month)
                ->whereYear('DocDate',$current_month)
                ->orderBy('DocNum', 'DESC');
            }

            $query_billings=$search->get();
            //dd($query_billings);

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Billing Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            $current_month=Carbon::now();
            return view('Invoice.index',compact('query_billings','current_month'));
        }
        else if($request->search_by !='' and $request->search_method !='' and $request->search_value !=''){
           /*  dd('test'); */
            $request->validate([
                'search_by' => 'required',
                'search_method' => 'required',
                'search_value' => 'required',
            ]);

            //call url for api
            $rule=Rule::where('rule_name','API Rad Users')->first();
            $url_user_rads=$rule->rule_value;

            $response = Http::get($url_user_rads);
            //dd($response);
            $datas=$response['results'];
            $conn_status=$response['success'];
            //dd($response['success']);

            if($conn_status=='true'){
                //clear table userrads
                //$deleteUrad=UserRad::truncate();

                //insert into table User_rads
                foreach($datas as $data)
                {
                //dd($data['cid']);
                    $cek=UserRad::where('cid',$data['cid'])->first();
                    
                    if($cek==''){
                        $userRad=UserRad::create([
                            'cid' => $data['cid'],
                            'username' => $data['username'],
                            'value' => $data['value'],
                            'id' => $data['id'],
                            'groupname' => $data['groupname'],
                            'attribute' => $data['attribute'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'disabled' => $data['disabled'],
                        ]);
                    }
                    else{
                        if($cek->disabled <> $data['disabled']){
                            $userRad=UserRad::where('cid',$data['cid'])
                            ->update([
                                'disabled' => $data['disabled'],
                            ]);
                        }
                    }
                }

                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Success Parse Radius List',
                    'conn_status' => "true",
                ]);
            }
            else{
                //clear table userrads
                //$deleteUrad=UserRad::truncate();
                
                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Failed Parse Radius List',
                    'conn_status' => 'false',
                ]);
            }

            $query=Billing::select(DB::raw('*,billings.updated_at as last_update'))
            ->leftJoin('user_rads','billings.CardCode','user_rads.cid')
            ->leftJoin('quarantines','user_rads.cid','quarantines.cid')
            ->orderBy('DocNum', 'DESC')
            ->groupBy('DocNum');

            if($request->search_method=='any'){
                $search=$query->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('billings.'.$request->search_by, $request->search_value);
            }

            $query_billings=$search->get();
            /* dd($query_billings); */

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Billing Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            $current_month=Carbon::now();
            return view('Invoice.index',compact('query_billings','current_month'));
        }
        else if($request->inv_period !=''){
        $period=$request->inv_period;
        $current_month = Carbon::createFromFormat('Y-m', $period);
        $query_billings=Billing::select(DB::raw('*,billings.updated_at as last_update'))
        ->whereMonth('DocDate',$current_month)
        ->whereYear('DocDate',$current_month)
        ->leftJoin('user_rads','billings.CardCode','user_rads.cid')
        ->leftJoin('quarantines','user_rads.cid','quarantines.cid')
        ->groupBy('DocNum')
        ->get();
        $current_month=Carbon::now();
        return view('Invoice.index',compact('query_billings','current_month'));
         }
        else{
            $query_billings=[];
            $current_month=Carbon::now();
            return view('Invoice.index',compact('query_billings','current_month'));
        }
    }

    public function downloadPdf($downloadPdf)
    {
        //Header Start
        $pdf_header = Billing::where('docnum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->first();
        //check new customer
        $date_check = date('d-M-Y', strtotime($pdf_header->DocDate));
        $due_date_check = date( "Y-m", strtotime( "$date_check -1 month" ) );
        $check = Billing::where('CardCode',$pdf_header->CardCode)
        ->where(DB::raw("date_format(DocDate,'%Y-%m')"),$due_date_check)
        ->count();
        
        //end check new customer
        if($check > 0){
            $billing_newCustomer = "NO";  
        }
        else {
             $billing_newCustomer = "YES";  
        }
        /* dd($billing_newCustomer); */
        if ($billing_newCustomer == "YES") {
            $date = date('d-M-Y', strtotime($pdf_header->DocDate));
            $due_date = date( "d-M-Y", strtotime( "$date +7 day" ) );
        } else {
            $date = new DateTime(date('d-m-Y', strtotime($pdf_header->DocDate)));
            $date = $date->setDate($date->format('Y'), $date->format('m'),25 );
            $due_date = $date->format('d-M-Y');
        }
        //Header End

        //BODY Start
        $date = date('d-m-Y', strtotime($pdf_header->DocDate));
        $previous_month = date( "Y-m", strtotime( "$date -1 month" ) );
        $previous_bill = Billing::where('CardCode',$pdf_header->CardCode)
        ->where(DB::raw("(DATE_FORMAT(DocDate,'%Y-%m'))"),'<=',$previous_month)
        ->orderBy('DocNum','desc')
        ->first();
        if ($previous_bill) {
            $previous_bill_detail = $previous_bill->TotalAmount;
            $count_char_previous_bill =  strrpos($previous_bill->Description,"(");
            $select_date_previous_bill = substr($previous_bill->Description,$count_char_previous_bill,12);
            $deleteChar_previous_bill = substr($select_date_previous_bill,-11);
            $date_desc_previous_bill = str_replace(".","-",$deleteChar_previous_bill);
            if ($previous_bill->PaymentStatus == 'Paid') {
                $Previous_totalAmount =  $previous_bill->TotalAmount;
                $date_previous_bill = TempCheckPayment::where('inv_number', $previous_bill->DocNum)->first();
                $payment = $date_previous_bill->nominal_payment;
                $date_previous_bill = date('d-M-Y', strtotime($date_previous_bill->payment_date));
                $Debt =  floor($Previous_totalAmount - $payment);
                if($Debt < 0){
                    $Debt = 0;
                }
               
            } else {
                $payment = 0;
                $date_previous_bill = null;
                $Debt =  $previous_bill->TotalAmount;
            }
        } else {
            $date_desc_previous_bill = null;
            $payment = 0;
            $date_previous_bill = null;
            $Debt = 0;
            $previous_bill_detail = 0;
        }
        
        $descripton = Billing::where('docnum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->get();
        $count =  $descripton->count();
        $count = 4 - $count;
        if ($count < 0) {
            $count = 0;
        }
        
        $address =  nl2br(str_replace("$pdf_header->CardName","",$pdf_header->EnvAddress));
        $address =  substr($address, 6);
        $count_name = strpos($address,"<br />");
        $count_all = strlen($address);
        $angka = $count_all - $count_name;
        $fix1 = substr($address,$count_name);
        $address = substr($fix1,6);
       /*  dd($address);
        dd(str_replace("$address","",$pdf_header->EnvAddress)); */
        //BODY END
        
        //Footer Start
        $pdf_footer = Billing::where('docnum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->first();
        //Footer End

        $banner=Rule::where('rule_name','Inovice Banner')->first();
  
        $pdf = PDF::loadview('Invoice.invoice_pdfNew',compact('pdf_header',
        'due_date',
        'date_desc_previous_bill',
        'previous_bill_detail',
        'date_previous_bill',
        'payment',
        'Debt',
        'descripton',
        'pdf_footer',
        'count',
        'address',
        'banner'));
        return $pdf->download($pdf_header->CardName.'_'.$pdf_header->DocNum.'.pdf');
    }

    public function downloadPdfCorp($downloadPdf,$DocDate)
    {
        
        $CardCode = BillingCorp::where('DocNum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->first();
        $DocDate = date('Y-m', strtotime($DocDate));
        $TotalAmount_all = BillingCorp::where('CardCode',$CardCode->CardCode)
        ->where(DB::raw("date_format(DocDate,'%Y-%m')"),$DocDate)
        ->orderBy("Docnum","asc")
        ->groupBy('Docnum')
        ->pluck('TotalAmount');
        $TotalAmount_all = $TotalAmount_all->sum();
        //Header Start
        $pdf_header = BillingCorp::where('docnum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->first();
        $due_date = date('d-M-Y', strtotime($pdf_header->DocDueDate));
        //Header End

        //BODY Start
        $date = date('d-m-Y', strtotime($pdf_header->DocDate));
        $previous_month = date( "Y-m", strtotime( "$date -1 month" ) );
        $previous_bill = BillingCorp::where('CardCode',$pdf_header->CardCode)
        ->where(DB::raw("(DATE_FORMAT(DocDate,'%Y-%m'))"),'<=',$previous_month)
        ->orderBy('DocNum','desc')
        ->first();
        if ($previous_bill) {
            $previous_bill_detail = BillingCorp::where('CardCode',$pdf_header->CardCode)
            ->where(DB::raw("date_format(DocDate,'%Y-%m')"),date('Y-m', strtotime($previous_bill->DocDate)))
            ->orderBy("Docnum","asc")
            ->groupBy('Docnum')
            ->pluck('TotalAmount');
            $previous_bill_detail = $previous_bill_detail->sum();
            $get_Docnum = BillingCorp::where('CardCode',$pdf_header->CardCode)
            ->where(DB::raw("date_format(DocDate,'%Y-%m')"),$previous_month)
            ->orderBy("Docnum","asc")
            ->groupBy('Docnum')
            ->get();
            if($get_Docnum->count() == 0){
                $get_Docnum = BillingCorp::where('CardCode',$pdf_header->CardCode)
                ->where(DB::raw("date_format(DocDate,'%Y-%m')"),'<=',$previous_month)
                ->orderBy("Docnum","desc")
                ->groupBy('Docnum')
                ->first();

                $get_Docnum = BillingCorp::where('CardCode',$pdf_header->CardCode)
                ->where(DB::raw("date_format(DocDate,'%Y-%m')"),date('Y-m', strtotime($get_Docnum->DocDate)))
                ->orderBy("Docnum","asc")
                ->groupBy('Docnum')
                ->get();
            }
            $count_char_previous_bill =  strrpos($previous_bill->Description,"(");
            $select_date_previous_bill = substr($previous_bill->Description,$count_char_previous_bill,12);
            $deleteChar_previous_bill = substr($select_date_previous_bill,-11);
            $date_desc_previous_bill = str_replace(".","-",$deleteChar_previous_bill);
            $inv_number=$previous_bill->DocNum;
                //call url for api
                $rule=Rule::where('rule_name','API Inquiry Payment')->first();
                $url_payment=$rule->rule_value;
                
                $sum = 0;
                foreach ($get_Docnum as $item) {
                        $response = Http::get($url_payment.$item->DocNum);
                        $response_message = json_decode(json_encode($response->json()));
                        if($response_message->success == false){
                            $nominal_payment = 0;
                            $date_previous_bill = null;
                            
                        }
                        else{
                            $date_previous_bill = $response['results'][0];
                            $nominal_payment = $response['results'][0]['Nominal Payment'];
                           
                        }
                        $sum += $nominal_payment ;     
                    }
            if ($date_previous_bill) {
                $Previous_totalAmount =  $previous_bill->TotalAmount;
                $payment = $sum;
                $date_previous_bill = date('d-M-Y', strtotime($date_previous_bill['PaymentDate']));      
            } else {
                $payment = 0;
                $date_previous_bill = null;   
            }
        } else {
            $date_desc_previous_bill = null;
            $payment = 0;
            $date_previous_bill = null;
            $previous_bill_detail = 0;
        }
        $descripton = BillingCorp::where('docnum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->get();

        $count =  $descripton->count();
        $count_check =  $descripton->count();
        $count_check = 24-$count_check;
        /* dd($count_check); */
        /* $count = 5; */
        if ($count > 4) {
            $next = 1;
            $count = 0;
        } else {
            $count = 4 - $count;
            $next = null;
        if ($count < 0) {
            $count = 0;
        }
        }
        $materai = BillingCorp::where('docnum',$downloadPdf)
        ->where('Description','like','%'.'materai'.'%')
        ->first();
        if ($materai) {
            $materai_value = null;
            $count = 2;
        }
        else{
            $materai_value = null;
        }
        $address =  nl2br(str_replace("$pdf_header->CardName","",$pdf_header->EnvAddress));
        $address =  substr($address, 6);
        $count_name = strpos($address,"<br />");
        $count_all = strlen($address);
        $angka = $count_all - $count_name;
        $fix1 = substr($address,$count_name);
        $address = substr($fix1,6);
        //BODY END
        
        //Footer Start
        $pdf_footer = BillingCorp::where('docnum',$downloadPdf)
        ->orderBy("LineNum","asc")
        ->first();
        //Footer End

        //call url for api
        $rule_urlSO=Rule::where('rule_name','API Inquiry SO All')->first();
        $url_InqSO=$rule_urlSO->rule_value;
        $NoPO = Http::get($url_InqSO.$pdf_header->CardCode);
        $NoPO = $NoPO->json();
        if ($NoPO == null) {
            $NoPO == null;
        } else {
            $NoPO = last($NoPO['results']);
        }

     
        $data = $descripton->first();
        if (str_contains($data->Description, 'Period')) {
        $count_char =  strrpos($data->Description,"(");
        $select_date = substr($data->Description,$count_char,12);
        $deleteChar = substr($select_date,-11);
        $date_desc_serve = str_replace(".","-",$deleteChar);
        } else {
            $date_desc_serve = null;
        }
       /*  dd($count); */
        $pdf = PDF::loadview('Invoice.invoice_pdfNew_corps',compact('pdf_header',
        'due_date',
        'date_desc_previous_bill',
        'previous_bill_detail',
        'date_previous_bill',
        'payment',
        'descripton',
        'pdf_footer',
        'count',
        'address',
        'next',
        'materai',
        'materai_value',
        'NoPO',
        'count_check',
        'date_desc_serve',
        'TotalAmount_all'));
        return $pdf->download($pdf_header->CardName.'_'.$pdf_header->DocNum.'.pdf');
    }

    public function import_remark(){
        
        $data =  Excel::toArray(new ImporRemarks,request()->file('doc1'));
        $data = $data[0];
        foreach ($data as $item){
        $updateOrder = BillingCorp::where('DocNum',$item['docnum'])
            ->update([
                'Remarks' => $item['remarks'],
                'CustomerRef' => $item['customerref']
            ]);
        }

        return back()->with('status','Upload Success');
    }

    public function sentPdf($DocNum,$Email)
    {
        //dd($DocNum,$Email);

        //Insert ke que Invoice
        $storeQueInv=QueInvoice::insertOrIgnore([
            'inv_no'=> $DocNum,
            'email_address' => $Email,
        ]);
        //Insert ke que Invoice 

        return redirect('/invoice')->with('status','Success Sent Invoice '.$DocNum.' to '.$Email);
    }

    public function import_delete()
    {
        //dd('hai');
        $data =  Excel::toArray(new ImportDeleteInvoice,request()->file('doc1'));
        $data = $data[0];
        $cekPaid=Billing::where('LineNum','1')->where('PaymentStatus','Paid')->whereIn('DocNum', $data)->count();
        $cekNotPaid=Billing::where('LineNum','1')->where('PaymentStatus','Not Paid')->whereIn('DocNum', $data)->count();
        //dd($cek);
        foreach ($data as $item){
            //cek status paid
            $cekPay=Billing::where('DocNum',$item['docnum'])->first();

            if($cekPay->PaymentStatus == 'Not Paid'){
                $deleteInvoice = Billing::where('DocNum',$item['docnum'])->delete();
            }
        }

        $msg="Success Upload: ".$cekPaid." Paid & ".$cekNotPaid." Not Paid";

        return back()->with('status',$msg);
    }
}
