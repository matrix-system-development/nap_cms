<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billing;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\Rule;
use App\Models\UserRad;
use App\Models\ApiLog;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Illuminate\Support\Facades\Cache;
use Stevebauman\Location\Facades\Location;

class BillingController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        //dd($request->all());
        if($request->inv_period==""){
            $current_month=Carbon::now();
        }

        if($request->inv_period != ""){
            $period=$request->inv_period."-01";
            $current_month = Carbon::createFromFormat('Y-m-d', $period);
            //$current_month = $period;
        }

        //dd($current_month);

        //call url for api
        $searchKey = 'rule_name_API Rad Users';
        $rule = Cache::remember($searchKey, 180, function (){
            return Rule::where('rule_name','API Rad Users')->first();
        });

        $url_user_rads=$rule->rule_value;

        $response = Http::get($url_user_rads);
        //dd($response);
        $datas=$response['results'];
        $conn_status=$response['success'];
        //dd($response['success']);

        if($conn_status=='true'){
            //clear table userrads
            //$deleteUrad=UserRad::truncate();

            //insert into table User_rads
            foreach($datas as $data)
            {
                //dd($data['cid']);
                $cid = $data['cid'];
                $searchKey = 'inquiry_user_rad_' . $cid;
                $cek= Cache::remember($searchKey, 180, function () use($cid) {
                    return UserRad::where('cid',$cid)->first();
                });
				
				if($cek==''){
					$userRad=UserRad::create([
						'cid' => $data['cid'],
						'username' => $data['username'],
						'value' => $data['value'],
						'id' => $data['id'],
						'groupname' => $data['groupname'],
						'attribute' => $data['attribute'],
						'firstname' => $data['firstname'],
						'lastname' => $data['lastname'],
						'disabled' => $data['disabled'],
					]);

                    // Update cache dengan data yang baru diupdate
                    Cache::put($searchKey, $cek, 180);
				}
				else{
					if($cek->disabled <> $data['disabled']){
						$userRad=UserRad::where('cid',$data['cid'])
						->update([
							'disabled' => $data['disabled'],
						]);

                        // Update cache dengan data yang baru diupdate
                        Cache::put($searchKey, $cek, 180);
					}
				}
            }

            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Success Parse Radius List',
                'conn_status' => "true",
            ]);
        }
        else{
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'rad-groupstatus',
                'description' => 'Failed Parse Radius List',
                'conn_status' => 'false',
            ]);
        }

        //cache
        $now = Carbon::today();
        $now = $now->format('Y-m-d');

        $searchKey = 'billing_index_' . md5(json_encode($now));
        $query_billings = Cache::remember($searchKey, 180, function () use ($current_month) {
            return Billing::select(DB::raw('*,billings.updated_at as last_update'))
            ->whereMonth('DocDate',$current_month)
            ->whereYear('DocDate',$current_month)
            ->leftJoin('user_rads','billings.CardCode','user_rads.cid')
            ->leftJoin('quarantines','user_rads.cid','quarantines.cid')
            ->groupBy('DocNum')
            ->get();
        });

        //dd($query_billings);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Billing Menu';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.index',compact('query_billings','current_month'));
    }

    public function indexSearch(Request $request)
    {
        //dd('hai');
        if($request->search_by !='' || $request->search_method !='' || $request->search_value !=''){
            $request->validate([
                'search_by' => 'required',
                'search_method' => 'required',
                'search_value' => 'required',
            ]);

            //call url for api
            $searchKey = 'rule_name_API Rad Users';
            $rule = Cache::remember($searchKey, 180, function (){
                return Rule::where('rule_name','API Rad Users')->first();
            });

            $url_user_rads=$rule->rule_value;

            $response = Http::get($url_user_rads);
            //dd($response);
            $datas=$response['results'];
            $conn_status=$response['success'];
            //dd($response['success']);

            if($conn_status=='true'){
                //clear table userrads
                //$deleteUrad=UserRad::truncate();

                //insert into table User_rads
                foreach($datas as $data)
                {
                //dd($data['cid']);
                    $cid = $data['cid'];
                    $searchKey = 'inquiry_user_rad_' . $cid;
                    $cek= Cache::remember($searchKey, 180, function () use($cid) {
                        return UserRad::where('cid',$cid)->first();
                    });

                    if($cek==''){
                        $userRad=UserRad::create([
                            'cid' => $data['cid'],
                            'username' => $data['username'],
                            'value' => $data['value'],
                            'id' => $data['id'],
                            'groupname' => $data['groupname'],
                            'attribute' => $data['attribute'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'disabled' => $data['disabled'],
                        ]);

                        // Update cache dengan data yang baru diupdate
                        Cache::put($searchKey, $cek, 180);
                    }
                    else{
                        if($cek->disabled <> $data['disabled']){
                            $userRad=UserRad::where('cid',$data['cid'])
                            ->update([
                                'disabled' => $data['disabled'],
                            ]);

                            // Update cache dengan data yang baru diupdate
                            Cache::put($searchKey, $cek, 180);
                        }
                    }
                }

                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Success Parse Radius List',
                    'conn_status' => "true",
                ]);
            }
            else{
                //clear table userrads
                //$deleteUrad=UserRad::truncate();
                
                //insert api_log
                $api_logs=ApiLog::create([
                    'api_name' => 'rad-groupstatus',
                    'description' => 'Failed Parse Radius List',
                    'conn_status' => 'false',
                ]);
            }

            $query=Billing::select(DB::raw('*,billings.updated_at as last_update'))
            ->leftJoin('user_rads','billings.CardCode','user_rads.cid')
            ->leftJoin('quarantines','user_rads.cid','quarantines.cid')
            ->groupBy('DocNum');

            if($request->search_method=='any'){
                $search=$query->where('billings.'.$request->search_by, 'LIKE', '%'.$request->search_value.'%');
            }
            else if($request->search_method=='exact'){
                $search=$query->where('billings.'.$request->search_by, $request->search_value);
            }
            //Cache::forget('billing_inquiry');//gak perlu dihapus
            $searchKey = 'billing_inquiry_' . md5(json_encode($request->search_method . $request->search_by . $request->search_value));
            $query_billings = Cache::remember($searchKey, 180, function () use ($search) {
                return $search->get();
            });

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Billing Menu';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
            $current_month=Carbon::now();
            return view('Billing.index',compact('query_billings','current_month'));
        }
        else{
            $query_billings=[];
            $current_month=Carbon::now();
            return view('Billing.index',compact('query_billings','current_month'));
        }
    }

    public function show($inv)
    {
        $info=Billing::select(DB::raw('*,billings.updated_at as last_update'))
        ->where('DocNum',$inv)
        ->first();

        $current_billings=Billing::where('DocNum',$inv)
        ->get();

        //dd($current_billings);
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Show Customer Billing';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('Billing.show',compact('info','current_billings'));
    }

}