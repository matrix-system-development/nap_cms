<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Billing;
use App\Models\BillingTakeout;
use App\Models\ApiLog;
use App\Models\QueInvoice;
use App\Models\Rule;

class BillingCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Billing:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Monthly Billing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //$now='2023-03-05';
        $now=Carbon::now()->format('Y-m-d');
        $month=Carbon::now()->format('Y-m');
        $year=Carbon::now()->format('Y');
        $exec_time=Carbon::now()->format('Y-m-d H:i:s');

        //call url for api
        $rule=Rule::where('rule_name','API Inquiry Billing All')->first();
        $url_billing_all=$rule->rule_value;

        $response = Http::get($url_billing_all.$now);
        $datas=$response['results'];
        $count_inv=$response['row_count'];

        foreach($datas as $data)
        {
            $DocDate=date_format(date_create($data['DocDate']),"Y-m-d");
            //dd($data['Advance'],$data['DocNum'],$DocDate,$now);
            if($data['Advance']=='N'){
                //cek apakah line sudah ada
                $cek_billing=Billing::where(DB::raw('CONCAT(DocNum,LineNum)'),$data['DocNum'].$data['LineNum'])->get();
                $count_cek=count($cek_billing);

                //cek previous month
                $check =  Billing::where('CardCode',$data['CardCode'])->count();

                if($check > 0){
                    $billing_newCustomer = "NO";  
                }
                else {
                    $billing_newCustomer = "YES";  
                }
                //end cek previous month
                
                if($count_cek=='0'){
                    if($DocDate==$now){
                        //jika belum ada di insert dan checker 1
                        $query_billing=Billing::create([
                            'CardCode'=> $data['CardCode'],
                            'CardName'=> $data['CardName'],
                            'PayToCode'=> $data['PayToCode'],
                            'CustAddress'=> $data['Cust Address'],
                            'EnvAddress'=> $data['Env Address'],
                            'Attention'=> $data['Attention'],
                            'DocNum'=> $data['DocNum'],
                            'LineNum'=> $data['LineNum'],
                            'DocDate'=> $data['DocDate'],
                            'DocDueDate'=> $data['DocDueDate'],
                            'CustomerOrderNo'=> $data['Cust Order No'],
                            'SalesID'=> $data['SalesID'],
                            'Description'=> $data['Description'],
                            'SOCurrency'=> $data['SO Currency'],
                            'UnitPrice'=> $data['Unit Price'],
                            'TotalPrice'=> $data['Total Price'],
                            'Amount'=> $data['Amount'],
                            'Rebate'=> $data['Rebate'],
                            'PPH23'=> $data['PPH23'],
                            'PPN'=> $data['PPN'],
                            'TotalAmount'=> floor($data['Total Amount']),
                            'VA'=> $data['VA'],
                            'CompanyCodeVA'=> $data['Company Code VA'],
                            'CustCodeVA'=> $data['Cust Code VA'],
                            'PaymentStatus'=> $data['PaymentStatus'],
                            'CID'=> $data['CID'],
                            'CreatedDateINV'=> $data['CreateDate'],
                            'Outstanding' => floor($data['Outstanding']),
                            'NewCustomer' => $billing_newCustomer,
                            'checker'=> '1',
                            'Email' => $data['Email'],
                            'PhoneNo' => $data['PhoneNo'],
                            'Service' => $data['Service'],
                            'Kapasitas' => $data['Kapasitas'],
                            'Kapasitas_UOM' => $data['Kapasitas UOM'],
                            'Asuransi' => $data['Asuransi'],
                            'Billing_Pay_Type' => $data['Billing Pay Type'],
                            'Regional' => $data['Regional'],
                            'Sales_Code' => $data['Sales Code'],
                            'Sales_Employee' => $data['Sales Employee'],
                            'Advance' => $data['Advance'],
                            'OutstandingBulanLalu' => floor($data['Outstanding Bulan Lalu']),
                            'current_outstanding' => floor($data['Outstanding']),
                        ]);
                        
                        if($data['LineNum']=='1'){
                            //Insert ke que Invoice
                            $storeQueInv=QueInvoice::insertOrIgnore([
                                'inv_no'=> $data['DocNum'],
                                'email_address' => $data['Email'],
                                'is_corp' => '0',
                            ]);
                            //Insert ke que Invoice 
                        }   
                    } 
                }else{
                    //jika ada maka update checker 1
                    $update_checker=Billing::where(DB::raw('CONCAT(DocNum,LineNum)'),$data['DocNum'].$data['LineNum'])
                    ->update([
                        'NewCustomer' => $billing_newCustomer,
                        'checker' => '1',
                        //'Outstanding' => $data['Outstanding'], //outstanding di update juga karna SAP selalu update outstanding sesuai tgl billing
                        'Email' => $data['Email'],
                        'PhoneNo' => $data['PhoneNo'],
                        'Service' => $data['Service'],
                        'Kapasitas' => $data['Kapasitas'],
                        'Kapasitas_UOM' => $data['Kapasitas UOM'],
                        'Asuransi' => $data['Asuransi'],
                        'Billing_Pay_Type' => $data['Billing Pay Type'],
                        'Regional' => $data['Regional'],
                        'Sales_Code' => $data['Sales Code'],
                        'Sales_Employee' => $data['Sales Employee'],
                        'Advance' => $data['Advance'],
                        'OutstandingBulanLalu' => floor($data['Outstanding Bulan Lalu']),
                    ]);
                }
            }
        }

        //yang checker nya nol dipindahkan ke table billing takeout
        $cek_billing_uncheck=Billing::whereMonth('DocDate',Carbon::now())
        ->whereYear('DocDate',Carbon::now())
        ->where('checker','0')
        ->get();

        foreach ($cek_billing_uncheck as $data_out) {
        //insert ke table billing takeout
            $insert_billingTakeout=BillingTakeout::create([
                'CardCode'=> $data_out['CardCode'],
                'CardName'=> $data_out['CardName'],
                'PayToCode'=> $data_out['PayToCode'],
                'CustAddress'=> $data_out['CustAddress'],
                'EnvAddress'=> $data_out['EnvAddress'],
                'Attention'=> $data_out['Attention'],
                'DocNum'=> $data_out['DocNum'],
                'LineNum'=> $data_out['LineNum'],
                'DocDate'=> $data_out['DocDate'],
                'DocDueDate'=> $data_out['DocDueDate'],
                'CustomerOrderNo'=> $data_out['CustomerOrderNo'],
                'SalesID'=> $data_out['SalesID'],
                'Description'=> $data_out['Description'],
                'SOCurrency'=> $data_out['SOCurrency'],
                'UnitPrice'=> $data_out['UnitPrice'],
                'TotalPrice'=> $data_out['TotalPrice'],
                'Amount'=> $data_out['Amount'],
                'Rebate'=> $data_out['Rebate'],
                'PPH23'=> $data_out['PPH23'],
                'PPN'=> $data_out['PPN'],
                'TotalAmount'=> floor($data_out['TotalAmount']),
                'VA'=> $data_out['VA'],
                'CompanyCodeVA'=> $data_out['CompanyCodeVA'],
                'CustCodeVA'=> $data_out['CustCodeVA'],
                'PaymentStatus'=> $data_out['PaymentStatus'],
                'CID'=> $data_out['CID'],
                'CreatedDateINV'=> $data_out['CreatedDateINV'],
                'Outstanding' => floor($data['Outstanding']),
                'NewCustomer' => $data['NewCustomer'],
                'checker'=> $data_out['checker'],
                'Email' => $data_out['Email'],
                'PhoneNo' => $data['PhoneNo'],
                'Service' => $data['Service'],
                'Kapasitas' => $data['Kapasitas'],
                'Kapasitas_UOM' => $data['Kapasitas UOM'],
                'Asuransi' => $data['Asuransi'],
                'Billing_Pay_Type' => $data['Billing Pay Type'],
                'Regional' => $data['Regional'],
                'Sales_Code' => $data['Sales Code'],
                'Sales_Employee' => $data['Sales Employee'],
                'Advance' => $data['Advance'],
                'OutstandingBulanLalu' => floor($data['Outstanding Bulan Lalu']),
                'current_outstanding' => floor($data['Outstanding']),
            ]);
        }

        $delete_uncheck=Billing::whereMonth('DocDate',Carbon::now())
                ->whereYear('DocDate',Carbon::now())
                ->where('checker','0')
                ->delete();

        //update billing checker jd nol
        $update_checker=Billing::whereMonth('DocDate',Carbon::now())
                ->whereYear('DocDate',Carbon::now())
                ->update([
                    'checker' => '0'
                ]);

        //insert api_log
        $api_logs=ApiLog::create([
            'api_name' => 'inq-billing',
            'description' => 'Success Parse '.$count_inv.' Billing Invoices',
            'conn_status' => "true",
        ]);

        $success= $count_inv." rows, success job at ".$exec_time; 
        echo $success;
    }
}
