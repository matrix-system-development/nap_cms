<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Quarantine;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Log;
use App\Models\Rule;
use App\Models\Billing;

class TempUnblockCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tempUnblock:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Temporary Unblock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now=Carbon::now();
        $cek_quarantine=Quarantine::where('temporary_unblock_flag','1')
        ->get();
        
        foreach ($cek_quarantine as $data) {
            if($data->temporary_unblock_exp <= $now){

                //cek ke billing
                $cek_billing=Billing::where('CardCode',$data->cid)
                ->orderBy('DocNum','desc')
                ->first();

                //dd($cek_billing);

                if(($cek_billing->PaymentStatus=='Not Paid') && ($now >= $cek_billing->DocDueDate))
                {
                    //call url for api block
                    $rule=Rule::where('rule_name','API Rad Block')->first();
                    $url_block_rads=$rule->rule_value;

                    $response = Http::post($url_block_rads, [
                        'cid' => $data->cid,
                    ]);
                    //dd($response);
                    $cid=$response['cid'];

                    if($response['success']=="true"){
                        $username=$response['user'];
                        $message=$response['message'];
                        $created_by="rad-block";

                        $updateQuarantine=Quarantine::where('cid',$data->cid)
                        ->update([
                            'temporary_unblock_flag' => '0',
                            'temporary_unblock_exp' => '',
                            'created_by' => 'tempUnblock:cron',
                            'message' => 'Temporary Unblock Expired',
                            'is_approval' => '0', //tidak butuh approval
                            'id_approval' => '0',
                        ]);

                        if($updateQuarantine){
                            $logs=Log::create([
                                'cid_log' => $data->cid,
                                'username_log' => $username,
                                'message_log' => $message,
                                'action_by_log' => 'tempUnblock:cron',
                                'created_by_log' => $created_by,
                                'remarks_log' => 'Temporary Unblock Expired',
                            ]);
                        }
                        else{
                            $logs=Log::create([
                                'cid_log' => $data->cid,
                                'username_log' => $username,
                                'message_log' => $message,
                                'action_by_log' => 'tempUnblock:cron',
                                'created_by_log' => $created_by,
                                'remarks_log' => 'Failed Update Quarantine',
                            ]);
                        }
                    }
                    else{
                        $username=$response['error'];
                        $message=$response['message'];
                        $created_by="rad-block";

                        $logs=Log::create([
                            'cid_log' => $data->cid,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' => 'tempUnblock:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Failed Update Quarantine',
                        ]);
                    }
                }
                else{
                    //unblock permanen
                    $rule=Rule::where('rule_name','API Rad Unblock')->first();
                    $url_unblock_rads=$rule->rule_value;

                    $response = Http::post($url_unblock_rads, [
                        'cid' => $data->cid,
                    ]);

                    if($response['success']=="true"){
                        $username=$response['user'];
                        $message=$response['message'];
                        $created_by="rad-unblock";

                        $logs=Log::create([
                            'cid_log' => $data->cid,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' => 'tempUnblock:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Temporary Unblock Expired',
                        ]);
    
                        $delete_quarantine=Quarantine::where('cid',$data->cid)
                        ->delete();
                    }
                    else{
                        $username=$response['error'];
                        $message=$response['message'];
                        $created_by="rad-unblock";

                        $logs=Log::create([
                            'cid_log' => $data->cid,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' =>'tempUnblock:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Failed Update Log',
                        ]);
                    }
                }
            }
        }
    }
}
