<?php

namespace App\Console\Commands;

use App\Models\Rule;
use App\Models\TempCheckPayment;
use Illuminate\Console\Command;
use App\Traits\AJPTrait;
use Carbon\Carbon;

class StoreAJP_SAPCron extends Command
{
    use AJPTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'StoreAJP:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store data into SAP API Payment to create Atomatic Journal Payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //1. seleksi data temp check payment yang is_sent_SAP = 0 & list payment channel nya ambil dari rules
        //cari list payment channel yang di push ke SAP
        $channelList = Rule::where('rule_name','Auto Journal Payment')->pluck('rule_value');

        //get data pembayaran yang akan di push ke SAP
        $datas = TempCheckPayment::where('is_sent_SAP','0')
            ->whereIn('payment_channel',$channelList)
            //->where('RequestID','202410010947530256700987130749')//nanti ini dimatiin
            ->get();

        //2. lakukan looping untuk di post ke API Payment SAP & update  is_sent_SAP = 1
        foreach ($datas as $data) {
            $id_payment = $data->id;
            $payment_date = $data->payment_date;

            // Check if payment_date is end of month and time is 21:00 or later
            $date = Carbon::parse($payment_date);
            $endOfMonth = $date->copy()->endOfMonth();
            $timeCheck = $date->hour >= 21;

            if ($date->isSameDay($endOfMonth) && $timeCheck) {
                $message = "Tanggal pembayaran adalah akhir bulan dan waktu adalah pukul 21:00 atau lebih untuk ID Pembayaran " . $id_payment . "\n";
                echo $message;
            } 
            else {
                try {
                    $settleAJP = $this->settlementAJP($id_payment, $payment_date);
    
                    if($settleAJP=="success"){
                        $message = $id_payment . " AJP Success\n";
                        echo $message;
                    }
                    else{
                        $message = $id_payment . " AJP Failed\n";
                        echo $message;
                    }
                } catch (\Throwable $th) {
                    $message = "Error processing Payment ID " . $id_payment . " : " . $th->getMessage() . "\n";
                    echo $message;
                }
            }
        }
    }
}
