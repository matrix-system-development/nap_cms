<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Billing;
use App\Models\Quarantine;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Log;
use App\Models\ApiLog;
use App\Models\Rule;

class BlockDailyCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BlockCustNew:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Block customer setiap hari jika tidak ada pembayaran untuk customer baru (H+7)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //dd('hai');
        //call url for api
        $rule=Rule::where('rule_name','API Rad Users')->first();
        $url_user_rads=$rule->rule_value;

        $response = Http::get($url_user_rads);
        $datas=$response['results'];
        $conn_status=$response['success'];
        //dd($datas);

        if($conn_status=='true'){
            //dd('hai');
            //Cek berdasarkan Customer ID di Radius
            foreach($datas as $data)
            {
                //dd($data['cid']);
                $nowsub=Carbon::now()->subDays(7)->format('Y-m-d');
                //dd($nowsub);
                //cek ke table billing ambil yg Customer baru yang docdate nya H+7 dan belum bayar
                $cek_billing=Billing::where(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), $nowsub)
                //$cek_billing=Billing::whereBetween(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), [$now_start, $now_end])
                ->where('NewCustomer','YES')
                ->where('CardCode',$data['cid'])
                ->groupBy('CardCode')
                ->get();
               
                //dd($cek_billing);

                foreach ($cek_billing as $billing) {    
                    //cek apakah payment statusnya Not Paid
                    if($billing->PaymentStatus=='Not Paid'){
                        // dd($data['cid']);

                        //jika Cust ID Not Paid maka jalankan API Block
                        //call url for api
                        $rule=Rule::where('rule_name','API Rad Block')->first();
                        $url_block_rads=$rule->rule_value;

                        //dd($data['cid']);
                        $response = Http::post($url_block_rads, [
                            'cid' => $data['cid'],
                        ]);
                        
                        //$cid=$response['cid'];
                        //$cid=$data['cid'];
                        //$username=$response['user'];
                        //$message=$response['message'];
                        //$created_by="rad-block";

                        //dd($cid);
                
                        if($response['success']=="true"){ 
			    $cid=$data['cid'];
                            $username=$response['user'];
                            $message=$response['message'];
                            $created_by="rad-block";

                            // cek table Quarantine dulu
                            $cek_quarantine=Quarantine::where('cid',$data['cid'])->count();
                            //dd($cek_quarantine);

                            // jika di table quarntine cid nya null maka dia isolate cid karna ketika di block maka cid wajib di Isolate
                            if($cek_quarantine =='0'){
                                $quarantine=Quarantine::create([
                                    'cid' => $data['cid'],
                                    'temporary_unblock_flag' => '0',
                                    'temporary_unblock_exp' => '',
                                    'created_by' => 'BlockCustNew:cron',
                                    'message' => 'Customer Not Paid',
                                ]);
                            }
                            else{
                                //dd($data['cid']);
                                $quarantine=Quarantine::where('cid',$data['cid'])
                                    ->update([
                                        'temporary_unblock_flag' => '0',
                                        'temporary_unblock_exp' => '',
                                        'created_by' => 'BlockCustNew:cron',
                                        'message' => 'Customer Not Paid',
                                    ]);
                            }

                            if($quarantine){
                                $logs=Log::create([
                                    'cid_log' => $cid,
                                    'username_log' => $username,
                                    'message_log' => $message,
                                    'action_by_log' => 'BlockCustNew:cron',
                                    'created_by_log' => $created_by,
                                    'remarks_log' => 'Customer Not Paid',
                                ]);

                                if($logs){
                                    //insert api_log
                                    $api_logs=ApiLog::create([
                                        'api_name' => 'Service Block Cust',
                                        'description' => 'Success Block Customer',
                                        'conn_status' => "true",
                                    ]);
                                }
                            }
                        }
                        else{ 
			    $cid=$data['cid'];
                            $username=$response['error'];
                            $message=$response['message'];
                            $created_by="rad-block";

                            $logs=Log::create([
                                'cid_log' => $cid,
                                'username_log' => $username,
                                'message_log' => $message,
                                'action_by_log' => 'BlockCustNew:cron',
                                'created_by_log' => $created_by,
                            ]);
                            
                            if($logs){
                                //insert api_log
                                $api_logs=ApiLog::create([
                                    'api_name' => 'Service Block Cust',
                                    'description' => 'Success Block Customer',
                                    'conn_status' => "true",
                                ]);
                            }
                        }
                    }
                    echo $data['cid'];
                }
            }
        }
        else{
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'Service Block Cust',
                'description' => 'Failed Block Customer',
                'conn_status' => "true",
            ]);
        }

        return Command::SUCCESS;
    }
}
