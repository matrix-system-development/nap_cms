<?php

namespace App\Console\Commands;

use App\Models\TempCheckPayment;
use Illuminate\Console\Command;
use App\Traits\AJPTrait;

class CheckAJP_SAPCron extends Command
{
    use AJPTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckSyncAJP:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking and update is sync from SAP paymemt temp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //get data yang is_sent_SAP='1' and is_sync_SAP='0'
        $datas = TempCheckPayment::where('is_sent_SAP','1')
            ->where('is_sync_SAP', '0')
            ->get();
        
        foreach ($datas as $data) {
            $id_payment = $data->id;
            try {
                //sent data to API inquiry temp payment
                $checkAJP = $this->checkAJP($id_payment);
                
                if($checkAJP == "success"){
                    $message = $id_payment . " Check AJP Success\n";
                    echo $message;
                }
                else{
                    $message = $id_payment . " Check AJP Failed\n";
                    echo $message;
                }
            } catch (\Throwable $th) {
                //throw $th;
                $message = "Error check Payment ID " . $id_payment . " : " . $th->getMessage() . "\n";
                echo $message;
            }
        }
    }
}
