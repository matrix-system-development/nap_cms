<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\QueUpdateBilling;
use App\Models\Billing;
use App\Models\Rule;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class BillingApprovalCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BillingApproval:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $que_update = QueUpdateBilling::get();

        $url_inquiry = Rule::where('rule_name','Inquiry Billing by Docnum')->first();
        $url_inquiry = $url_inquiry->rule_value;
        
        foreach ($que_update as $item) {
            $response = Http::get($url_inquiry.$item->docnum);
            if ($response->successful()) {
                $customer = $response->json();
                $customer = $customer['results'];
                $delete_billing = Billing::where('docnum',$item->docnum)->delete();
                foreach ($customer as $customer_data) {
                    $data = $customer_data;
                    $check =  Billing::where('CardCode',$data['CardCode'])->count();
                    if($check > 0){
                        $billing_newCustomer = "NO";  
                    }
                    else {
                        $billing_newCustomer = "YES";  
                    }
    
                    DB::beginTransaction();
                    try {
                        
                        $delete_que = QueUpdateBilling::where('docnum',$item->docnum)->delete();
                        $insert_billing=Billing::create([
                            'CardCode'=> $data['CardCode'],
                            'CardName'=> $data['CardName'],
                            'PayToCode'=> $data['PayToCode'],
                            'CustAddress'=> $data['Cust Address'],
                            'EnvAddress'=> $data['Env Address'],
                            'Attention'=> $data['Attention'],
                            'DocNum'=> $data['DocNum'],
                            'LineNum'=> $data['LineNum'],
                            'DocDate'=> $data['DocDate'],
                            'DocDueDate'=> $data['DocDueDate'],
                            'CustomerOrderNo'=> $data['Cust Order No'],
                            'SalesID'=> $data['SalesID'],
                            'Description'=> $data['Description'],
                            'SOCurrency'=> $data['SO Currency'],
                            'UnitPrice'=> $data['Unit Price'],
                            'TotalPrice'=> $data['Total Price'],
                            'Amount'=> $data['Amount'],
                            'Rebate'=> $data['Rebate'],
                            'PPH23'=> $data['PPH23'],
                            'PPN'=> $data['PPN'],
                            'TotalAmount'=> floor($data['Total Amount']),
                            'VA'=> $data['VA'],
                            'CompanyCodeVA'=> $data['Company Code VA'],
                            'CustCodeVA'=> $data['Cust Code VA'],
                            'PaymentStatus'=> $data['PaymentStatus'],
                            'CID'=> $data['CID'],
                            'CreatedDateINV'=> $data['CreateDate'],
                            'Outstanding' => floor($data['Outstanding']),
                            'NewCustomer' => $billing_newCustomer,
                            'checker'=> '1',
                            'Email' => $data['Email'],
                            'PhoneNo' => $data['PhoneNo'],
                            'Service' => $data['Service'],
                            'Kapasitas' => $data['Kapasitas'],
                            'Kapasitas_UOM' => $data['Kapasitas UOM'],
                            'Asuransi' => $data['Asuransi'],
                            'Billing_Pay_Type' => $data['Billing Pay Type'],
                            'Regional' => $data['Regional'],
                            'Sales_Code' => $data['Sales Code'],
                            'Sales_Employee' => $data['Sales Employee'],
                            'Advance' => $data['Advance'],
                            'OutstandingBulanLalu' => floor($data['Outstanding Bulan Lalu']),
                            'current_outstanding' => floor($data['Outstanding']),
                        ]);
                         DB::commit();
                    } catch (\Throwable $th) {
                            DB::rollBack();
                        }    
                }
            } else {
                    $statusCode = $response->status();
            }
        }

        return Command::SUCCESS;
    }
}
