<?php

namespace App\Console\Commands;

use App\Models\ApiLog;
use App\Models\BillingCorp;
use App\Models\Log;
use App\Models\Quarantine;
use App\Models\Rule;
use App\Models\TempCheckPayment;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CheckPaymentCorpCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckPaymentCorp:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and Update status payment in Billing Corporate Table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //cek Billing yang Not Paid
        $query_billing=BillingCorp::where('PaymentStatus','Not Paid')
        ->groupBy('DocNum')
        ->get();        

        foreach ($query_billing as $data) {
            //call url for api
            $rule=Rule::where('rule_name','API Inquiry Payment')->first();
            $url_payment=$rule->rule_value;

            //cek ke api inq-payment
            $response = Http::get($url_payment.$data->DocNum);
            $conn_status=$response['success'];
            
            if($conn_status=="true"){
                $data_payment=$response['results'];
                //dd($data_payment);
                foreach ($data_payment as $payment) {
                    $income_payment=$data->DocNum.$payment['Code'].$payment['Nominal Payment'];
                    //dd($income_payment);
                    $cek_temp=TempCheckPayment::where(DB::raw('CONCAT(inv_number,cust_code,nominal_payment,"0000")'),$income_payment)->get();
                    //dd($cek_temp);
                    $count_cek_temp=count($cek_temp);
                    
                    if($count_cek_temp=='0'){
                        //insert ke table temp
                        $temp_check=TempCheckPayment::create([
                            'inv_number' => $data->DocNum,
                            'status_check' => '0',
                            'cust_code' => $payment['Code'],
                            'nominal_payment' => $payment['Nominal Payment'],
                            'payment_date' => $payment['PaymentDate'],
                            'payment_channel' => 'SAP',
                        ]);
                    }
                }
            }
        }
        //dd('hai');
        //cek temp yang status payment 0
        $temp_table=TempCheckPayment::where('status_check','0')->get();

        //update table billing
        foreach ($temp_table as $item) {
            $cek_outstanding=BillingCorp::where('DocNum',$item->inv_number)
            ->groupBy('DocNum')
            ->first();

            //$upd_outstanding=$data->Outstanding - $data_payment[->Nominal Payment];
            $update_billing=BillingCorp::where('DocNum',$item->inv_number)
            ->update([
                'PaymentStatus' => 'Paid',
                'Outstanding' => $cek_outstanding->Outstanding - $cek_outstanding->Outstanding,
            ]);

            if($update_billing){
                $update_check_status=TempCheckPayment::where('inv_number',$item->inv_number)
                ->where('status_check','0')
                ->update([
                    'status_check' => '1',
                ]);
            }

            // Cek jka cid di Quarantine ada atau tidak
            $cek_quarantine=Quarantine::where('cid',$item->cust_code)->count();

            if($cek_quarantine>0){  
                //walaupun cid nya tdk ada di table Quarantine maka fungsi unblock tetap berjalan karna buka temp unblock
                //call url for api
                $rule=Rule::where('rule_name','API Rad Unblock')->first();
                $url_unblock_rads=$rule->rule_value;

                $response = Http::post($url_unblock_rads, [
                    'cid' => $item->cust_code,
                ]);
        
                if($response['success']=="true"){
                    $username=$response['user'];
                    $message=$response['message'];
                    $created_by="inq_payment";

                    $logs=Log::create([
                        'cid_log' => $item->cust_code,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' => 'CheckPayment:cron',
                        'created_by_log' => $created_by,
                        'remarks_log' => 'Status Paid',
                    ]);

                    $delete_quarantine=Quarantine::where('cid',$item->cust_code)
                    ->delete();
                }
                else{
                    $username=$response['error'];
                    $message=$response['message'];
                    $created_by="inq_payment";

                    $logs=Log::create([
                        'cid_log' => $item->cust_code,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' =>'CheckPayment:cron',
                        'created_by_log' => $created_by,
                        'remarks_log' => 'Failed Unblock',
                    ]);
                }
            }
            else{
                //call url for api
                $rule=Rule::where('rule_name','API Rad Unblock')->first();
                $url_unblock_rads=$rule->rule_value;

                $response = Http::post($url_unblock_rads, [
                    'cid' => $item->cust_code,
                ]);
        
                if($response['success']=="true"){
                    $username=$response['user'];
                    $message=$response['message'];
                    $created_by="inq_payment";

                    $logs=Log::create([
                        'cid_log' => $item->cust_code,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' => 'CheckPayment:cron',
                        'created_by_log' => $created_by,
                        'remarks_log' => 'Status Paid',
                    ]);
                }
                else{
                    $username=$response['error'];
                    $message=$response['message'];
                    $created_by="inq_payment";

                    $logs=Log::create([
                        'cid_log' => $item->cust_code,
                        'username_log' => $username,
                        'message_log' => $message,
                        'action_by_log' =>'CheckPayment:cron',
                        'created_by_log' => $created_by,
                        'remarks_log' => 'Failed Unblock',
                    ]);
                }
            }
        }

        //insert api_log
        $api_logs=ApiLog::create([
            'api_name' => 'inq-payment',
            'description' => 'Success Check Payment',
            'conn_status' => "true",
        ]);
    }
}
