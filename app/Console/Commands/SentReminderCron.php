<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\QueReminder;
use App\Models\Billing;
use App\Models\UserRad;
use App\Models\Log;
use App\Mail\ReminderMail;
use Illuminate\Support\Facades\Mail;

class SentReminderCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SentReminder:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sent email reminder billing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cekrow_que=QueReminder::count();

        if($cekrow_que > 0){
            $queues=QueReminder::get();
        
            foreach ($queues as $queue) {
                $billing_unpaid=Billing::where('CardCode',$queue->cust_code)
                ->where('PaymentStatus','Not Paid')
                ->where('NewCustomer','NO')
                ->orderBy('DocNum','desc')
                ->orderBy('LineNum','asc')
                ->first();

                if($billing_unpaid){
                    if (env('APP_ENV')=='local') {
                        $url = 'http://127.0.0.1:8000/';
                    }
                    
                    if (env('APP_ENV')=='staging') {
                        # code...
                        $url = 'https://stagingapi.napinfo.co.id/nap_cms/';
                    }

                    if (env('APP_ENV')=='production') {
                        # code...
                        $url = 'https://lentera.napinfo.co.id/cms/';
                    }

                    $details = [
                        'cust_name' => $billing_unpaid->CardName,
                        'cust_code' => $billing_unpaid->CardCode,
                        'url' => 'http://127.0.0.1:8000/',
                        'inv_date' => $billing_unpaid->DocDate,
                        'inv_duedate' => $billing_unpaid->DocDueDate,
                        'inv_no' => $billing_unpaid->DocNum,
                        'currency' => $billing_unpaid->SOCurrency,
                        'amount' => $billing_unpaid->Outstanding
                    ];
                    $recipient=$queue->email_address;
        
                    $sent=Mail::to($recipient)
                    ->send(new ReminderMail($details));

                    if(count(Mail::failures()) > 0 ){
                        $message_log='Failed Sent Email Reminder Billing Notification';
                        $remark_log='Failed Sent Email';
                    } 
                    else{
                        $message_log='Success Sent Email Reminder Billing Notification';
                        $remark_log='Success Sent Email';
                    }
                }

                //cek username
                $user_rad=UserRad::where('cid',$queue->cust_code)->first();
                $count_user_rad=UserRad::where('cid',$queue->cust_code)->count();

                if($count_user_rad > 0){
                    $username=$user_rad->username;
                }
                else{
                    $username='Username Not Found';
                }

                //Log
                $logs=Log::create([
                    'cid_log' => $queue->cust_code,
                    'username_log' => $username,
                    'message_log' => $message_log,
                    'action_by_log' => 'SentEmailReminder:cron',
                    'created_by_log' => 'send-queue-reminder-email',
                    'remarks_log' => $remark_log,
                ]);

                //delete queue
                $delete_queue=QueReminder::where('cust_code',$queue->cust_code)->delete();
            }
            
            $message='Success Sent '.$cekrow_que.' Queue Reminder';
        
            echo $message;
        }
        else{
            $message='No Queue to Sent';
        
            echo $message;
        }
    }
}
