<?php

namespace App\Console\Commands;

use App\Mail\InvoiceMail;
use App\Models\QueInvoice;
use App\Models\Rule;
use App\Models\Billing;
use App\Models\BillingCorp;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class InvoiceCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Invoice:Cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sent Customer Invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cekrow_que=QueInvoice::count();
        $cekRule=Rule::where('rule_name','Sent Invoice')->first();

        if($cekRule->rule_value=='1'){
            if($cekrow_que > 0){
                $invoices=QueInvoice::limit(15)->get();
    
                foreach ($invoices as $invoice) {
                    if(env('APP_ENV')=='local' || env('APP_ENV')=='staging') {
                        $email="enquity.ekayekti@napinfo.co.id;hardy.prabowo@napinfo.co.id";
                    } 
                    else{
                        //Jika environment PRODUCTION dinyalain untuk siap kirim ke customer
                        $email=str_replace(" ","",$invoice->email_address);   # code...
                    }
                    
                    $recipients=explode(";",$email);
                    
                    foreach ($recipients as $recipient) {
                        //percabangan query billing corp dan broadband
                        if($invoice->is_corp=='1'){
                            $billings =  BillingCorp::where('docnum',$invoice->inv_no)
                            ->orderBy("LineNum","asc")
                            ->first();
                            $date_check = date('d-M-Y', strtotime($billings->DocDate));
                            $due_date_check = date( "Y-m", strtotime( "$date_check -1 month" ) );
                            $check = BillingCorp::where('CardCode',$billings->CardCode)
                            ->where(DB::raw("date_format(DocDate,'%Y-%m')"),$due_date_check)
                            ->count();
                        }
                        else{
                            $billings =  Billing::where('docnum',$invoice->inv_no)
                            ->orderBy("LineNum","asc")
                            ->first();
                            $date_check = date('d-M-Y', strtotime($billings->DocDate));
                            $due_date_check = date( "Y-m", strtotime( "$date_check -1 month" ) );
                            $check = Billing::where('CardCode',$billings->CardCode)
                            ->where(DB::raw("date_format(DocDate,'%Y-%m')"),$due_date_check)
                            ->count();
                        }
                        //end check new customer

                        if($check > 0){
                            $billing_newCustomer = "NO";  
                        }
                        else {
                             $billing_newCustomer = "YES";  
                        }
                        if ($billing_newCustomer == "YES") {
                            $date_month = date('M Y', strtotime($billings->DocDate));
                            $date = date('d-M-Y', strtotime($billings->DocDate));
                            $due_date = date( "d-M-Y", strtotime( "$date +7 day" ) );
                        } else {
                            $date_month = new DateTime(date('M Y', strtotime($billings->DocDate)));
                            $date = new DateTime(date('d-m-Y', strtotime($billings->DocDate)));
                            $test = $date->setDate($date->format('Y'), $date->format('m'),25 );
                            $due_date = $test->format('d-M-Y');
                        }
                        if($invoice->is_corp=='1'){
                            $due_date = date('d-M-Y', strtotime($billings->DocDueDate));
                        }
                        if($invoice->is_corp=='1'){
                            $details = [
                                'inv_no' => $invoice->inv_no,//tanggal diuery dari invoice no hari ini
                                'tgl_tagihan_bulan' => (date('M Y', strtotime($billings->DocDate))),
                                'tgl_jatuh_tempo' => $due_date,
                                'tgl_tagihan' => (date('d-M-Y', strtotime($billings->DocDate))),
                                'outstanding' => round($billings->current_outstanding),
                                'is_corp' => $invoice->is_corp,
                                'subject' => 'Tagihan Internet Corporate Matrix NAP Info'
                            ];
                        }
                        else{
                            $details = [
                                'inv_no' => $invoice->inv_no,//tanggal diuery dari invoice no hari ini
                                'tgl_tagihan_bulan' => (date('M Y', strtotime($billings->DocDate))),
                                'tgl_jatuh_tempo' => $due_date,
                                'tgl_tagihan' => (date('d-M-Y', strtotime($billings->DocDate))),
                                'outstanding' => round($billings->current_outstanding),
                                'is_corp' => $invoice->is_corp,
                                'subject' => 'Tagihan Internet Broadband Matrix NAP Info'
                            ];
                        }

                        if(env('APP_ENV')=='local' || env('APP_ENV')=='staging') {
                            $bcc="hardy.prabowo@napinfo.co.id";
                        } else {
                            //Jika environment PRODUCTION dinyalain BCC ke REVA
                            $bcc="revenue.assurance@napinfo.co.id";
                        }
                        
                        //dd($details);
                        $sent=Mail::to($recipient)
                            ->bcc($bcc)
                            ->send(new InvoiceMail($details));
                    }

                    $deleteQue=QueInvoice::where('inv_no',$invoice->inv_no)->delete();
                }
    
                $message='Success Sent Queue Invoice';
            
                echo $message;
            }
            else{
                $message='No Queue to Sent';
            
                echo $message;
            }
        }
    }
}
