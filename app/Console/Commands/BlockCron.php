<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Billing;
use App\Models\Quarantine;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Log;
use App\Models\ApiLog;
use App\Models\Rule;

class BlockCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BlockCust:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Block customer setiap tanggal 26 jika tidak ada pembayaran untuk customer lama';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //dd('test');
		//dd($data['cid']);
		$now_start=Carbon::now()->format('Y-m-05');
		$now_end=Carbon::now()->format('Y-m-20');
	
		//cek ke table billing ambil yg DocDate nya tanggal 5-20 di bulan berjalan
		//$cek_billing=Billing::where(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), $now)
		$cek_billing=Billing::whereBetween(DB::raw("(STR_TO_DATE(DocDate,'%Y-%m-%d'))"), [$now_start, $now_end])
		->where('NewCustomer','NO')
		//->where('CardCode','C090326')
		->where('PaymentStatus','Not Paid')
		->groupBy('CardCode')
		->get();
	
		//dd($cek_billing);

		foreach ($cek_billing as $billing) {    
			//call url for api
			$rule=Rule::where('rule_name','API Rad Block')->first();
			$url_block_rads=$rule->rule_value;

			//dd($billing->CardCode);
			$response = Http::post($url_block_rads, [
				'cid' => $billing->CardCode,
			]);

			//dd($cid);
	
			if($response['success']=="true"){
				//$cid=$response['cid'];
				//dd($response['user']);
				$cid=$billing->CardCode;
				$username=$response['user'];
				$message=$response['message'];
				$created_by="rad-block";
				// cek table Quarantine dulu
				$cek_quarantine=Quarantine::where('cid',$billing->CardCode)->count();
				//dd($cek_quarantine);

				// jika di table quarntine cid nya null maka dia isolate cid karna ketika di block maka cid wajib di Isolate
				if($cek_quarantine =='0'){
					$quarantine=Quarantine::create([
						'cid' => $billing->CardCode,
						'temporary_unblock_flag' => '0',
						'temporary_unblock_exp' => '',
						'created_by' => 'BlockCust:cron',
						'message' => 'Customer Not Paid',
						'is_approval' => '0' //tidak butuh approval
					]);
				}
				else{
					//dd($billing->CardCode);
					$quarantine=Quarantine::where('cid',$billing->CardCode)
						->update([
							'temporary_unblock_flag' => '0',
							'temporary_unblock_exp' => '',
							'created_by' => 'BlockCust:cron',
							'message' => 'Customer Not Paid',
							'is_approval' => '0' //tidak butuh approval
						]);
				}

				if($quarantine){
					//dd($response['user']);
					$logs=Log::create([
						'cid_log' => $cid,
						'username_log' => $username,
						'message_log' => $message,
						'action_by_log' => 'BlockCust:cron',
						'created_by_log' => $created_by,
						'remarks_log' => 'Customer Not Paid',
					]);

					if ($logs) {
						//insert api_log
						$api_logs=ApiLog::create([
							'api_name' => 'Service Block Cust',
							'description' => 'Success Block Customer',
							'conn_status' => "true",
						]);
					}
				}
			}
			else{
				//dd($billing->CardCode);
				$cid=$billing->CardCode;
				$username=$response['error'];
				$message=$response['message'];
				$created_by="rad-block";
				
				$logs=Log::create([
					'cid_log' => $cid,
					'username_log' => $username,
					'message_log' => $message,
					'action_by_log' => 'BlockCust:cron',
					'created_by_log' => $created_by,
				]);

				if ($logs) {
					//insert api_log
					$api_logs=ApiLog::create([
						'api_name' => 'Service Block Cust',
						'description' => 'Success Block Customer',
						'conn_status' => "true",
					]);
				}
			}
			//echo $quarantine['cid'];
		}

        return Command::SUCCESS;
    }
}
