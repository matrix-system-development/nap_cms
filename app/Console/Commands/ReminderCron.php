<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\QueReminder;
use App\Models\Billing;
use App\Models\BillingTakeout;
use App\Models\ApiLog;
use App\Models\Rule;

class ReminderCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Reminder:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Queue for Customer Billing Reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $month=Carbon::now()->format('m');
        $year=Carbon::now()->format('Y');
        $exec_time=Carbon::now()->format('Y-m-d H:i:s');

        //delete queue
        $delete_queue=QueReminder::truncate();

        //cari yang unpaid
        $unpaid_bills=Billing::whereYear('DocDate',$year)
        ->whereMonth('DocDate',$month)
        ->where('NewCustomer','NO')
        ->where('PaymentStatus','Not Paid')
        ->groupBy('CardCode')
        //->take(10)
        ->get();

        //dd($unpaid_bills);
        
        foreach ($unpaid_bills as $data) {
            $cust_emails=explode(";",$data->Email);
            //dd($cust_emails);
            foreach ($cust_emails as $email) {
                $store_quereminder=QueReminder::create([
                    'cust_code' => $data->CardCode,
                    'email_address' => str_replace(' ','',$email)
                    //'email_address' => "sysdev.napinfo@gmail.com"
                ]);
            }
        }

        $countQue=QueReminder::count();

        $success= "Succes Add ".$countQue." emails, Success job at ".$exec_time; 
        echo $success;
    }
}
