<?php

namespace App\Console\Commands;

use App\Models\ApiLog;
use App\Models\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\Billing;
use App\Models\TempCheckPayment;
use App\Models\Quarantine;
use App\Models\Rule;

class CheckPaymentAllCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckPaymentAll:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and Update status payment in Billing Table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //cek Billing All
        $query_billing=Billing::groupBy('DocNum')
        ->get();        

        foreach ($query_billing as $data) {
            //call url for api
            $rule=Rule::where('rule_name','API Inquiry Payment')->first();
            $url_payment=$rule->rule_value;

            //cek ke api inq-payment
            $response = Http::get($url_payment.$data->DocNum);
            $conn_status=$response['success'];

            if($conn_status=="true"){
                  //insert ke table temp
                $temp_check=TempCheckPayment::create([
                    'inv_number' => $data->DocNum,
                    'status_check' => $conn_status,
                    'cust_code' => $data->CardCode,
                ]);
            }
        }

        //cek temp yang status payment 1 atau sudah bayar
        $temp_table=TempCheckPayment::where('status_check','1')->get();

        //update table billing
        foreach ($temp_table as $item) {
            $update_billing=Billing::where('DocNum',$item->inv_number)
            ->update([
                'PaymentStatus' => 'Paid',
            ]);

            // Cek jka cid di Quarantine ada atau tidak
            $cek_quarantine=Quarantine::where('cid',$item->cust_code)->count();

            if($cek_quarantine>0){
                $delete_quarantine=Quarantine::where('cid',$item->cust_code)
                ->delete();
                
                //jika cid nya tdk ada di table Quarantine maka fungsi unblock tetap berjalan karna buka temp unblock
                if($delete_quarantine || $cek_quarantine =='0'){

                    //call url for api
                    $rule=Rule::where('rule_name','API Rad Unblock')->first();
                    $url_unblock_rads=$rule->rule_value;

                    $response = Http::post($url_unblock_rads, [
                        'cid' => $item->cust_code,
                    ]);
            
                    $username=$response['user'];
                    $message=$response['message'];
                    $created_by="inq_payment";
            
                    if($response['success']=="true"){
                        $logs=Log::create([
                            'cid_log' => $item->cust_code,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' => 'CheckPaymentAll:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Status Paid',
                        ]);
                    }
                    else{
                        $logs=Log::create([
                            'cid_log' => $item->cust_code,
                            'username_log' => $username,
                            'message_log' => $message,
                            'action_by_log' =>'CheckPaymentAll:cron',
                            'created_by_log' => $created_by,
                            'remarks_log' => 'Failed Update Log',
                        ]);
                    }
                }
            }
        }

        //delete table temp
        $delete_temp=TempCheckPayment::truncate();
        
        if($delete_temp){
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'inq-payment',
                'description' => 'Success Check Payment',
                'conn_status' => "true",
            ]);
        }
        else{
            //insert api_log
            $api_logs=ApiLog::create([
                'api_name' => 'inq-payment',
                'description' => 'Failed Check Payment',
                'conn_status' => "false",
            ]);
        }
    }
}
