<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\TempUnblockCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $now=Carbon::now()->format('YmdHis');;

        $schedule->command('tempUnblock:cron')
                ->everyTenMinutes();
                //->sendOutputTo("storage/logs_block_monthly/temp.txt");
        
        //start Billing Cron
        $schedule->command('Billing:cron')
                ->withoutOverlapping()
                ->dailyAt('23:00')
                ->sendOutputTo("storage/logs_billing/LogBilling_".$now.".txt");
        //end Billing Cron

        //start Billing Cron
        $schedule->command('BillingCorp:cron')
                ->withoutOverlapping()
                ->dailyAt('23:30')
                ->sendOutputTo("storage/logs_billing/LogBillingCorp_".$now.".txt");
        //end Billing Cron

        $schedule->command('CheckPayment:cron')
                ->everyThirtyMinutes();
                //->sendOutputTo("storage/logs_block_monthly/pay.txt");
        
        $schedule->command('StoreAJP:cron')
                ->everyTenMinutes()
                ->sendOutputTo("storage/logs_AJP/ajp_".$now.".txt");

        $schedule->command('CheckSyncAJP:cron')
                ->everyTenMinutes()
                ->sendOutputTo("storage/logs_AJP/check_ajp_".$now.".txt");

        $schedule->command('BlockCust:cron')
                ->monthlyOn(26, '23:59')
                ->sendOutputTo("storage/logs_block_monthly/LogBlock_".$now.".txt");

        $schedule->command('BlockCustNew:cron')
                ->dailyAt('12:05')
                ->sendOutputTo("storage/logs_block_daily/LogBlockDaily_".$now.".txt");
        
        //start AddQueue Cron
        $schedule->command('Reminder:cron')
                ->twiceMonthly(15, 22, '05:30')
                ->sendOutputTo("storage/logs_reminder/LogReminderQue_".$now.".txt");
        //end AddQueue Cron

        //start Sent Email Cron
        $schedule->command('SentReminder:cron')
                ->twiceMonthly(15, 22, '06:00')
                ->sendOutputTo("storage/logs_email/LogSentReminder_".$now.".txt");
        //end Sent Email Cron

        //start Sent Email Invoice Cron
        $schedule->command('Invoice:Cron')
                ->everyTenMinutes()
                ->sendOutputTo("storage/logs_email/LogSentInvoice_".$now.".txt");
        //end Sent Email Invoice Cron

        //start UpdateBillingQueue Cron
        $schedule->command('BillingApproval:cron')
                ->everyFiveMinutes()
                ->sendOutputTo("storage/LogUpdateBillingQue_".$now.".txt");
         //start UpdateBillingQueue Cron
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
